﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Metadata;

namespace ViewModel
{
    public class BankViewModel
    {
        public long id { get; set; }

        [Required]
        public string name { get; set; }

        [Required]
        public string va_code { get; set; }

        public long created_by { get; set; }

        public string dibuat { get; set; }
    }
}
