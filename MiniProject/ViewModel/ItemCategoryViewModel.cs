﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net.Http;
using System.Reflection.Metadata;

namespace ViewModel
{
    public class ItemCategoryViewModel
    {
        public long id { get; set; }

        [Required(ErrorMessage = "Nama Harus Diisi !")]
        //[Remote("IsAlreadySigned", "Register", HttpMethod = "POST", ErrorMessage = "EmailId already exists in database.")]
        public string name { get; set; }
        public bool is_delete { get; set; }
        public long modified_by { get; set; }
        public long pengubah { get; set; }
    }
}
