﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class MenuViewModel
    {
        public long id { get; set; }

        public string name { get; set; }

        public string url { get; set; }

        public long? parent_id { get; set; }

        public List<MenuViewModel> SubMenu { get; set; }
    }
}
