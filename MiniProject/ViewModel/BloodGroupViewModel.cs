﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Metadata;

namespace ViewModel
{
    public class BloodGroupViewModel
    {
        public long id { get; set; }

        [Required]
        public string code { get; set; }

        public string description { get; set; }
    }
}
