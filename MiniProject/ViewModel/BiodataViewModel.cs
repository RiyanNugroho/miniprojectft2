﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Metadata;

namespace ViewModel
{
    public class BiodataViewModel
    {
        public IFormFile FilePath { get; set; }
        public long id { get; set; }

        public string fullname { get; set; }

        public string mobile_phone { get; set; }

        public string image { get; set; }

        public string image_path { get; set; }
    }
}
