﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ViewModel
{
    public class DoctorSpecializationViewModel
    {
        public long id { get; set; }

        public long? doctor_id { get; set; }

        public long? specialization_id { get; set; }

        public string specialization_name { get; set; }

        public string doctor_name { get; set; }

        public bool count_list { get; set; }
    }
}
