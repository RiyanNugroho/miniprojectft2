﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class CustomerRegisteredCardViewModel
    {
        public long id { get; set; }

        public long? customer_id { get; set; }

        [MaxLength(20)]
        public string card_number { get; set; }

        public DateTime validity_period { get; set; }

        [MaxLength(5)]
        public string cvv { get; set; }
    }
}
