﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class CustomerVaViewModel
    {
        public long id { get; set; }

        public long? customer_id { get; set; }

        [MaxLength(20)]
        public string va_number { get; set; }
    }
}
