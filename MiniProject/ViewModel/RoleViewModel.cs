﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Metadata;

namespace ViewModel
{
    public class RoleViewModel
    {
        public long id { get; set; }

        public string name { get; set; }

        public string? code { get; set; }

    }
}
