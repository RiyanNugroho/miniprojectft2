﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Metadata;

namespace ViewModel
{
    public class UserViewModel
    {
        public long id { get; set; }

        public long biodata_id { get; set; }

        public long? customer_id { get; set; }

        public long? doctor_id { get; set; }

        //[Required]
        public string fullname { get; set; }

        public long role_id { get; set; }

        public string roleName { get; set; }

        [Required, Display(Name = "Email"), DataType(DataType.EmailAddress), EmailAddress(ErrorMessage = "Format email tidak sesuai!")]
        public string email { get; set; }

        [Required, Display(Name = "Password"), DataType(DataType.Password)]
        public string password { get; set; }

        public int login_attempt { get; set; }

        public bool is_locked { get; set; }

        public DateTime last_login { get; set; }

        public string message { get; set; }

        public string token { get; set; }

        public string mobile_phone  { get; set; }
    }
}
