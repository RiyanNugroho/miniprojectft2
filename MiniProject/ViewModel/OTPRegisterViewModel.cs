﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class OTPRegisterViewModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        public string? email { get; set; }

        public long? user_id { get; set; }
        
        public string? token { get; set; }

        public DateTime? expired_on { get; set; }

        public bool? is_expired { get; set; }

        [MaxLength(20)]
        public string? used_for { get; set; }
    }
}
