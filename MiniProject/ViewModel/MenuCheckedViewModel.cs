﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModel
{
    public class MenuRoleCheck
    {
        public long RoleId { get; set; }
        public List<MenuCheckedViewModel> List { get; set; }
    }

    public class MenuCheckedViewModel
    {
        public long Id { get; set; }

        public bool Check { get; set; }

    }
}
