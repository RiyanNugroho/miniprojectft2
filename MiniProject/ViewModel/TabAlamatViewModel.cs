﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class TabAlamatViewModel
    {
        public long id { get; set; }

        public long? biodata_id { get; set; }

        [Required]
        public string label { get; set; }
        [Required]
        public string recipient { get; set; }
        [Required]
        public string recipient_phone_number { get; set; }
        [Required]
        public long? location_id { get; set; }

        [MaxLength(10)]
        public string postal_code { get; set; }
        [Required]
        public string address { get; set; }

        public bool Check { get; set; }
        [Required]
        public string kota { get; set; }
        public string Nama { get; set; }
    }
}
