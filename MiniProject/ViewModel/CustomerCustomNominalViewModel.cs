﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModel
{
    public class CustomerCustomNominalViewModel
    {
        public long id { get; set; }

        public long? customer_id { get; set; }

        public int nominal { get; set; }
    }
}
