﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Metadata;

namespace ViewModel
{
    public class EducationLevelViewModel
    {
        public long id { get; set; }

        [Required]
        public string name { get; set; }
        public long biodata_id { get; set; }
        public string dibuat_oleh { get; set; }
    }
}
