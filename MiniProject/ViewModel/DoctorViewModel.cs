﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Metadata;

namespace ViewModel
{
    public class DoctorViewModel
    {
        public long id { get; set; }

        public long biodata_id { get; set; }

        [MaxLength(50)]
        public string str { get; set; }

        public string Nama_Dokter { get; set; }
    }
}