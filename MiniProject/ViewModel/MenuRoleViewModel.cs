﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Metadata;

namespace ViewModel
{
    public class MenuRoleViewModel
    {
        public long id { get; set; }

        public long? menu_id { get; set; }

        public string namaMenu { get; set; }

        public long? role_id { get; set; }

        public string namaRole { get; set; }

        public string kodeRole { get; set; }
        public bool Check { get; set; }
    }
}
