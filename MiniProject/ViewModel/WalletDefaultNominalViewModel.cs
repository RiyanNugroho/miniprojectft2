﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net.Http;
using System.Reflection.Metadata;

namespace ViewModel
{
    public class WalletDefaultNominalViewModel
    {
        public long id { get; set; }
        public int nominal { get; set; }
    }
}
