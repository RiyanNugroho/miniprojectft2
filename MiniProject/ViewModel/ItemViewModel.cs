﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Metadata;

namespace ViewModel
{
    public class MedItemViewModel
    {
        //masuk
        public IFormFile FilePath { get; set; }

        public long id { get; set; }

        public string name { get; set; }

        public long? medical_item_category_id { get; set; }

        [Display(Name = "Category")]
        public string categoryName { get; set; }

        public string composition { get; set; }

        public long? medical_item_segmentation_id { get; set; }

        [Display(Name = "Segmentation")]
        public string segmentationName { get; set; }

        public string manufacturer { get; set; }

        public string indication { get; set; }

        public string dosage { get; set; }

        public string directions { get; set; }

        public string contraindication { get; set; }

        public string caution { get; set; }

        public string packaging { get; set; }

        public long price_max { get; set; }

        public long price_min { get; set; }

        //keluar
        public string image { get; set; }

        public string image_path { get; set; }
        public bool is_delete { get; set; }

    }
}
