﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class CustomerWalletViewModel
    {
        public long id { get; set; }

        public long? customer_id { get; set; }

        [MaxLength(6)]
        public string pin { get; set; }

        public decimal balance { get; set; }

        [MaxLength(50)]
        public string barcode { get; set; }

        public decimal points { get; set; }

        public DateTime create_on { get; set; }
    }
}
