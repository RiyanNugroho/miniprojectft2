﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Metadata;

namespace ViewModel
{
    public class ItemSegmentationViewModel
    {
        public long id { get; set; }

        [Required(ErrorMessage ="Nama Harus Diisi !")]
        public string name { get; set; }
        public bool is_delete { get; set; }
    }
}
