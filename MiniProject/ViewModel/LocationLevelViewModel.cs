﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class LocationLevelViewModel
    {
        public long id { get; set; }
        
        public string name { get; set; }
        
        public string abbreviation { get; set; }
    }
}
