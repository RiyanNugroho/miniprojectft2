﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Metadata;

namespace ViewModel
{
    public class Customer
    {
        public long id { get; set; }

        public long? biodata_id { get; set; }

        [Required]
        public string fullname { get; set; }

        public DateTime dob { get; set; }

        [Required]
        public string gender { get; set; }

        public long? blood_group_id { get; set; }

        public string bloodname { get; set; }

        public string rhesus_type { get; set; }

        public decimal? height { get; set; }

        public decimal? weight { get; set; }
    }

    public class PasienViewModel
    {
        public long id { get; set; }

        public long customer_id { get; set; }

        public long? biodata_id { get; set; }

        [Required]
        public string fullname { get; set; }

        public DateTime dob { get; set; }

        [Required]
        public string gender { get; set; }

        public long? blood_group_id { get; set; }

        public string bloodname { get; set; }

        public string rhesus_type { get; set; }

        public decimal? height { get; set; }

        public decimal? weight { get; set; }

        [Required, Display(Name = "Relasi")]
        public long? customer_relation_id { get; set; }
    }
}
