﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class LocationViewModel
    {
        public long id { get; set; }

        [Required, Display(Name = "Nama Lokasi")]                
        public string name { get; set; }

        [Display(Name ="Nama Lokasi")]
        public string LocaLevName { get; set; }

        public string region { get; set; }

        public string? regionlist { get; set; }

        public long? parent_id { get; set; }

        [Required, Display(Name ="Level Lokasi")]
        public long? location_level_id { get; set; }
    }    
}
