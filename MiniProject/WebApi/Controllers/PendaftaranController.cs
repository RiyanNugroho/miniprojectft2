﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PendaftaranController : ControllerBase
    {
        private PendaftaranRepository daftarRepo = new PendaftaranRepository();
        

        [HttpGet("Role/{id}")]
        public async Task<ResponseResult> ByParent(long id)
        {
            return daftarRepo.ByParent(id);
        }        

        [HttpPost]
        public async Task<ResponseResult> Post(PendaftaranViewModel model)
        {
            return daftarRepo.Create(model);
        }        
    }
}
