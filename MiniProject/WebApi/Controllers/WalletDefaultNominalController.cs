﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReadableBodyStream(Roles = "Administrator,Pasien")]
    public class WalletDefaultNominalController : ControllerBase
    {
        private WalletDefaultNominalRepository DNominalRepo = new WalletDefaultNominalRepository(ClaimContext.userId());
        [HttpGet]
        public async Task<List<WalletDefaultNominalViewModel>> Get()
        {
            return DNominalRepo.GetAll();
        }
        [HttpPost]
        public async Task<ResponseResult> Post(WalletDefaultNominalViewModel model)
        {
            return DNominalRepo.Create(model);
        }
    }
}
