﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApi.DataModels;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using WebApi.Repositories;
using Microsoft.AspNetCore.Authorization;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReadableBodyStream(Roles = "Administrator,Pasien")]

    public class PasienController : ControllerBase
    {
        private PasienRepository pasienRepo = new PasienRepository(ClaimContext.userId());

        [HttpGet]
        public async Task<ResponseResult> Get()
        {
            return await pasienRepo.All(1, 3, "", "fullname", true);
        }

        //[HttpGet]
        //public async Task<List<PasienViewModel>> Get()
        //{
        //    return pasienRepo.GetAll();
        //}

        [HttpGet("M_blood_group/{id}")]
        public async Task<ResponseResult> ByParent1(long id)
        {
            return pasienRepo.ByParent1(id);
        }

        [HttpGet("CustomerRelation/{id}")]
        public async Task<ResponseResult> ByParent2(long id)
        {
            return pasienRepo.ByParent2(id);
        }

        [HttpGet("{id}")]
        public async Task<PasienViewModel> Get(long id)
        {
            return pasienRepo.GetById(id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(PasienViewModel model)
        {
            return pasienRepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(PasienViewModel model)
        {

            return pasienRepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            PasienViewModel model = new PasienViewModel() { id = id };
            return pasienRepo.Delete(model);
        }

        //[HttpGet("page/{page}/{rows}/{search}/{cols}/{asc}")]
        //public async Task<ResponseResult> ListPage(int page, int rows, string search, string cols, bool asc)
        //{
        //    search = search == "--all" ? "" : search;
        //    return await pasienRepo.All(page, rows, search, cols, asc);
        //}
    }
}
