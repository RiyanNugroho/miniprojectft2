﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenerateOTPRegisterController : ControllerBase
    {
        GenerateOTPRegisterRepository genOTPRepo = new GenerateOTPRegisterRepository();

        [HttpPost]
        public async Task<ResponseResult> Post(OTPRegisterViewModel model)
        {
            return genOTPRepo.Create(model);
        }
    }
}
