﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private RoleRepository Rolerepo = new RoleRepository();

        [HttpGet]
        public async Task<List<RoleViewModel>> Get()
        {
            return Rolerepo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<RoleViewModel> Get(long id)
        {
            return Rolerepo.GetById(id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(RoleViewModel model)
        {
            return Rolerepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(RoleViewModel model)
        {

            return Rolerepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            RoleViewModel model = new RoleViewModel() { id = id };
            return Rolerepo.Delete(model);
        }

        [HttpGet("search/{content}")]
        public async Task<List<RoleViewModel>> Search(string content)
        {

            return Rolerepo.ByFilter(content);
        }
    }
}
