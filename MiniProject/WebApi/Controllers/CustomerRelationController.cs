﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApi.DataModels;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using WebApi.Repositories;
using Microsoft.AspNetCore.Authorization;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReadableBodyStream(Roles = "Administrator")]
    public class CustomerRelationController : ControllerBase
    {
        private CustomerRelationRepository custRelationRepo = new CustomerRelationRepository(ClaimContext.userId());

        [HttpGet]
        public async Task<List<CustRelationViewModel>> Get()
        {
            return custRelationRepo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<CustRelationViewModel> Get(long id)
        {
            return custRelationRepo.GetById(id);
        }

        [HttpGet("Search/{content}")]
        public async Task<List<CustRelationViewModel>> Search(string content)
        {
            return custRelationRepo.ByFilter(content);
        }

        //[HttpGet("Search/{content}")]
        //public async Task<ResponseResult> Search(string content)
        //{
        //    return custRelationRepo.ByFilter(content);
        //}

        [HttpPost]
        public async Task<ResponseResult> Post(CustRelationViewModel model)
        {
            return custRelationRepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(CustRelationViewModel model)
        {

            return custRelationRepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            CustRelationViewModel model = new CustRelationViewModel() { id = id };
            return custRelationRepo.Delete(model);
        }
    }
}
