﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReadableBodyStream(Roles = "Administrator,Pasien")]
    public class itemSegmentationController : ControllerBase
    {
        private itemSegmentationRepository itemSegmentationRepo = new itemSegmentationRepository(ClaimContext.userId());
        [HttpGet]
        public async Task<ResponseResult> Get()
        {
            return await itemSegmentationRepo.All(1, 10, "");
        }
        [HttpGet("{id}")]
        public async Task<ItemSegmentationViewModel> Get(long id)
        {
            return itemSegmentationRepo.GetById(id);
        }
        [HttpPost]
        public async Task<ResponseResult> Post(ItemSegmentationViewModel model)
        {
            return itemSegmentationRepo.Create(model);
        }
        [HttpPut]
        public async Task<ResponseResult> Put(ItemSegmentationViewModel model)
        {
            return itemSegmentationRepo.Update(model);
        }
        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            ItemSegmentationViewModel model = new ItemSegmentationViewModel() { id = id };
            return itemSegmentationRepo.Delete(model);
        }

        //[HttpGet("search/{content}")]
        //public async Task<List<ItemSegmentationViewModel>> Search(string content)
        //{
        //    return itemSegmentationRepo.ByFilter(content);
        //}

        [HttpGet("page/{page}/{rows}/{search}")]
        public async Task<ResponseResult> ListPage(int page, int rows, string search)
        {
            search = search == "--all" ? "" : search;
            return await itemSegmentationRepo.All(page, rows, search);
        }
    }
}
