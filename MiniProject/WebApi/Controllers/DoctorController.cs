﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoctorController : ControllerBase
    {
        private DoctorRepository doctorrepo = new DoctorRepository();
        [HttpGet]
        public async Task<List<DoctorViewModel>> Get()
        {
            return doctorrepo.GetAll();
        }

        [HttpGet("search/{content}")]
        public async Task<List<DoctorViewModel>> Search(string content)
        {
            return doctorrepo.ByFilter(content);
        }

        [HttpGet("{id}")]
        public async Task<DoctorViewModel> Get(long id)
        {
            return doctorrepo.GetById(id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(DoctorViewModel model)
        {
            return doctorrepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(DoctorViewModel model)
        {
            return doctorrepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            DoctorViewModel model = new DoctorViewModel() { id = id };
            return doctorrepo.Delete(model);
        }

        [HttpGet("page/{page}/{rows}/{search}")]
        public async Task<ResponseResult> ListPage(int page, int rows, string search)
        {
            search = search == "--all" ? "" : search;
            return await doctorrepo.All(page, rows, search);
        }
    }
}
