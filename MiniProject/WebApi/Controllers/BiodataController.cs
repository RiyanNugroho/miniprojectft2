﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BiodataController : ControllerBase
    {
        private BiodataRepository bioRepo = new BiodataRepository();

        private M_medical_itemRepository M_medical_itemRepo = new M_medical_itemRepository(ClaimContext.userId());
        private string[] acceptedExt = new string[] { ".jpg", ".jpeg", ".png", ".gif" };

        private string GetUniqueName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
            + "_" + Guid.NewGuid().ToString().Substring(0, 4)
            + Path.GetExtension(fileName);
        }

      

        [HttpPost]
        public async Task<ResponseResult> Post([FromForm] BiodataViewModel model)
        {
            //return M_medical_itemRepo.Create(model);
            ResponseResult result = new ResponseResult();
            try
            {
                if (model.FilePath != null)
                {
                    string fileExt = Path.GetExtension(model.FilePath.FileName);
                    if (Array.IndexOf(acceptedExt, fileExt) != -1)
                    {
                        var uniqueFileName = GetUniqueName(model.FilePath.FileName);
                        ////lokasi penempatan image nya
                        //var uploads = Path.Combine("Resources", "images");
                        ////menambah namauniknya
                        //var filePath = Path.Combine(uploads, uniqueFileName);

                        //using (var fileStream = new FileStream(filePath, FileMode.Create))
                        //{
                        //    model.FilePath.CopyTo(fileStream);
                        //}

                        model.image_path = uniqueFileName;

                        result = bioRepo.Create(model);
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = "File Type accepted : " + string.Join(", ", acceptedExt);
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
                result.Entity = StatusCode(500, $"internal server error :{e}");
            }
            return result;
        }
        [HttpGet]
        public async Task<List<BiodataViewModel>> Get()
        {
            return await bioRepo.ByFilter("");
        }

        [HttpGet("search/{search}")]
        public async Task<List<BiodataViewModel>> Search(string search)
        {
            search = search == "--all" ? "" : search;
            return await bioRepo.ByFilter(search);
        }

        [HttpGet("{id}")]
        public async Task<BiodataViewModel> Get(long id)
        {
            return bioRepo.GetById(id);
        }


        [HttpPut]
        public async Task<ResponseResult> Put(BiodataViewModel model)
        {
            return bioRepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            BiodataViewModel model = new BiodataViewModel() { id = id };
            return bioRepo.Delete(model);
        }
    }
}
