﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoctorTreatmentController : ControllerBase
    {
        private DoctorTreatmentRepository doctreatrepo = new DoctorTreatmentRepository();
        [HttpGet]
        public async Task<List<DoctorTreatmentViewModel>> Get()
        {
            return doctreatrepo.GetAll();
        }

        [HttpGet("search/{content}")]
        public async Task<List<DoctorTreatmentViewModel>> Search(string content)
        {
            return doctreatrepo.ByFilter(content);
        }

        [HttpGet("{id}")]
        public async Task<DoctorTreatmentViewModel> Get(long id)
        {
            return doctreatrepo.GetById(id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(DoctorTreatmentViewModel model)
        {
            return doctreatrepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(DoctorTreatmentViewModel model)
        {
            return doctreatrepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            DoctorTreatmentViewModel model = new DoctorTreatmentViewModel() { id = id };
            return doctreatrepo.Delete(model);
        }
    }
}
