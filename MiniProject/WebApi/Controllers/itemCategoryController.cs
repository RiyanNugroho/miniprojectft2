﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReadableBodyStream(Roles = "Administrator,Pasien")]
    public class itemCategoryController : ControllerBase
    {
        private itemCategoryRepository itemCategoryRepo = new itemCategoryRepository(ClaimContext.userId(), ClaimContext.biodata_id());
        [HttpGet]
        public async Task<ResponseResult> Get()
        {
            return await itemCategoryRepo.All(1,10,"");
        }
        [HttpGet("{id}")]
        public async Task<ItemCategoryViewModel> Get(long id)
        {
            return itemCategoryRepo.GetById(id);
        }
        [HttpPost]
        public async Task<ResponseResult> Post(ItemCategoryViewModel model)
        {
            return itemCategoryRepo.Create(model);
        }
        public async Task<ResponseResult> Put(ItemCategoryViewModel model)
        {
            return itemCategoryRepo.Update(model);
        }
        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            ItemCategoryViewModel model = new ItemCategoryViewModel() { id = id };
            return itemCategoryRepo.Delete(model);
        }

        [HttpGet("page/{page}/{rows}/{search}")]
        public async Task<ResponseResult> ListPage(int page, int rows, string search)
        {
            search = search == "--all" ? "" : search;
            return await itemCategoryRepo.All(page, rows, search);
        }
    }
}
