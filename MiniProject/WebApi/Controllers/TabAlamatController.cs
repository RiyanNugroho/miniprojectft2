﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReadableBodyStream(Roles = "Administrator,Pasien")]
    public class TabAlamatController : ControllerBase
    {
        private TabAlamatRepository tabAlamatRepo = new TabAlamatRepository(ClaimContext.userId(), ClaimContext.biodata_id());
        [HttpGet]
        public async Task<ResponseResult> Get()
        {
            return await tabAlamatRepo.All(1, 10, "", "biodata_id", true);
        }

        [HttpGet("TabAlamat/{id}")]
        public async Task<ResponseResult> ByParent(long id)
        {
            return tabAlamatRepo.ByParent(id);
        }

        [HttpGet("{Id}")]
        public async Task<TabAlamatViewModel> Get(long Id)
        {
            return tabAlamatRepo.GetById(Id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(TabAlamatViewModel model)
        {
            return tabAlamatRepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(TabAlamatViewModel model)
        {
            return tabAlamatRepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            TabAlamatViewModel model = new TabAlamatViewModel() { id = id };
            return tabAlamatRepo.Delete(model);
        }


        [HttpGet("page/{page}/{rows}/{search}/{column}/{ascen}")]
        public async Task<ResponseResult> ListPage(int page, int rows, string search, string column, bool ascen)
        {

            search = search == "--all" ? "" : search;
            return await tabAlamatRepo.All(page, rows, search, column, ascen);
        }

        [HttpPost("deletemany")]
        [ReadableBodyStream(Roles = "Administrator,Product")]
        public async Task<ResponseResult> DeleteMany(List<AlamatMultiCheckViewModel> model)
        {
            return await tabAlamatRepo.DeleteMany(model);
        }
    }
}