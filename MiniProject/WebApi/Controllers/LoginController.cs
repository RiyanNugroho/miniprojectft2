﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;
using WebApi.Repositories;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration configuration;
        private LoginRepository loginRepo = new LoginRepository();

        public LoginController(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<UserViewModel> Post(UserViewModel model)
        {
            UserViewModel result = new UserViewModel();
            if (ModelState.IsValid)
            {
                result = loginRepo.Authentication(model);
                if (result != null)
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, result.email),
                        new Claim("FullName", result.fullname),
                        new Claim("Id", result.id.ToString()),
                        new Claim("biodataid", result.biodata_id.ToString()),
                        new Claim("customerid", result.customer_id.ToString()),
                        new Claim("doctorid", result.doctor_id.ToString()),
                    };

                    claims.Add(new Claim(ClaimTypes.Role, result.roleName));

                    if (result.is_locked != true && result.login_attempt == 0)
                    {
                        var token = GetToken(claims);
                        result.token = new JwtSecurityTokenHandler().WriteToken(token);
                    }
                }
                else
                {
                    return new UserViewModel();
                }
            }
            return result;
        }

        private JwtSecurityToken GetToken(List<Claim> authClaims)
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]));

            var token = new JwtSecurityToken(
                issuer: configuration["JWT:ValidIssuer"],
                audience: configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddDays(1),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );

            return token;
        }
    }
}
