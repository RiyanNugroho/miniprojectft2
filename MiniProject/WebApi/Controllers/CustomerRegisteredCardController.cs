﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReadableBodyStream(Roles = "Administrator,Pasien")]
    public class CustomerRegisteredCardController : ControllerBase
    {
        private CustomerRegisteredCardRepository CustRegRepo = new CustomerRegisteredCardRepository(ClaimContext.userId(), ClaimContext.customer_id());

        public async Task<List<CustomerRegisteredCardViewModel>> Get()
        {
            return CustRegRepo.GetAll();
        }

        [HttpGet("{customer_id}")]
        public async Task<CustomerRegisteredCardViewModel> Get(long customer_id)
        {
            return CustRegRepo.GetById(customer_id);
        }
    }
}
