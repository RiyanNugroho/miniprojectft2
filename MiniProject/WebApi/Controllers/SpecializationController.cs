﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpecializationController : ControllerBase
    {
        private SpecializationRepository specRepo = new SpecializationRepository();
        [HttpGet]
        public async Task<List<SpecializationViewModel>> Get()
        {
            return specRepo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<SpecializationViewModel> Get(long id)
        {
            return specRepo.GetById(id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(SpecializationViewModel model)
        {
            return specRepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(SpecializationViewModel model)
        {
            return specRepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            SpecializationViewModel model = new SpecializationViewModel() { id = id };
            return specRepo.Delete(model);
        }        
    }
}
