﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReadableBodyStream(Roles = "Administrator")]
    public class BankController : ControllerBase
    {
        private BankRepository bankRepo = new BankRepository(ClaimContext.userName());
        [HttpGet]
        public async Task<ResponseResult> Get()
        {
            return await bankRepo.All(1, 10, "", "name", true);
        }

        [HttpGet("{Id}")]
        public async Task<BankViewModel> Get(long Id)
        {
            return bankRepo.GetById(Id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(BankViewModel model)
        {
            return bankRepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(BankViewModel model)
        {
            return bankRepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            BankViewModel model = new BankViewModel() { id = id };
            return bankRepo.Delete(model);
        }

        //[HttpGet("search/{content}")]
        //public async Task<List<BankViewModel>> Search(string content)
        //{

        //    return bankRepo.All(content);
        //}

        [HttpGet("page/{page}/{rows}/{search}/{column}/{ascen}")]
        public async Task<ResponseResult> ListPage(int page, int rows, string search, string column, bool ascen)
        {
            //int pPage = 1;
            //int pRows = 3;
            //ResponseResult result = new ResponseResult();
            //var bankList = bankRepo.All(page, rows);
            //decimal totalPage = (decimal)bankList.Item2 / rows;

            //result.Entity = new { List = bankList.Item1, Page = totalPage };
            search = search == "--all" ? "" : search;
            return await bankRepo.All(page, rows, search, column, ascen);
        }
    }
}
