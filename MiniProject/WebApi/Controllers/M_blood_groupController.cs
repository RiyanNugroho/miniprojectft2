﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReadableBodyStream(Roles = "Administrator, Dokter, Pasien")]
    public class M_blood_groupController : ControllerBase
    {
        private M_blood_groupRepository bloodRepo = new M_blood_groupRepository(ClaimContext.userId());
        [HttpGet]
        public async Task<List<BloodGroupViewModel>> Get()
        {
            return bloodRepo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<BloodGroupViewModel> Get(long id)
        {
            return bloodRepo.GetById(id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(BloodGroupViewModel model)
        {
            return bloodRepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(BloodGroupViewModel model)
        {
            return bloodRepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            BloodGroupViewModel model = new BloodGroupViewModel() { id = id };
            return bloodRepo.Delete(model);
        }

        [HttpGet("search/{content}")]
        public async Task<List<BloodGroupViewModel>> Search(string content)
        {            
            return bloodRepo.ByFilter(content);
        }
    }
}
