﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReadableBodyStream(Roles = "Administrator")]
    public class LocationController : ControllerBase
    {
        private LocationRepository locRepo = new LocationRepository(ClaimContext.userId());
        [HttpGet]
        public async Task<ResponseResult> Get()
        {
            return await locRepo.All(1, 10, "");
        }

        [HttpGet("LocationLevel/{id}")]
        public async Task<ResponseResult> ByParent(long id)
        {
            return locRepo.ByParent(id);
        }

        [HttpGet("{id}")]
        public async Task<LocationViewModel> Get(long id)
        {
            return locRepo.GetById(id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(LocationViewModel model)
        {
            return locRepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(LocationViewModel model)
        {
            return locRepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            LocationViewModel model = new LocationViewModel() { id = id };
            return locRepo.Delete(model);
        }

        [HttpGet("page/{page}/{rows}/{search}")]
        public async Task<ResponseResult> ListPage(int page, int rows, string search)
        {
            search = search == "--all" ? "" : search;
            return await locRepo.All(page, rows, search);
        }

    }
}
