﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationLevelController : ControllerBase
    {
        private LocationLevelRepository loclevRepo = new LocationLevelRepository();
        [HttpGet]
        public async Task<List<LocationLevelViewModel>> Get()
        {
            return loclevRepo.GetAll();
        }
       

        [HttpGet("{id}")]
        public async Task<LocationLevelViewModel> Get(long id)
        {
            return loclevRepo.GetById(id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(LocationLevelViewModel model)
        {
            return loclevRepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(LocationLevelViewModel model)
        {
            return loclevRepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            LocationLevelViewModel model = new LocationLevelViewModel() { id = id };
            return loclevRepo.Delete(model);
        }
    }
}
