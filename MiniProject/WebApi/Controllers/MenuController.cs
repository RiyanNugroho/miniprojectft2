﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApi.DataModels;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using WebApi.Repositories;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private MenuRepository Menurepo = new MenuRepository();

        [HttpGet]
        public async Task<List<MenuViewModel>> Get()
        {
            return Menurepo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<MenuViewModel> Get(long id)
        {
            return Menurepo.GetById(id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(MenuViewModel model)
        {
            return Menurepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(MenuViewModel model)
        {

            return Menurepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            MenuViewModel model = new MenuViewModel() { id = id };
            return Menurepo.Delete(model);
        }

        [HttpGet("search/{content}")]
        public async Task<List<MenuViewModel>> Search(string content)
        {

            return Menurepo.ByFilter(content);
        }
    }
}