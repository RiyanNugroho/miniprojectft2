﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReadableBodyStream(Roles = "Administrator,Pasien")]
    public class CustomerCustomNominalController : ControllerBase
    {
        private CustomerCustomNominalRepository CustNominalRepo = new CustomerCustomNominalRepository(ClaimContext.userId(), ClaimContext.customer_id());
        [HttpGet]
        public async Task<List<CustomerCustomNominalViewModel>> Get()
        {
            return CustNominalRepo.GetAll();
        }
        [HttpPost]
        public async Task<ResponseResult> Post(CustomerCustomNominalViewModel model)
        {
            return CustNominalRepo.Create(model);
        }
    }
}
