﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReadableBodyStream(Roles = "Administrator,Pasien")]
    public class CustomerWalletController : ControllerBase
    {
        private CustomerWalletRepository CustWalletRepo = new CustomerWalletRepository(ClaimContext.userId(), ClaimContext.customer_id());

        public async Task<List<CustomerWalletViewModel>> Get()
        {
            return CustWalletRepo.GetAll();
        }

        [HttpGet("{customer_id}")]
        public async Task<CustomerWalletViewModel> Get(long customer_id)
        {
            return CustWalletRepo.GetById(customer_id);
        }
    }
}
