﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReadableBodyStream(Roles = "Administrator,Pasien")]
    public class M_medical_itemController : ControllerBase
    {
        private M_medical_itemRepository M_medical_itemRepo = new M_medical_itemRepository(ClaimContext.userId());
        private itemCategoryRepository itemCatRepo = new itemCategoryRepository(ClaimContext.userId(), ClaimContext.biodata_id());
        private string[] acceptedExt = new string[] { ".jpg", ".jpeg", ".png", ".gif" };

        private string GetUniqueName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
            + "_" + Guid.NewGuid().ToString().Substring(0, 4)
            + Path.GetExtension(fileName);
        }

        //[HttpGet]
        //public async Task<ResponseResult> Get()
        //{
        //    ResponseResult result = new ResponseResult();
        //    result.Entity = M_medical_itemRepo.GetAll();
        //    return result;

        //}

        [HttpGet("itemCategory/{id}")]
        public async Task<ResponseResult> GetByParent(long id)
        {
            return M_medical_itemRepo.GetByParent(id);
        }

        [HttpGet]
        public async Task<ResponseResult> Get()
        {
            return await M_medical_itemRepo.All(1, 10, "");
        }
        [HttpGet("{id}")]
        public async Task<MedItemViewModel> Get(long id)
        {
            return M_medical_itemRepo.GetById(id);
        }

            [HttpPost]
        public async Task<ResponseResult> Post([FromForm]  MedItemViewModel model)
        {
            //return M_medical_itemRepo.Create(model);
            ResponseResult result = new ResponseResult();
            try
            {
                if (model.FilePath != null)
                {
                    string fileExt = Path.GetExtension(model.FilePath.FileName);
                    if (Array.IndexOf(acceptedExt, fileExt) != -1)
                    {
                        var uniqueFileName = GetUniqueName(model.FilePath.FileName);
                        ////lokasi penempatan image nya
                        //var uploads = Path.Combine("Resources", "images");
                        ////menambah namauniknya
                        //var filePath = Path.Combine(uploads, uniqueFileName);

                        //using (var fileStream = new FileStream(filePath, FileMode.Create))
                        //{
                        //    model.FilePath.CopyTo(fileStream);
                        //}

                        model.image_path = uniqueFileName;

                        result = M_medical_itemRepo.Create(model);
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = "File Type accepted : " + string.Join(", ", acceptedExt);
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
                result.Entity = StatusCode(500, $"internal server error :{e}");
            }
            return result;
        }
        [HttpPut]
        public async Task<ResponseResult> Put(MedItemViewModel model)
        {
           return M_medical_itemRepo.Update(model);
        }
        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            MedItemViewModel model = new MedItemViewModel() { id = id };
            return M_medical_itemRepo.Delete(model);
        }

        [HttpGet("page/{page}/{rows}/{search}")]
        public async Task<ResponseResult> ListPage(int page, int rows, string search)
        {
            search = search == "--all" ? "" : search;
            return await M_medical_itemRepo.All(page, rows, search);
        }
    }
}
