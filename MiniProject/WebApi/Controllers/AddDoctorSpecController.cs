﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReadableBodyStream(Roles = "Dokter")]
    public class AddDoctorSpecController : ControllerBase
    {
        private AddDoctorSpecRepository docspecRepo = new AddDoctorSpecRepository(ClaimContext.userId(), ClaimContext.doctor_id());
        [HttpGet]
        public async Task<List<DoctorSpecializationViewModel>> Get()
        {
            return docspecRepo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<DoctorSpecializationViewModel> Get(long id)
        {
            return docspecRepo.GetById(id);
        }

        [HttpGet("Specialization/{id}")]
        public async Task<ResponseResult> ByParent(long id)
        {
            return docspecRepo.ByParent(id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(DoctorSpecializationViewModel model)
        {
            return docspecRepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(DoctorSpecializationViewModel model)
        {
            return docspecRepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            DoctorSpecializationViewModel model = new DoctorSpecializationViewModel() { id = id };
            return docspecRepo.Delete(model);
        }
    }
}
