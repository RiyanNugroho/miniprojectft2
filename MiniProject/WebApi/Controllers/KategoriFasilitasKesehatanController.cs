﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[ReadableBodyStream(Roles = "Administrator, Dokter")]
    public class KategoriFasilitasKesehatanController : ControllerBase
    {
        private KategoriFasilitasKesehatanRepository faskesRepo = new KategoriFasilitasKesehatanRepository(ClaimContext.userId());
        [HttpGet]
        public async Task<List<FacilityCategoryViewModel>> Get()
        {
            return faskesRepo.GetAll();
        }

        [HttpGet("search/{content}")]
        public async Task<List<FacilityCategoryViewModel>> Search(string content)
        {
            return faskesRepo.ByFilter(content);
        }

        [HttpGet("{id}")]
        public async Task<FacilityCategoryViewModel> Get(long id)
        {
            return faskesRepo.GetById(id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(FacilityCategoryViewModel model)
        {
            return faskesRepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(FacilityCategoryViewModel model)
        {
            return faskesRepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            FacilityCategoryViewModel model = new FacilityCategoryViewModel() { id = id };
            return faskesRepo.Delete(model);
        }
    }
}