﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApi.DataModels;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using WebApi.Repositories;
using Microsoft.AspNetCore.Authorization;


namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JenjangPendidikanController : ControllerBase
    {
        private JenjangPendidikanRepository JenjangPendidikanRepo = new JenjangPendidikanRepository();

        [HttpGet]
        public async Task<List<EducationLevelViewModel>> Get()
        {
            return JenjangPendidikanRepo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<EducationLevelViewModel> Get(long id)
        {
            return JenjangPendidikanRepo.GetById(id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(EducationLevelViewModel model)
        {
            return JenjangPendidikanRepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(EducationLevelViewModel model)
        {

            return JenjangPendidikanRepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            EducationLevelViewModel model = new EducationLevelViewModel() { id = id };
            return JenjangPendidikanRepo.Delete(model);
        }

        [HttpGet("search/{content}")]
        public async Task<List<EducationLevelViewModel>> Search(string content)
        {

            return JenjangPendidikanRepo.ByFilter(content);
        }
    }
}
