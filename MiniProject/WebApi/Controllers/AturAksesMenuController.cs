﻿using System.Threading.Tasks;
using WebApi.DataModels;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using WebApi.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AturAksesMenuController : ControllerBase
    {
        private AturAksesMenuRepository AturAksesMenuRepo = new AturAksesMenuRepository();

        [HttpGet]
        public async Task<List<MenuRoleViewModel>> Get()
        {
            return AturAksesMenuRepo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<MenuRoleViewModel> Get(long id)
        {
            return AturAksesMenuRepo.GetById(id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(MenuRoleViewModel model)
        {
            return AturAksesMenuRepo.Create(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            MenuRoleViewModel model = new MenuRoleViewModel() { id = id };
            return AturAksesMenuRepo.Delete(model);
        }

        [HttpGet("search/{content}")]
        public async Task<List<MenuRoleViewModel>> Search(string content)
        {

            return AturAksesMenuRepo.ByFilter(content);
        }

        [HttpPut]
        public async Task<ResponseResult> CheckList(MenuRoleCheck model)
        {
            return await AturAksesMenuRepo.CheckList(model);
        }

    }
}
