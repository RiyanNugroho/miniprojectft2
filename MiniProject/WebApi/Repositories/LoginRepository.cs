﻿using System;
using System.Collections.Generic;
using ViewModel;
using System.Linq;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class LoginRepository
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();
        private UserViewModel result = new UserViewModel();

        public UserViewModel Authentication(UserViewModel userLogin)
        {
            M_user user = _MiniProjDbContext.M_users.Where(o => o.email == userLogin.email).FirstOrDefault();
            M_customer customer = _MiniProjDbContext.M_customers.Where(o => o.biodata_id == user.biodata_id).FirstOrDefault();
            M_doctor doctor = _MiniProjDbContext.M_doctors.Where(o => o.biodata_id == user.biodata_id).FirstOrDefault();

            if (user != null)
            {
                if (user.password != userLogin.password)
                {
                    int attempt = (int)user.login_attempt;
                    user.login_attempt = ++attempt;

                    if (attempt > 2)
                    {
                        user.is_locked = true;
                    }
                    else
                    {
                        user.is_locked = false;
                    }

                    result = _MiniProjDbContext.M_users
                        .Where(o => o.email == userLogin.email)
                        .Select(o => new UserViewModel
                        {
                            id = o.id,
                            fullname = o.m_biodata.fullname,
                            biodata_id = (int)o.biodata_id,
                            customer_id = 0,
                            doctor_id = 0,
                            roleName = o.m_role.name,
                            email = o.email,
                            login_attempt = (int)user.login_attempt,
                            is_locked = (bool)o.is_locked,
                            last_login = (DateTime)user.last_login
                        }).FirstOrDefault();
                }
                else
                {
                    if (customer != null && user.role_id != 1)
                    {
                        user.login_attempt = 0;
                        user.last_login = DateTime.Now;
                        result = (from cs in _MiniProjDbContext.M_customers
                                  join us in _MiniProjDbContext.M_users
                                  on cs.biodata_id equals us.biodata_id into usercust

                                  from uscs in usercust
                                  where uscs.email == userLogin.email && uscs.password == userLogin.password
                                  select new UserViewModel
                                  {
                                      id = uscs.id,
                                      fullname = uscs.m_biodata.fullname,
                                      biodata_id = (int)uscs.biodata_id,
                                      customer_id = customer.id,
                                      doctor_id = 0,
                                      roleName = uscs.m_role.name,
                                      email = uscs.email,
                                      login_attempt = (int)user.login_attempt,
                                      is_locked = (bool)uscs.is_locked,
                                      last_login = (DateTime)user.last_login
                                  }).FirstOrDefault();
                    }
                    else if (doctor != null && user.role_id != 1)
                    {
                        user.login_attempt = 0;
                        user.last_login = DateTime.Now;
                        result = (from dc in _MiniProjDbContext.M_doctors
                                  join us in _MiniProjDbContext.M_users
                                  on dc.biodata_id equals us.biodata_id into userdoct

                                  from usdc in userdoct
                                  where usdc.email == userLogin.email && usdc.password == userLogin.password
                                  select new UserViewModel
                                  {
                                      id = usdc.id,
                                      fullname = usdc.m_biodata.fullname,
                                      biodata_id = (int)usdc.biodata_id,
                                      customer_id = 0,
                                      doctor_id = doctor.id,
                                      roleName = usdc.m_role.name,
                                      email = usdc.email,
                                      login_attempt = (int)user.login_attempt,
                                      is_locked = (bool)usdc.is_locked,
                                      last_login = (DateTime)user.last_login
                                  }).FirstOrDefault();
                    }
                    else
                    {
                        user.login_attempt = 0;
                        user.last_login = DateTime.Now;
                        result = _MiniProjDbContext.M_users
                                 .Where(o => o.email == userLogin.email && o.password == userLogin.password)
                                 .Select(o => new UserViewModel
                                 {
                                     id = o.id,
                                     fullname = o.m_biodata.fullname,
                                     biodata_id = (int)o.biodata_id,
                                     customer_id = 0,
                                     doctor_id = 0,
                                     roleName = o.m_role.name,
                                     email = o.email,
                                     login_attempt = (int)user.login_attempt,
                                     is_locked = (bool)o.is_locked,
                                     last_login = (DateTime)user.last_login
                                 }).FirstOrDefault();
                    }

                    //result = new UserViewModel()
                    //{
                    //    id = user.id,
                    //    fullname = user.m_biodata.fullname,
                    //    roleName = user.m_role.name,
                    //    email = user.email,
                    //    login_attempt = (int)user.login_attempt,
                    //    is_locked = (bool)user.is_locked,
                    //    last_login = (DateTime)user.last_login
                    //};
                }
                _MiniProjDbContext.SaveChanges();
            }
            else
            {
                result = null;
            }
            return result;
        }

        public List<string> Authorization(string roles)
        {
            List<string> result = new List<string>();
            var list = (from _email in _MiniProjDbContext.M_users
                        join _roles in _MiniProjDbContext.M_roles
                        on _email.role_id equals _roles.id into EmailRoles

                        from er in EmailRoles.DefaultIfEmpty()
                        where er.name == roles
                        select new { er.name }
                        ).ToList();

            foreach (var item in list)
                result.Add(item.name);

            return result;
        }
    }
}
