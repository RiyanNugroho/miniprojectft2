﻿using System.Collections.Generic;
using System;
using ViewModel;
using WebApi.DataModels;
using System.Linq;

namespace WebApi.Repositories
{
    public class RoleRepository : IRepository<RoleViewModel>
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();

        private ResponseResult result = new ResponseResult();

        private long _UserName;

        public RoleRepository()
        {
            _UserName = 1;
        }

        public RoleRepository(long username)
        {
            _UserName = username;
        }

        public ResponseResult Create(RoleViewModel entity)
        {
            try
            {
                M_role item = new M_role();
                item.name = entity.name;



                _MiniProjDbContext.M_roles.Add(item);
                _MiniProjDbContext.SaveChanges();

                result.Entity = item;
            }
            catch (Exception e)
            {
                result.Entity = entity;
                result.Message = e.Message;
                result.Success = false;

            }
            return result;
        }

        public ResponseResult Delete(RoleViewModel entity)
        {
            try
            {
                M_role item = _MiniProjDbContext.M_roles
                    .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                if (item != null)
                {
                    result.Entity = item;
                    _MiniProjDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Update(RoleViewModel entity)
        {
            try
            {
                M_role item = _MiniProjDbContext.M_roles
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                if (item != null)
                {
                    item.name = entity.name;



                    _MiniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<RoleViewModel> GetAll()
        {
            List<RoleViewModel> result = new List<RoleViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.M_roles
                          select new RoleViewModel
                          {
                              id = o.id,
                              name = o.name,
                              code = o.code,
                          }).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;

        }

        public RoleViewModel GetById(long id)
        {
            RoleViewModel result = new RoleViewModel();
            try
            {
                result = (from o in _MiniProjDbContext.M_roles
                          select new RoleViewModel
                          {
                              id = o.id,
                              name = o.name,
                              code = o.code,
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;

        }

        public List<RoleViewModel> ByFilter(string search)
        {

            List<RoleViewModel> result = new List<RoleViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.M_roles
                          .Where(o => o.name.Contains(search))
                          select new RoleViewModel
                          {
                              id = o.id,
                              name = o.name,
                              code = o.code
                          }).ToList();
            }
            catch (Exception e)
            {

                string error = e.Message;
            }
            return result;
        }
    }
}
