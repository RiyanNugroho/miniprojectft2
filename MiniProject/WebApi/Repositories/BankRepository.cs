﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using ViewModel;
using WebApi.DataModels;
using System;
using System.Net.Http;

namespace WebApi.Repositories
{
    public class BankRepository : IRepository<BankViewModel>
    {
        private MiniProjDbContext _miniProjDbContext = new MiniProjDbContext();

        private ResponseResult result = new ResponseResult();

        private long _UserName;
        private string _userName;

        public BankRepository()
        {
            _UserName = 1;
        }

        public BankRepository(string userName)
        {
            _userName = userName;
        }

        public BankRepository(long userId)
        {
            _UserName = userId;
        }


        public ResponseResult Create(BankViewModel entity)
        {

            //string newvac = va_code();
            try
            {
                //if (newvac != "")
                //{
                //    entity.va_code = newvac;


                M_bank item = new M_bank();
                item.name = entity.name;
                item.va_code = entity.va_code;
                entity.dibuat = _userName;
                item.created_by = _UserName;
                item.created_on = DateTime.Now;

                M_bank check = _miniProjDbContext.M_banks.Where(o => o.name == entity.name && !o.is_delete).FirstOrDefault();

                if (check != null)
                {
                    if (item.name == check.name && ! check.is_delete)
                    {
                        result.Entity = entity;
                        result.Message = "Data Bank Sudah Tersedia!";
                        result.Success = false;
                    }
                    else
                    {
                        _miniProjDbContext.M_banks.Add(item);
                        //check.name = check.name + "_old";
                        _miniProjDbContext.SaveChanges();
                        result.Entity = check;
                    }
                }
                else
                {
                    _miniProjDbContext.M_banks.Add(item);
                    _miniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
                // }
                // else
                // {
                //     result.Success = false;
                //     result.Message = "Error Va Code!";
                // }

            }
            catch (Exception e)
            {

                result.Entity = entity;
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(BankViewModel entity)
        {
            try
            {
                M_bank item = _miniProjDbContext.M_banks
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();
                if (item != null)
                {
                    item.is_delete = true;

                    item.deleted_by = _UserName;
                    item.deleted_on = DateTime.Now;

                    result.Entity = item;
                    _miniProjDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Data Bank Tidak Ditemukan!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {

                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<BankViewModel> GetAll()
        {
            throw new NotImplementedException();
            //List<BankViewModel> result = new List<BankViewModel>();
            //try
            //{
            //    result = (from o in _miniProjDbContext.Banks
            //              where o.is_delete != true
            //              select new BankViewModel
            //              {
            //                  id = o.id,
            //                  name = o.name,
            //                  va_code = o.va_code
            //              }).ToList();
            //}
            //catch (Exception e)
            //{

            //    string error = e.Message;
            //}
            //return result;
        }

        public BankViewModel GetById(long id)
        {
            BankViewModel result = new BankViewModel();
            try
            {
                result = (from o in _miniProjDbContext.M_banks
                          where o.id == id && o.is_delete != true
                          select new BankViewModel
                          {
                              id = o.id,
                              name = o.name,
                              va_code = o.va_code
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {

                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(BankViewModel entity)
        {
            try
            {
                M_bank item = _miniProjDbContext.M_banks
                    .Where(o => o.id == entity.id)
                     .FirstOrDefault();

                 M_bank check = _miniProjDbContext.M_banks.Where(o => o.name == entity.name && !o.is_delete).FirstOrDefault();
                if (item != null)
                {
                    item.id = entity.id;
                    item.name = entity.name;
                    item.va_code = entity.va_code;

                    item.modified_by = _UserName;
                    item.modified_on = DateTime.Now;


                    if (check != null)
                    {
                        result.Message = "Data Bank yang diubah sudah tersedia!";
                        result.Success = false;
                    }
                    else
                    {

                        _miniProjDbContext.SaveChanges();
                        result.Entity = item;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = "Data Bank Tidak Ditemukan!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {

                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }



        //public List<BankViewModel> ByFilter(string search)
        //{
        //    List<BankViewModel> result = new List<BankViewModel>();
        //    try
        //    {
        //        result = (from o in _miniProjDbContext.Banks
        //                  .Where(o => o.is_delete != true && o.name.Contains(search))
        //                  select new BankViewModel
        //                  {
        //                      id = o.id,
        //                      name = o.name,
        //                      va_code = o.va_code
        //                  }).ToList();
        //    }
        //    catch (Exception e)
        //    {

        //        string error = e.Message;
        //    }
        //    return result;
        //}

        public async Task<ResponseResult> All(int page, int rows, string search, string column, bool ascen)
        {
            // Page
            // Berapa baris @ page
            // Total 105. Req page 2, 10 row @ page
            ResponseResult result = new ResponseResult();
            int rowNum = 0;

            try
            {
                var query = _miniProjDbContext.M_banks
                         .Where(o => o.name.Contains(search));

               
                    query = query.Where(o => o.is_delete == false);
              

                switch (column)
                {
                    case "name":
                        query = !ascen ? query.OrderByDescending(o => o.name) : query.OrderBy(o => o.name);
                        break;
                    case "va_code":
                        query = !ascen ? query.OrderByDescending(o => o.va_code) : query.OrderBy(o => o.va_code);
                        break;
                    default:
                        break;
                }

                result.Entity = query
                    .Skip((page - 1) * rows)
                    .Take(rows)
                      .Select(o => new BankViewModel
                      {
                          id = o.id,
                          name = o.name,
                          va_code = o.va_code,

                          dibuat = _userName,
                          created_by = o.created_by
                      }).ToList();
                //result = query
                result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);
                if (result.Pages < page && result.Pages > 0)
                {
                    return await All(1, rows, search, column, ascen);
                }

            }
            catch (Exception e)
            {
                result.Success = false;
                string error = e.Message;
            }

            return result;
        }

        //private string va_code()
        //{
        //    string newvac = (DateTime.Now.ToString("yy") + DateTime.Now.Month.ToString("D2")) + "-";

        //    try
        //    {
        //        var thevac = _miniProjDbContext.Banks
        //            .Where(o => o.va_code.Contains(newvac))
        //            .OrderByDescending(o => o.va_code)
        //            .FirstOrDefault();
        //        if (thevac != null)
        //        {
        //            string[] vacod = thevac.va_code.Split('-');

        //            int newInc = int.Parse(vacod[1]) + 1;
        //            newvac += newInc.ToString("D4");

        //        }
        //        else
        //        {
        //            newvac += "0001";
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        string error = e.Message;
        //        newvac = "";
        //    }
        //    return newvac;
        //}


    }
}
