﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;
using WebApi.Repositories;


namespace WebApi.Repositories
{
    public class KategoriFasilitasKesehatanRepository : IRepository<FacilityCategoryViewModel>
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();

        private ResponseResult result = new ResponseResult();

        private long _UserName;

        public KategoriFasilitasKesehatanRepository()
        {
            _UserName = 1;
        }

        public KategoriFasilitasKesehatanRepository(long userId)
        {
            _UserName = userId;
        }


        public List<FacilityCategoryViewModel> ByFilter(string search)
        {
            List<FacilityCategoryViewModel> result = new List<FacilityCategoryViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.M_medical_facility_categories
                          .Where(o => o.is_delete != true && o.name.Contains(search))
                          select new FacilityCategoryViewModel
                          {
                              id = o.id,
                              name = o.name
                              
                          }).ToList();
            }
            catch (Exception e)
            {

                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Create(FacilityCategoryViewModel entity)
        {
            try
            {
                M_medical_facility_category item = new M_medical_facility_category();
                item.name = entity.name;

                item.created_by = _UserName;
                item.created_on = DateTime.Now;

                M_medical_facility_category check = _MiniProjDbContext.M_medical_facility_categories.Where(o => o.name == entity.name && !o.is_delete).FirstOrDefault();

                if (check != null)
                {
                    if (item.name == check.name && !check.is_delete)
                    {
                        result.Entity = entity;
                        result.Message = "Item Sudah Tersedia!";
                        result.Success = false;
                    }
                    else
                    {
                        _MiniProjDbContext.M_medical_facility_categories.Add(item);
                        //check.name = check.name + "_old";
                        _MiniProjDbContext.SaveChanges();
                        result.Entity = check;
                    }
                }
                else
                {
                    _MiniProjDbContext.M_medical_facility_categories.Add(item);
                    _MiniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
            }
            catch (Exception e)
            {
                result.Entity = entity;
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }      

        public ResponseResult Delete(FacilityCategoryViewModel entity)
        {
            try
            {
                M_medical_facility_category item = _MiniProjDbContext.M_medical_facility_categories
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                if (item != null)
                {
                    item.is_delete = true;
                    item.deleted_by = _UserName;
                    item.deleted_on = DateTime.Now;

                    result.Entity = item;
                    _MiniProjDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<FacilityCategoryViewModel> GetAll()
        {
            List<FacilityCategoryViewModel> result = new List<FacilityCategoryViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.M_medical_facility_categories
                          where o.is_delete != true
                          select new FacilityCategoryViewModel
                          {
                              id = o.id,
                              name = o.name
                          }).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public FacilityCategoryViewModel GetById(long id)
        {
            FacilityCategoryViewModel result = new FacilityCategoryViewModel();
            try
            {
                result = (from o in _MiniProjDbContext.M_medical_facility_categories
                          where o.id == id && o.is_delete != true
                          select new FacilityCategoryViewModel
                          {
                              id = o.id,
                              name = o.name,
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }
        
        public ResponseResult Update(FacilityCategoryViewModel entity)
        {
            try
            {
                M_medical_facility_category item = _MiniProjDbContext.M_medical_facility_categories
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();

                M_medical_facility_category check = _MiniProjDbContext.M_medical_facility_categories.Where(o => o.name == entity.name && !o.is_delete).FirstOrDefault();

                if (item != null)
                {
                    item.name = entity.name;

                    item.modified_by = _UserName;
                    item.modified_on = DateTime.Now;

                    if (check != null)
                    {
                        result.Message = "Item yang diubah sudah tersedia!";
                        result.Success = false;
                    }
                    else
                    {
                        _MiniProjDbContext.SaveChanges();
                        result.Entity = item;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }
    }
}
