﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;
using WebApi.Repositories;

namespace WebApi.Repositories
{
    public class DoctorTreatmentRepository
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();

        private ResponseResult result = new ResponseResult();

        private long _UserName;

        public DoctorTreatmentRepository()
        {
            _UserName = 1;
        }

        public DoctorTreatmentRepository(long userId)
        {
            _UserName = userId;
        }

        public List<DoctorTreatmentViewModel> ByFilter(string search)
        {
            List<DoctorTreatmentViewModel> result = new List<DoctorTreatmentViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.T_doctor_treatments
                          .Where(o => o.is_delete != true && o.name.Contains(search))
                          select new DoctorTreatmentViewModel
                          {
                              id = o.id,
                              doctor_id = o.doctor_id,
                              name = o.name
                          }).ToList();
            }
            catch (Exception e)
            {

                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Create(DoctorTreatmentViewModel entity)
        {
            try
            {
                T_doctor_treatment item = new T_doctor_treatment();
                item.id = entity.id;
                item.doctor_id = entity.doctor_id;
                item.name = entity.name;

                item.created_by = _UserName;
                item.created_on = DateTime.Now;

                T_doctor_treatment check = _MiniProjDbContext.T_doctor_treatments.Where(o => o.name == entity.name && !o.is_delete).FirstOrDefault();


                if (check != null)
                {
                    if (item.name == check.name && !check.is_delete)
                    {
                        result.Entity = entity;
                        result.Message = "Item Sudah Tersedia!";
                        result.Success = false;
                    }
                    else
                    {
                        _MiniProjDbContext.T_doctor_treatments.Add(item);
                        //check.name = check.name + "_old";
                        _MiniProjDbContext.SaveChanges();
                        result.Entity = check;
                    }
                }
                else
                {
                    _MiniProjDbContext.T_doctor_treatments.Add(item);
                    _MiniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
            }
            catch (Exception e)
            {
                result.Entity = entity;
                result.Message = e.Message;
                result.Success = false;

            }
            return result;
        }

        public ResponseResult Delete(DoctorTreatmentViewModel entity)
        {
            try
            {
                T_doctor_treatment item = _MiniProjDbContext.T_doctor_treatments
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                if (item != null)
                {
                    item.is_delete = true;
                    item.deleted_by = _UserName;
                    item.deleted_on = DateTime.Now;

                    result.Entity = item;
                    _MiniProjDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<DoctorTreatmentViewModel> GetAll()
        {
            List<DoctorTreatmentViewModel> result = new List<DoctorTreatmentViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.T_doctor_treatments
                          where o.is_delete != true
                          select new DoctorTreatmentViewModel
                          {
                              id = o.id,
                              doctor_id = o.doctor_id,
                              name = o.name,
                          }).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public DoctorTreatmentViewModel GetById(long id)
        {
            DoctorTreatmentViewModel result = new DoctorTreatmentViewModel();
            try
            {
                result = (from o in _MiniProjDbContext.T_doctor_treatments
                          where o.id == id && o.is_delete != true
                          select new DoctorTreatmentViewModel
                          {
                              id = o.id,
                              doctor_id = o.doctor_id,
                              name = o.name,
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(DoctorTreatmentViewModel entity)
        {
            try
            {
                T_doctor_treatment item = _MiniProjDbContext.T_doctor_treatments
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();

                T_doctor_treatment check = _MiniProjDbContext.T_doctor_treatments.Where(o => o.name == entity.name && !o.is_delete).FirstOrDefault();

                if (item != null)
                {
                    item.id = entity.id;
                    item.doctor_id = entity.doctor_id;
                    item.name = entity.name;

                    item.created_by = _UserName;
                    item.created_on = DateTime.Now;

                    item.modified_by = _UserName;
                    item.modified_on = DateTime.Now;

                    if (check != null)
                    {
                        result.Message = "Item yang diubah sudah tersedia!";
                        result.Success = false;
                    }
                    else
                    {
                        _MiniProjDbContext.SaveChanges();
                        result.Entity = item;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }
    }
}
