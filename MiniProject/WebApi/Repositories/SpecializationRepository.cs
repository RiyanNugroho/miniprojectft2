﻿using System;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class SpecializationRepository : IRepository<SpecializationViewModel>
    {
        private MiniProjDbContext _miniProjDbContext = new MiniProjDbContext();
        private ResponseResult result = new ResponseResult();
        private long _UserName;

        public SpecializationRepository()
        {
            _UserName = 1;
        }

        public SpecializationRepository(long userName)
        {
            _UserName = userName;
        }

        public ResponseResult Create(SpecializationViewModel entity)
        {
            try
            {
                M_specialization item = new M_specialization();
                item.name = entity.name;


                item.created_by = _UserName;
                item.created_on = DateTime.Now;

                _miniProjDbContext.M_specializations.Add(item);
                _miniProjDbContext.SaveChanges();

                result.Entity = item;
            }
            catch (Exception e)
            {
                result.Entity = entity;
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(SpecializationViewModel entity)
        {
            try
            {
                M_specialization item = _miniProjDbContext.M_specializations
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();
                if (item != null)
                {
                   // item.is_delete = true;
                    item.deleted_by = _UserName;
                    item.deleted_on = DateTime.Now;

                    result.Entity = item;
                    _miniProjDbContext.SaveChanges();

                }
                else
                {
                    result.Success = false;
                    result.Message = "Not found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<SpecializationViewModel> GetAll()
        {
            List<SpecializationViewModel> result = new List<SpecializationViewModel>();
            try
            {
                result = (from o in _miniProjDbContext.M_specializations
                          .Where(o => o.is_delete != true)
                          select new SpecializationViewModel
                          {
                              id = o.id,
                              name = o.name
                          }).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public SpecializationViewModel GetById(long id)
        {
            SpecializationViewModel result = new SpecializationViewModel();
            try
            {
                result = (from o in _miniProjDbContext.M_specializations
                          where o.id == id && o.is_delete != true
                          select new SpecializationViewModel
                          {
                              id = o.id,
                              name = o.name
                          }).FirstOrDefault();

            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(SpecializationViewModel entity)
        {
            try
            {
                M_specialization item = _miniProjDbContext.M_specializations
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();
                if (item != null)
                {
                    item.id = entity.id;
                    item.name = entity.name;

                    item.modified_by = _UserName;
                    item.modified_on = DateTime.Now;


                    _miniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }
    }
}
