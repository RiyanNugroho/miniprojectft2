﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;
using WebApi.Repositories;

namespace WebApi.Repositories
{
    public class DoctorRepository
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();

        private ResponseResult result = new ResponseResult();

        private long _UserName;

        public DoctorRepository()
        {
            _UserName = 1;
        }

        public DoctorRepository(long userId)
        {
            _UserName = userId;
        }


        public List<DoctorViewModel> ByFilter(string search)
        {
            List<DoctorViewModel> result = new List<DoctorViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.M_doctors
                          .Where(o => o.is_delete != true && o.M_Biodata.fullname.Contains(search))
                          select new DoctorViewModel
                          {
                              id = o.id,
                              biodata_id = o.biodata_id,
                              str = o.str

                          }).ToList();
            }
            catch (Exception e)
            {

                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Create(DoctorViewModel entity)
        {
            try
            {
                M_doctor item = new M_doctor();
                item.biodata_id = entity.biodata_id;
                item.str = entity.str;

                item.created_by = _UserName;
                item.created_on = DateTime.Now;

                _MiniProjDbContext.M_doctors.Add(item);
                _MiniProjDbContext.SaveChanges();

                result.Entity = item;
            }
            catch (Exception e)
            {
                result.Entity = entity;
                result.Message = e.Message;
                result.Success = false;

            }
            return result;
        }

        public ResponseResult Delete(DoctorViewModel entity)
        {
            try
            {
                M_doctor item = _MiniProjDbContext.M_doctors
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                if (item != null)
                {
                    item.is_delete = true;
                    item.deleted_by = _UserName;
                    item.deleted_on = DateTime.Now;

                    result.Entity = item;
                    _MiniProjDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<DoctorViewModel> GetAll()
        {
            List<DoctorViewModel> result = new List<DoctorViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.M_doctors
                          where o.is_delete != true
                          select new DoctorViewModel
                          {
                              id = o.id,
                              biodata_id = o.biodata_id,
                              str = o.str,
                              Nama_Dokter = o.M_Biodata.fullname
                          }).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public DoctorViewModel GetById(long id)
        {
            DoctorViewModel result = new DoctorViewModel();
            try
            {
                result = (from o in _MiniProjDbContext.M_doctors
                          where o.id == id && o.is_delete != true
                          select new DoctorViewModel
                          {
                              id = o.id,
                              biodata_id = o.biodata_id,
                              str = o.str,
                              Nama_Dokter = o.M_Biodata.fullname
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(DoctorViewModel entity)
        {
            try
            {
                M_doctor item = _MiniProjDbContext.M_doctors
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                if (item != null)
                {
                    item.biodata_id = entity.biodata_id;
                    item.str = entity.str;

                    item.created_by = _UserName;
                    item.created_on = DateTime.Now;

                    item.modified_by = _UserName;
                    item.modified_on = DateTime.Now;

                    _MiniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public async Task<ResponseResult> All(int page, int rows, string search)
        {
            // Page
            // Berapa baris @ page
            // Total 105. Req page 2, 10 row @ page
            ResponseResult result = new ResponseResult();
            int rowNum = 0;

            try
            {   //bila sudah ada token
                //var query = _MiniProjDbContext.M_medical_items
                //         .Where(o => o.name.Contains(search));

                //if (_userName != 1)
                //{
                //    query = query.Where(o => o.is_delete == false);
                //}

                //bila tidak pakai token
                var query = _MiniProjDbContext.M_doctors
                         .Where(o => o.is_delete != true && o.M_Biodata.fullname.Contains(search));

                result.Entity = query
                    .Skip((page - 1) * rows)
                    .Take(rows)
                      .Select(o => new DoctorViewModel
                      {
                          id = o.id,
                          biodata_id = o.biodata_id,
                          str = o.str,
                          Nama_Dokter = o.M_Biodata.fullname
                      }).ToList();
                //result = query
                result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                if (result.Pages < page && result.Pages > 0)
                {
                    return await All(1, rows, search);
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                string error = e.Message;
            }
            return result;
        }
    }
}
