﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class itemSegmentationRepository : IRepository<ItemSegmentationViewModel>
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();
        private ResponseResult result = new ResponseResult();
        private long _userName;
        public itemSegmentationRepository()
        {
            _userName = 1;
        }
        public itemSegmentationRepository(long userId)
        {
            _userName = userId;
        }
        public ResponseResult Create(ItemSegmentationViewModel entity)
        {
            try
            {
                M_medical_item_segmentation item = new M_medical_item_segmentation();
                item.name = entity.name;

                item.created_by = _userName;
                item.created_on = DateTime.Now;

                M_medical_item_segmentation check = _MiniProjDbContext.M_medical_item_segmentations.Where(o => o.name == entity.name && !o.is_delete).FirstOrDefault();

                if (check != null)
                {
                    if (item.name == check.name && !check.is_delete)
                    {
                        result.Entity = entity;
                        result.Message = "Nama Sudah Tersedia!";
                        result.Success = false;
                    }
                    else
                    {
                        _MiniProjDbContext.M_medical_item_segmentations.Add(item);
                        //check.name = check.name + "_old";
                        _MiniProjDbContext.SaveChanges();
                        result.Entity = check;
                    }
                }
                else
                {
                    _MiniProjDbContext.M_medical_item_segmentations.Add(item);
                    _MiniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
            }
            catch (Exception x)
            {
                result.Entity = entity;
                result.Message = x.Message;
                result.Success = false;
            }
            return result;


        }

        public ResponseResult Delete(ItemSegmentationViewModel entity)
        {
            try
            {
                M_medical_item_segmentation item = _MiniProjDbContext.M_medical_item_segmentations.Where(o => o.id == entity.id).FirstOrDefault();
                if (item != null)
                {
                    item.is_delete = true;
                    item.deleted_by = _userName;
                    item.deleted_on = DateTime.Now;

                    result.Entity = item;
                    //_MiniProjDbContext.M_medical_item_segmentations.Remove(item);
                    _MiniProjDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Nama Tidak Ditemukan";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {

                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<ItemSegmentationViewModel> GetAll()
        {
            throw new NotImplementedException();

            //List<ItemSegmentationViewModel> result = new List<ItemSegmentationViewModel>();
            //try
            //{
            //    result = (from o in _MiniProjDbContext.M_medical_item_segmentations
            //              where o.is_delete != true
            //              select new ItemSegmentationViewModel
            //              {
            //                  id = o.id,
            //                  name = o.name
            //              }
            //             ).ToList();
            //}
            //catch (Exception e)
            //{
            //    string error = e.Message;
            //}
            //return result;
        }

        public ItemSegmentationViewModel GetById(long id)
        {
            ItemSegmentationViewModel result = new ItemSegmentationViewModel();
            try
            {
                result = (from o in _MiniProjDbContext.M_medical_item_segmentations
                          where o.id == id && o.is_delete != true
                          select new ItemSegmentationViewModel
                          {
                              id = o.id,
                              name = o.name
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(ItemSegmentationViewModel entity)
        {
            try
            {
                M_medical_item_segmentation item = _MiniProjDbContext.M_medical_item_segmentations
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();

                M_medical_item_segmentation check = _MiniProjDbContext.M_medical_item_segmentations.Where(o => o.name == entity.name && !o.is_delete).FirstOrDefault();

                if (item != null)
                {
                    item.name = entity.name;

                    item.modified_by = _userName;
                    item.modified_on = DateTime.Now;

                    if (check != null)
                    {
                        result.Message = "Nama Tersebut Sudah Tersedia!";
                        result.Success = false;
                    }
                    else
                    {
                        _MiniProjDbContext.SaveChanges();
                        result.Entity = item;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = "Nama Tidak Ditemukan!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;

        }
        //public List<ItemSegmentationViewModel> ByFilter(string search)
        //{
        //    List<ItemSegmentationViewModel> result = new List<ItemSegmentationViewModel>();
        //    try
        //    {
        //        result = (from o in _MiniProjDbContext.M_medical_item_segmentations
        //                  .Where(o => o.is_delete != true && o.name.Contains(search))
        //                  select new ItemSegmentationViewModel
        //                  {
        //                      id = o.id,
        //                      name = o.name
        //                  }).ToList();
        //    }
        //    catch (Exception e)
        //    {

        //        string error = e.Message;
        //    }
        //    return result;
        //}
        public async Task<ResponseResult> All(int page, int rows, string search)
        {
            // Page
            // Berapa baris @ page
            // Total 105. Req page 2, 10 row @ page
            ResponseResult result = new ResponseResult();
            int rowNum = 0;

            try
            {
                //bila sudah ada token
                var query = _MiniProjDbContext.M_medical_item_segmentations
                         .Where(o => o.name.Contains(search));

                if (_userName != 1)
                {
                    query = query.Where(o => o.is_delete == false);
                }

                //bila tidak ada token
                //var query = _MiniProjDbContext.M_medical_item_segmentations
                //          .Where(o => o.is_delete != true && o.name.Contains(search));

                result.Entity = query
                    .Skip((page - 1) * rows)
                    .Take(rows)
                      .Select(o => new ItemSegmentationViewModel
                      {
                          id = o.id,
                          name = o.name,
                          is_delete = o.is_delete
                      }).ToList();
                //result = query
                result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                if (result.Pages < page && result.Pages > 0)
                {
                    return await All(1, rows, search);
                }
                

            }
            catch (Exception e)
            {
                result.Success = false;
                string error = e.Message;
            }

            return result;
        }
    }
}
