﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ViewModel;
using WebApi.DataModels;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace WebApi.Repositories
{
    public class M_blood_groupRepository : IRepository<BloodGroupViewModel>
    {
        private MiniProjDbContext _miniProjDbContext = new MiniProjDbContext();
        private ResponseResult result = new ResponseResult();
        private long _UserName;

        public M_blood_groupRepository()
        {
            _UserName = 1;
        }

        public M_blood_groupRepository(long userName)
        {
            _UserName = userName;
        }

        public ResponseResult Create(BloodGroupViewModel entity)
        {
            try
            {
                M_blood_group item = new M_blood_group();
                item.code = entity.code;
                item.description = entity.description;


                item.created_by = _UserName;
                item.created_on = DateTime.Now;

                M_blood_group check = _miniProjDbContext.M_blood_groups.Where(o => o.code == entity.code && !o.is_delete).FirstOrDefault();
                if (check != null)
                {
                    if (item.code == check.code && !check.is_delete)
                    {
                        result.Entity = entity;
                        result.Message = "Data sudah ada";
                        result.Success = false;
                    }
                    else
                    {
                        _miniProjDbContext.M_blood_groups.Add(item);
                        _miniProjDbContext.SaveChanges();
                        result.Entity = item;
                    }
                }
                else
                {
                    _miniProjDbContext.M_blood_groups.Add(item);
                    _miniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
            }
            catch (Exception e)
            {
                result.Entity = entity;
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(BloodGroupViewModel entity)
        {
            try
            {
                M_blood_group item = _miniProjDbContext.M_blood_groups
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();
                if (item != null)
                {
                    item.is_delete = true;
                    item.deleted_by = _UserName;
                    item.deleted_on = DateTime.Now;

                    result.Entity = item;
                    _miniProjDbContext.SaveChanges();

                }
                else
                {
                    result.Success = false;
                    result.Message = "Not found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<BloodGroupViewModel> GetAll()
        {
            List<BloodGroupViewModel> result = new List<BloodGroupViewModel>();
            try
            {
                result = (from o in _miniProjDbContext.M_blood_groups
                          .Where(o => o.is_delete != true)
                          select new BloodGroupViewModel
                          {
                              id = o.id,
                              code = o.code,
                              description = o.description
                          }).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public BloodGroupViewModel GetById(long id)
        {
            BloodGroupViewModel result = new BloodGroupViewModel();
            try
            {
                result = (from o in _miniProjDbContext.M_blood_groups
                          where o.id == id && o.is_delete != true
                          select new BloodGroupViewModel
                          {
                              id = o.id,
                              code = o.code,
                              description = o.description
                          }).FirstOrDefault();

            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(BloodGroupViewModel entity)
        {
            try
            {
                M_blood_group item = _miniProjDbContext.M_blood_groups
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();
                if (item != null)
                {
                    item.id = entity.id;
                    item.code = entity.code;
                    item.description = entity.description;

                    item.modified_by = _UserName;
                    item.modified_on = DateTime.Now;

                    M_blood_group check = _miniProjDbContext.M_blood_groups.Where(o => o.code == entity.code && !o.is_delete).FirstOrDefault();

                    if(check != null)
                    {
                        result.Message = "Data Yang Diubah Sudah Ada";
                        result.Success = false;
                    }
                    else
                    {
                        _miniProjDbContext.SaveChanges();
                        result.Entity = item;
                    }                                        
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }

        public List<BloodGroupViewModel> ByFilter(string search)
        {
            List<BloodGroupViewModel> result = new List<BloodGroupViewModel>();

            try
            {
                result = (from o in _miniProjDbContext.M_blood_groups
                          .Where(o => o.is_delete != true && (o.code.Contains(search) || o.description.Contains(search)))
                          select new BloodGroupViewModel
                          {
                              id = o.id,
                              code = o.code,
                              description = o.description
                          }).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;

        }

    }
}
