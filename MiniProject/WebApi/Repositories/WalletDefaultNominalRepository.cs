﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class WalletDefaultNominalRepository : IRepository<WalletDefaultNominalViewModel>
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();
        private ResponseResult result = new ResponseResult();
        private long _userName;
        public WalletDefaultNominalRepository()
        {
            _userName = 1;
        }
        public WalletDefaultNominalRepository(long userId)
        {
            _userName = userId;
        }

        public ResponseResult Create(WalletDefaultNominalViewModel entity)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Delete(WalletDefaultNominalViewModel entity)
        {
            throw new NotImplementedException();
        }

        public List<WalletDefaultNominalViewModel> GetAll()
        {
            //throw new NotImplementedException();

            List<WalletDefaultNominalViewModel> result = new List<WalletDefaultNominalViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.M_wallet_default_nominals
                          select new WalletDefaultNominalViewModel
                          {
                              id = o.id,
                              //nominal = o.nominal
                          }
                         ).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public WalletDefaultNominalViewModel GetById(long id)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Update(WalletDefaultNominalViewModel entity)
        {
            throw new NotImplementedException();
        }
    }
}
