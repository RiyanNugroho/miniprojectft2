﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;
using WebApi.Repositories;

namespace WebApi.Repositories
{
    public class PasienRepository : IRepository<PasienViewModel>
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();
        private ResponseResult result = new ResponseResult();
        private long _userId;

        public PasienRepository()
        {
            _userId = 5;
        }

        public PasienRepository(long userId)
        {
            _userId = userId;
        }

        public ResponseResult ByParent1(long id)
        {
            try
            {
                result.Entity = (from o in _MiniProjDbContext.M_customers
                                 where o.blood_group_id == id
                                 select new Customer
                                 {
                                     id = o.id,
                                     biodata_id = o.biodata_id,
                                     fullname = o.M_biodata.fullname,
                                     blood_group_id = o.blood_group_id,
                                     bloodname = o.M_blood_group.code,
                                     rhesus_type = o.rhesus_type,
                                     height = o.height,
                                     weight = o.weight
                                 }).ToList();
            }
            catch (Exception x)
            {
                result.Success = false;
                result.Message = x.Message;
            }
            return result;
        }

        public ResponseResult ByParent2(long id)
        {
            try
            {
                result.Entity = (from o in _MiniProjDbContext.M_customer_members
                                 where o.customer_relation_id == id
                                 select new PasienViewModel
                                 {
                                     id = o.id,
                                     customer_id = (long)o.customer_id,
                                     customer_relation_id = (long)o.customer_relation_id
                                 }).ToList();
            }
            catch (Exception x)
            {
                result.Success = false;
                result.Message = x.Message;
            }
            return result;
        }

        public ResponseResult Create(PasienViewModel entity)
        {
            try
            {
                M_customer_member item1 = new M_customer_member();
                M_biodata item2 = new M_biodata();
                M_customer item3 = new M_customer();

                //item1.customer_id = _userId;
                item2.fullname = entity.fullname;
                item3.dob = entity.dob;
                item3.gender = entity.gender;
                item3.blood_group_id = entity.blood_group_id;
                item3.rhesus_type = entity.rhesus_type;
                item3.height = (decimal)entity.height;
                item3.weight = (decimal)entity.weight;
                item1.customer_relation_id = entity.customer_relation_id;
                    
                item1.created_by = _userId;
                item2.created_by = _userId;
                item3.created_by = _userId;
                item1.created_on = DateTime.Now;
                item2.created_on = DateTime.Now;
                item3.created_on = DateTime.Now;

                _MiniProjDbContext.M_customer_members.Add(item1);
                _MiniProjDbContext.M_biodatas.Add(item2);
                _MiniProjDbContext.M_customers.Add(item3);
                _MiniProjDbContext.SaveChanges();

                result.Entity = entity;
            }
            catch (Exception x)

            {
                result.Entity = entity;
                result.Message = x.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(PasienViewModel entity)
        {
            try
            {
                M_customer_member item = _MiniProjDbContext.M_customer_members
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();
                if (item != null)
                {
                    item.is_delete = true;
                    item.deleted_by = _userId;
                    item.deleted_on = DateTime.Now;

                    result.Entity = item;
                    _MiniProjDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Item Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception x)
            {
                result.Message = x.Message;
                result.Success = false;
            }
            return result;
        }

        public List<PasienViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public PasienViewModel GetById(long id)
        {
            PasienViewModel result = new PasienViewModel();
            try
            {
                result = (from o in _MiniProjDbContext.M_customer_members
                          where o.customer_id == _userId && o.is_delete != true
                          select new PasienViewModel
                          {
                              id = o.id,
                              customer_id = o.M_customer.id,
                              customer_relation_id = o.M_customer_relation.id,
                          }).FirstOrDefault();
            }
            catch (Exception x)
            {
                string error = x.Message;
            }
            return result;
        }

        public ResponseResult Update(PasienViewModel entity)
        {
            try
            {
                M_customer item = _MiniProjDbContext.M_customers
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                if (item != null)
                {
                    item.biodata_id = entity.biodata_id;
                    item.dob = entity.dob;
                    item.gender = entity.gender;
                    item.blood_group_id = entity.blood_group_id;
                    item.rhesus_type = entity.rhesus_type;
                    item.height = (decimal)entity.height;
                    item.weight = (decimal)entity.weight;

                    item.modified_by = _userId;
                    item.modified_on = DateTime.Now;

                    _MiniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Item Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        /*--------------------    Outside CRUD    --------------------*/

        public async Task<ResponseResult> All(int page, int rows, string search, string cols, bool asc)
        {
            //throw new NotImplementedException();

            ResponseResult result = new ResponseResult();
            int rowNum = 0;

            try
            {
                var query = _MiniProjDbContext.M_customers
                         .Where(o => o.M_biodata.fullname.Contains(search));

                switch (cols)
                {
                    case "fullname":
                        query = !asc ? query.OrderByDescending(o => o.M_biodata.fullname) : query.OrderBy(o => o.M_biodata.fullname);
                        break;
                    default:
                        break;
                }

                result.Entity = query
                    .Skip((page - 1) * rows)
                    .Take(rows)
                      .Select(o => new PasienViewModel
                      {
                          id = o.id,
                          biodata_id = o.biodata_id,
                          fullname = o.M_biodata.fullname,
                          dob = (DateTime)o.dob,
                          gender = o.gender,
                          bloodname = o.M_blood_group.code,
                          rhesus_type = o.rhesus_type,
                          height = o.height,
                          weight = o.weight
                      }).ToList();

                result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);
                if (result.Pages < page)
                {
                    return await All(1, rows, search, cols, asc);
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                string error = e.Message;
            }

            return result;
        }
    }
}
