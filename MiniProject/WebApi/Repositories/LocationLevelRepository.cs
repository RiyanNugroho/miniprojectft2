﻿using System;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class LocationLevelRepository : IRepository<LocationLevelViewModel>
    {
        private MiniProjDbContext _miniProjDbContext = new MiniProjDbContext();
        private ResponseResult result = new ResponseResult();
        private long _UserName;

        public LocationLevelRepository()
        {
            _UserName = 1;
        }

        public LocationLevelRepository(long userName)
        {
            _UserName = userName;
        }
        public ResponseResult Create(LocationLevelViewModel entity)
        {
            try
            {
                M_location_level item = new M_location_level();
                item.name = entity.name;
                item.abbreviation = entity.abbreviation;

                item.created_by = _UserName;
                item.created_on = DateTime.Now;

                _miniProjDbContext.Add(item);
                _miniProjDbContext.SaveChanges();

                result.Entity = item;
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(LocationLevelViewModel entity)
        {
            try
            {
                M_location_level item = _miniProjDbContext.M_location_levels
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();
                if (item != null)
                {
                    item.deleted_by = _UserName;
                    item.deleted_on = DateTime.Now;

                    result.Entity = item;
                    _miniProjDbContext.M_location_levels.Remove(item);
                    _miniProjDbContext.SaveChanges();

                }
                else
                {
                    result.Success = false;
                    result.Message = "Not found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<LocationLevelViewModel> GetAll()
        {
            List<LocationLevelViewModel> result = new List<LocationLevelViewModel>();
            try
            {
                result = (from o in _miniProjDbContext.M_location_levels
                          select new LocationLevelViewModel
                          {
                              id = o.id,
                              name = o.name,
                              abbreviation = o.abbreviation
                          }).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public LocationLevelViewModel GetById(long id)
        {
            LocationLevelViewModel result = new LocationLevelViewModel();
            try
            {
                result = (from o in _miniProjDbContext.M_location_levels
                          where o.id == id
                          select new LocationLevelViewModel
                          {
                              id = o.id,
                              name = o.name,
                              abbreviation = o.abbreviation
                          }).FirstOrDefault();

            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(LocationLevelViewModel entity)
        {
            try
            {
                M_location_level item = _miniProjDbContext.M_location_levels
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();
                if (item != null)
                {
                    item.id = entity.id;
                    item.name = entity.name;
                    item.abbreviation = entity.abbreviation;

                    item.modified_by = _UserName;
                    item.modified_on = DateTime.Now;


                    _miniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }
    }
}
