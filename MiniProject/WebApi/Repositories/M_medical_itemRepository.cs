﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class M_medical_itemRepository : IRepository<MedItemViewModel>
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();
        private ResponseResult result = new ResponseResult();
        private long _userName;
        public M_medical_itemRepository()
        {
            _userName = 1 ;
        }
        public M_medical_itemRepository(long userName)
        {
            _userName = userName;
        }
        public ResponseResult Create(MedItemViewModel entity)
        {
            try
            {
                byte[] image = null;
                using (var fileStream = entity.FilePath.OpenReadStream())
                using (var memoryStream = new MemoryStream())
                {
                    fileStream.CopyTo(memoryStream);
                    image = memoryStream.ToArray();
                }


                M_medical_item item = new M_medical_item();
                item.name = entity.name;
                item.medical_item_category_id = entity.medical_item_category_id;
                item.composition = entity.composition;
                item.medical_item_segmentation_id = entity.medical_item_segmentation_id;
                item.manufacturer = entity.manufacturer;
                item.indication = entity.indication;
                item.dosage = entity.dosage;
                item.directions = entity.directions;
                item.contraindication = entity.contraindication;
                item.caution = entity.caution;
                item.packaging = entity.packaging;
                item.price_max = entity.price_max;
                item.price_min = entity.price_min;
                item.image = image;
                item.image_path = entity.image_path;


                item.created_by = _userName;
                item.created_on = DateTime.Now;


                M_medical_item check = _MiniProjDbContext.M_medical_items.Where(o => o.name == entity.name && !o.is_delete).FirstOrDefault();

                if (check != null)
                {
                    if (item.name == check.name && !check.is_delete)
                    {
                        result.Entity = entity;
                        result.Message = "Nama Item Sudah Tersedia!";
                        result.Success = false;
                    }
                    else
                    {
                        _MiniProjDbContext.M_medical_items.Add(item);
                        //check.name = check.name + "_old";
                        _MiniProjDbContext.SaveChanges();
                        result.Entity = check;
                    }
                }
                else
                {
                    _MiniProjDbContext.M_medical_items.Add(item);
                    _MiniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
                result.Entity = entity;
            }
            return result;
        }

        public ResponseResult Delete(MedItemViewModel entity)
        {
            try
            {
                M_medical_item item = _MiniProjDbContext.M_medical_items.Where(o => o.id == entity.id).FirstOrDefault();
                if (item != null)
                {
                    item.is_delete = true;
                    item.deleted_by = _userName;
                    item.deleted_on = DateTime.Now;

                    result.Entity = item;
                    //_MiniProjDbContext.M_medical_items.Remove(item);
                    _MiniProjDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Data Tidak Ditemukan";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<MedItemViewModel> GetAll()
        {
            List<MedItemViewModel> result = new List<MedItemViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.M_medical_items
                          select new MedItemViewModel
                          {
                              id = o.id,
                              name = o.name,
                              medical_item_category_id = o.medical_item_category_id,
                              categoryName=o.M_medical_item_category.name,
                              composition = o.composition,
                              medical_item_segmentation_id=o.medical_item_segmentation_id,
                              segmentationName=o.M_medical_item_segmentation.name,
                              manufacturer = o.manufacturer,
                              indication = o.indication,
                              dosage = o.dosage,
                              directions = o.directions,
                              contraindication = o.contraindication,
                              caution = o.caution,
                              packaging = o.packaging,
                              price_max = o.price_max,
                              price_min = o.price_min,
                              image = o.image != null ? String.Format("data:image/png;base64,{0}", Convert.ToBase64String(o.image)) : "",
                              image_path = o.image_path
                          }).ToList();
            }
            catch (Exception e)
            {
                string s = e.Message;
            }
            return result;
        }

        public async Task<ResponseResult> All(int page, int rows, string search)
        {
            // Page
            // Berapa baris @ page
            // Total 105. Req page 2, 10 row @ page
            ResponseResult result = new ResponseResult();
            int rowNum = 0;

            try
            {   //bila sudah ada token
                //var query = _MiniProjDbContext.M_medical_items
                //         .Where(o => o.name.Contains(search));

                //if (_userName != 1)
                //{
                //    query = query.Where(o => o.is_delete == false);
                //}

                //bila tidak pakai token
                var query = _MiniProjDbContext.M_medical_items
                         .Where(o => o.is_delete != true && o.name.Contains(search));

                result.Entity = query
                    .Skip((page - 1) * rows)
                    .Take(rows)
                      .Select(o => new MedItemViewModel
                      {
                          id = o.id,
                          name = o.name,
                          medical_item_category_id = o.medical_item_category_id,
                          categoryName = o.M_medical_item_category.name,
                          composition = o.composition,
                          medical_item_segmentation_id = o.medical_item_segmentation_id,
                          segmentationName = o.M_medical_item_segmentation.name,
                          manufacturer = o.manufacturer,
                          indication = o.indication,
                          dosage = o.dosage,
                          directions = o.directions,
                          contraindication = o.contraindication,
                          caution = o.caution,
                          packaging = o.packaging,
                          price_max = o.price_max,
                          price_min = o.price_min,
                          image = o.image != null ? String.Format("data:image/png;base64,{0}", Convert.ToBase64String(o.image)) : "",
                          image_path = o.image_path,
                          is_delete = o.is_delete
                      }).ToList();
                //result = query
                result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                if (result.Pages < page && result.Pages > 0)
                {
                    return await All(1, rows, search);
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                string error = e.Message;
            }

            return result;
        }

        public MedItemViewModel GetById(long id)
        {
            MedItemViewModel result = new MedItemViewModel();
            try
            {
                result = (from o in _MiniProjDbContext.M_medical_items
                          where o.id == id
                          select new MedItemViewModel
                          {
                              id = o.id,
                              name = o.name,
                              medical_item_category_id = o.medical_item_category_id,
                              categoryName = o.M_medical_item_category.name,
                              composition = o.composition,
                              medical_item_segmentation_id = o.medical_item_segmentation_id,
                              segmentationName = o.M_medical_item_segmentation.name,
                              manufacturer = o.manufacturer,
                              indication = o.indication,
                              dosage = o.dosage,
                              directions = o.directions,
                              contraindication = o.contraindication,
                              caution = o.caution,
                              packaging = o.packaging,
                              price_max = o.price_max,
                              price_min = o.price_min,
                              image_path = o.image_path
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string s = e.Message;
            }
            return result;
        }

        public ResponseResult GetByParent(long id)
        {
            try
            {
                result.Entity = (from o in _MiniProjDbContext.M_medical_items
                                 where o.medical_item_category_id == id && o.medical_item_segmentation_id == id
                                 select new MedItemViewModel
                                 {
                                     id = o.id,
                                     name = o.name,
                                     medical_item_category_id = o.medical_item_category_id,
                                     categoryName = o.M_medical_item_category.name,
                                     composition = o.composition,
                                     medical_item_segmentation_id = o.medical_item_segmentation_id,
                                     segmentationName = o.M_medical_item_segmentation.name,
                                     manufacturer = o.manufacturer,
                                     indication = o.indication,
                                     dosage = o.dosage,
                                     directions = o.directions,
                                     contraindication = o.contraindication,
                                     caution = o.caution,
                                     packaging = o.packaging,
                                     price_max = o.price_max,
                                     price_min = o.price_min,
                                     image_path = o.image_path
                                 }).ToList();
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }
        public ResponseResult Update(MedItemViewModel entity)
        {
            try
            {
                M_medical_item item = _MiniProjDbContext.M_medical_items
            .Where(o => o.id == entity.id)
            .FirstOrDefault();

                M_medical_item check = _MiniProjDbContext.M_medical_items.Where(o => o.name == entity.name && !o.is_delete).FirstOrDefault();

                if (item != null)
                {
                    byte[] image = null;
                    using (var fileStream = entity.FilePath.OpenReadStream())
                    using (var memoryStream = new MemoryStream())
                    {
                        fileStream.CopyTo(memoryStream);
                        image = memoryStream.ToArray();
                    }

                    item.name = entity.name;
                    item.medical_item_category_id = entity.medical_item_category_id;
                    item.composition = entity.composition;
                    item.medical_item_segmentation_id = entity.medical_item_segmentation_id;
                    item.manufacturer = entity.manufacturer;
                    item.indication = entity.indication;
                    item.dosage = entity.dosage;
                    item.directions = entity.directions;
                    item.contraindication = entity.contraindication;
                    item.caution = entity.caution;
                    item.packaging = entity.packaging;
                    item.price_max = entity.price_max;
                    item.price_min = entity.price_min;
                    item.image = image;
                    item.image_path = entity.image_path;

                    item.modified_by = _userName;
                    item.modified_on = DateTime.Now;

                    if (check != null)
                    {
                        result.Message = "Nama Tersebut Sudah Tersedia!";
                        result.Success = false;
                    }
                    else
                    {
                        _MiniProjDbContext.SaveChanges();
                        result.Entity = item;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = "Nama Tidak Ditemukan!";
                    result.Entity = entity;
                }



            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;

            }

            return result;
        }
    }
}
