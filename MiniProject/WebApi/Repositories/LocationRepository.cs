﻿using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using WebApi.DataModels;
using System.Threading.Tasks;

namespace WebApi.Repositories
{
    public class LocationRepository : IRepository<LocationViewModel>
    {
        private MiniProjDbContext _miniProjDbContext = new MiniProjDbContext();
        private ResponseResult result = new ResponseResult();
        private long _UserName;

        public LocationRepository()
        {
            _UserName = 1;
        }

        public LocationRepository(long userName)
        {
            _UserName = userName;
        }

        public ResponseResult ByParent(long id)
        {
            try
            {
                result.Entity = (from o in _miniProjDbContext.M_locations
                                 select new LocationViewModel
                                 {
                                     id = o.id,
                                     name = o.name,
                                     location_level_id = o.location_level_id,
                                     parent_id = o.parent_id,
                                     LocaLevName = o.M_location_level.name,
                                     region = o.location.M_location_level.abbreviation + " " + o.location.name,
                                     regionlist = o.M_location_level.abbreviation + " " + o.name
                                 }).ToList();
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }
        public ResponseResult Create(LocationViewModel entity)
        {
            try
            {
                M_location item = new M_location();
                item.name = entity.name;
                item.location_level_id = entity.location_level_id;
                if (entity.regionlist == null)
                {
                    item.parent_id = entity.parent_id;
                }
                else
                {
                    item.parent_id = long.Parse(entity.regionlist);
                }

                item.created_by = _UserName;
                item.created_on = DateTime.Now;

                M_location check = _miniProjDbContext.M_locations.Where(o => o.name == entity.name && o.location_level_id == entity.location_level_id && o.parent_id == item.parent_id && !o.is_delete).FirstOrDefault();
                if (check != null)
                {
                    //if(item.name == check.name && item.location_level_id != check.location_level_id)
                    //{

                    //    _miniProjDbContext.M_locations.Add(item);
                    //    _miniProjDbContext.SaveChanges();
                    //    result.Entity = item;
                    //}
                    if (item.name == check.name && !check.is_delete)
                    {
                        result.Entity = entity;
                        result.Message = "Data Sudah Ada";
                        result.Success = false;
                    }
                    else
                    {
                        _miniProjDbContext.M_locations.Add(item);
                        _miniProjDbContext.SaveChanges();
                        result.Entity = item;
                    }
                }
                else
                {
                    _miniProjDbContext.M_locations.Add(item);
                    _miniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(LocationViewModel entity)
        {
            try
            {
                M_location item = _miniProjDbContext.M_locations
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();
                if (item != null)
                {
                    item.deleted_by = _UserName;
                    item.is_delete = true;
                    item.deleted_on = DateTime.Now;

                    M_location check = _miniProjDbContext.M_locations.Where(o => o.parent_id == item.id && !o.is_delete).FirstOrDefault();
                    if (check != null)
                    {
                        result.Success = false;
                        result.Message = "Data Sedang Dipakai!";
                        result.Entity = entity;
                    }
                    else
                    {
                        result.Entity = item;
                        _miniProjDbContext.SaveChanges();
                    }


                }
                else
                {
                    result.Success = false;
                    result.Message = "Not found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<LocationViewModel> GetAll()
        {
            throw new NotImplementedException();
            //List<LocationViewModel> result = new List<LocationViewModel>();
            //try
            //{
            //    result = (from o in _miniProjDbContext.M_locations
            //              where o.is_delete != true 
            //              select new LocationViewModel
            //              {
            //                  id = o.id,
            //                  name = o.name,
            //                  location_level_id = o.location_level_id,
            //                  parent_id = o.parent_id,
            //                  LocaLevName = o.M_location_level.name,
            //                  //region = o.M_location_level.abbreviation + " " + o.name,
            //                  region = o.location.M_location_level.abbreviation + " " + o.location.name,
            //                  regionlist = o.M_location_level.abbreviation + " " + o.name + ", " + o.location.M_location_level.abbreviation + " " + o.location.name

            //              }).ToList();
            //}
            //catch (Exception e)
            //{
            //    string error = e.Message;
            //}
            //return result;
        }

        public LocationViewModel GetById(long id)
        {
            LocationViewModel result = new LocationViewModel();
            try
            {
                result = (from o in _miniProjDbContext.M_locations
                          where o.id == id && o.is_delete != true
                          select new LocationViewModel
                          {
                              id = o.id,
                              name = o.name,
                              location_level_id = o.location_level_id,
                              parent_id = o.parent_id,
                              LocaLevName = o.M_location_level.name,
                              region = o.location.M_location_level.abbreviation + " " + o.location.name,
                              regionlist = o.parent_id.ToString()
                              //regionlist = o.location.M_location_level.abbreviation + " " + o.location.name
                          }).FirstOrDefault();

            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(LocationViewModel entity)
        {
            try
            {
                M_location item = _miniProjDbContext.M_locations
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();
                if (item != null)
                {
                    item.id = entity.id;
                    item.name = item.name;
                    item.location_level_id = item.location_level_id;
                    if (entity.regionlist == null)
                    {
                        item.parent_id = entity.parent_id;
                    }
                    else
                    {
                        item.parent_id = item.parent_id;
                    }

                    item.modified_by = _UserName;
                    item.modified_on = DateTime.Now;

                    //M_location check = _miniProjDbContext.M_locations.Where(o => o.name == item.name && !o.is_delete || o.parent_id == item.parent_id && o.location_level_id == item.location_level_id).FirstOrDefault();
                    M_location check = _miniProjDbContext.M_locations.Where(o => o.name == item.name && o.location_level_id == entity.location_level_id && o.parent_id == item.parent_id && !o.is_delete).FirstOrDefault();
                    if (check.parent_id == null)
                    {
                        if (item.parent_id == entity.parent_id)
                        {
                            item.name = entity.name;
                            item.location_level_id = entity.location_level_id;
                            if (entity.regionlist != null)
                            {
                                item.parent_id = long.Parse(entity.regionlist);
                            }
                            else
                            {
                                item.parent_id = entity.parent_id;
                            }
                            _miniProjDbContext.SaveChanges();
                            result.Entity = item;
                        }
                        else if (item.name == entity.name && !check.is_delete && check.location_level_id == entity.location_level_id)
                        {
                            result.Success = false;
                            result.Message = "Data Yang Di Ubah Sudah Ada";
                            result.Entity = entity;
                        }
                        else
                        {
                            item.name = entity.name;
                            item.location_level_id = entity.location_level_id;
                            if (entity.regionlist != null)
                            {
                                item.parent_id = long.Parse(entity.regionlist);
                            }
                            else
                            {
                                item.parent_id = entity.parent_id;
                            }
                            _miniProjDbContext.SaveChanges();
                            result.Entity = item;
                        }
                    }
                    else if (item.name == entity.name && !check.is_delete && item.location_level_id == entity.location_level_id && item.parent_id == long.Parse(entity.regionlist))
                    {
                        result.Success = false;
                        result.Message = "Data Yang Di Ubah Sudah Ada";
                        result.Entity = entity;
                    }
                    else
                    {
                        item.name = entity.name;
                        item.location_level_id = entity.location_level_id;
                        item.parent_id = long.Parse(entity.regionlist);
                        _miniProjDbContext.SaveChanges();
                        result.Entity = item;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }

        public async Task<ResponseResult> All(int page, int rows, string search)
        {
            // Page
            // Berapa baris @ page
            // Total 105. Req page 2, 10 row @ page
            ResponseResult result = new ResponseResult();
            int rowNum = 0;

            try
            {   //bila sudah ada token
                //var query = _MiniProjDbContext.M_medical_item_categories
                //         .Where(o => o.name.Contains(search));

                //if (_userName != 1)
                //{
                //    query = query.Where(o => o.is_delete == true);
                //}

                var query = _miniProjDbContext.M_locations
                         .Where(o => o.is_delete != true && o.name.Contains(search));

                result.Entity = query
                    .Skip((page - 1) * rows)
                    .Take(rows)
                      .Select(o => new LocationViewModel
                      {
                          id = o.id,
                          name = o.name,
                          parent_id = o.parent_id,
                          LocaLevName = o.M_location_level.name,
                          region = o.location.M_location_level.abbreviation + " " + o.location.name,
                          regionlist = o.M_location_level.abbreviation + " " + o.name
                      }).ToList();
                //result = query
                result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                if (result.Pages < page && result.Pages > 0)
                {
                    return await All(1, rows, search);
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                string error = e.Message;
            }

            return result;
        }
    }
}
