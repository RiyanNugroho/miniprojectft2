﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;
using WebApi.Repositories;


namespace WebApi.Repositories
{
    public class CustomerRelationRepository : IRepository<CustRelationViewModel>
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();
        private BaseProperties baseProp = new BaseProperties();
        private ResponseResult result = new ResponseResult();
        private long _userId;

        public CustomerRelationRepository()
        {
            _userId = 3;
        }

        public CustomerRelationRepository(long userId)
        {
            _userId = userId;
        }

        public ResponseResult Create(CustRelationViewModel entity)
        {
            try
            {
                M_customer_relation item = new M_customer_relation();
                item.name = entity.name;

                item.created_by = _userId;
                item.created_on = DateTime.Now;

                M_customer_relation check = _MiniProjDbContext.M_customer_relations.Where(o => o.name == entity.name && !o.is_delete).FirstOrDefault();

                if (check != null)
                {
                    if (item.name == check.name && !check.is_delete)
                    {
                        result.Entity = entity;
                        result.Message = "Item Sudah Tersedia!";
                        result.Success = false;
                    }
                    else
                    {
                        _MiniProjDbContext.M_customer_relations.Add(item);
                        //check.name = check.name + "_old";
                        _MiniProjDbContext.SaveChanges();
                        result.Entity = check;
                    }
                }
                else
                {
                    _MiniProjDbContext.M_customer_relations.Add(item);
                    _MiniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
            }
            catch (Exception x)
            {
                result.Entity = entity;
                result.Message = x.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(CustRelationViewModel entity)
        {
            try
            {
                M_customer_relation item = _MiniProjDbContext.M_customer_relations
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                if (item != null)
                {
                    item.is_delete = true;
                    item.deleted_by = _userId;
                    item.deleted_on = DateTime.Now;

                    result.Entity = item;
                    _MiniProjDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Item Tidak Ditemukan!";
                    result.Entity = entity;
                }
            }
            catch (Exception x)
            {
                result.Message = x.Message;
                result.Success = false;
            }
            return result;

        }

        public List<CustRelationViewModel> GetAll()
        {
            List<CustRelationViewModel> result = new List<CustRelationViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.M_customer_relations
                          .Where(o => o.is_delete != true)
                          select new CustRelationViewModel
                          {
                              id = o.id,
                              name = o.name
                          }).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public CustRelationViewModel GetById(long id)
        {
            CustRelationViewModel result = new CustRelationViewModel();
            try
            {
                result = (from o in _MiniProjDbContext.M_customer_relations
                          .Where(o => o.id == id && o.is_delete != true)
                          select new CustRelationViewModel
                          {
                              id = o.id,
                              name = o.name
                          }).FirstOrDefault();
            }
            catch (Exception x)
            {
                string error = x.Message;
            }
            return result;

        }

        public ResponseResult Update(CustRelationViewModel entity)
        {
            try
            {
                M_customer_relation item = _MiniProjDbContext.M_customer_relations
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();

                M_customer_relation check = _MiniProjDbContext.M_customer_relations.Where(o => o.name == entity.name && !o.is_delete).FirstOrDefault();

                if (item != null)
                {
                    item.name = entity.name;

                    item.modified_by = _userId;
                    item.modified_on = DateTime.Now;

                    if (check != null)
                    {
                        result.Message = "Item yang diubah sudah tersedia!";
                        result.Success = false;
                    }
                    else
                    {
                        _MiniProjDbContext.SaveChanges();
                        result.Entity = item;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = "Item Tidak Ditemukan!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        /*--------------------    Paging / Search   --------------------*/

        public List<CustRelationViewModel> ByFilter(string search)
        {
            List<CustRelationViewModel> result = new List<CustRelationViewModel>();

            var item = _MiniProjDbContext.M_customer_relations
                    .Where(o => o.name.Contains(search) && o.is_delete != true);

            result = item.Take(5)
                    .Select(o => new CustRelationViewModel
                    {
                        id = o.id,
                        name = o.name
                    }).ToList();

            return result;
        }

        //public ResponseResult ByFilter(string search)
        //{
        //    try
        //    {
        //        List<CustRelationViewModel> list = new List<CustRelationViewModel>();

        //        var item = _MiniProjDbContext.M_customer_relations
        //                .Where(o => o.name.Contains(search) && o.is_delete != true);

        //        list = item.Take(5)
        //                .Select(o => new CustRelationViewModel
        //                {
        //                    id = o.id,
        //                    name = o.name
        //                }).ToList();

        //        if (list.Count != 0)
        //        {
        //            result.Entity = list;
        //            result.Message = "Hasil pencarian untuk : " + search;
        //            result.Success = true;
        //        }
        //        else
        //        {
        //            result.Entity = list;
        //            result.Message = "Item tidak ditemukan!";
        //            result.Success = false;
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        result.Message = x.Message;
        //        result.Success = false;
        //    }
        //    return result;
        //}
    }
}
