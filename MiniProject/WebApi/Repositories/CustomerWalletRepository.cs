﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class CustomerWalletRepository : IRepository<CustomerWalletViewModel>
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();
        private ResponseResult result = new ResponseResult();
        private long _userID;
        private long _customer;
        //private long customer_id;
        public CustomerWalletRepository()
        {
            _userID = 1;
        }
        public CustomerWalletRepository(long userId, long customer_id)
        {
            _userID = userId;
            _customer = customer_id;
        }
        public ResponseResult Create(CustomerWalletViewModel entity)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Delete(CustomerWalletViewModel entity)
        {
            throw new NotImplementedException();
        }

        public List<CustomerWalletViewModel> GetAll()
        {
            List<CustomerWalletViewModel> result = new List<CustomerWalletViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.T_customer_wallets
                          where o.customer_id == _customer
                          select new CustomerWalletViewModel
                          {
                              id = o.id,
                              customer_id = o.customer_id,
                              pin = o.pin,
                              balance = o.balance,
                              barcode = o.barcode,
                              points = o.points,
                              create_on = o.created_on
                          }
                         ).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public CustomerWalletViewModel GetById(long customer_id)
        {
            CustomerWalletViewModel result = new CustomerWalletViewModel();
            try
            {
                result = (from o in _MiniProjDbContext.T_customer_wallets
                          where o.customer_id == _customer 
                          select new CustomerWalletViewModel
                          {
                              id = o.id,
                              customer_id = o.customer_id,
                              pin = o.pin,
                              balance = o.balance,
                              barcode = o.barcode,
                              points = o.points,
                              create_on = o.created_on

                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(CustomerWalletViewModel entity)
        {
            throw new NotImplementedException();
        }
    }
}
