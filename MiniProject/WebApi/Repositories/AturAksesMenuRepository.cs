﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Helpers;
using ViewModel;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class AturAksesMenuRepository : IRepository<MenuRoleViewModel>
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();

        private ResponseResult result = new ResponseResult();

        private RoleViewModel _roleviewmodel;

        private long _UserName;

        public AturAksesMenuRepository()
        {
            _UserName = 1;
        }

        public AturAksesMenuRepository(long username)
        {
            _UserName = username;
        }

        public ResponseResult Create(MenuRoleViewModel entity)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Delete(MenuRoleViewModel entity)
        {
            throw new NotImplementedException();
        }

        public List<MenuRoleViewModel> GetAll()
        {
            List<MenuRoleViewModel> result = new List<MenuRoleViewModel>();
            try
            {
                //var qry = (from role in _MiniProjDbContext.M_roles
                //          join menurole in _MiniProjDbContext.M_menu_roles
                //          on role.id equals menurole.role_id into joined
                //          from o in joined.DefaultIfEmpty()
                //          select new { Id = role.id, Name = role.name, Code = role.code }).ToList();


                result = (from role in _MiniProjDbContext.M_roles
                          join menurole in _MiniProjDbContext.M_menu_roles
                          on role.id equals menurole.role_id into joinrole

                          from menu in _MiniProjDbContext.M_menu_roles.DefaultIfEmpty()
                          join m in _MiniProjDbContext.M_menus
                          on menu.id equals m.m_Menu.id into joinmenu

                          from o in joinmenu.DefaultIfEmpty()

                          select new MenuRoleViewModel
                          {
                              id = role.id,
                              namaRole = role.name,
                              kodeRole = role.code,


                          }).Distinct().ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public MenuRoleViewModel GetById(long id)
        {
            MenuRoleViewModel result = new MenuRoleViewModel();
            try
            {
                result = (from o in _MiniProjDbContext.M_menu_roles
                          where o.id == id
                          select new MenuRoleViewModel
                          {
                              id = o.id,
                              menu_id = o.menu.id,
                              namaMenu = o.menu.name,
                              role_id = o.role.id,
                              namaRole = o.role.name,
                              kodeRole = o.role.code
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(MenuRoleViewModel entity)
        {
            try
            {
                M_menu_role item = _MiniProjDbContext.M_menu_roles
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                if (item != null)
                {
                    item.menu.name = entity.namaMenu;

                    _MiniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<MenuRoleViewModel> ByFilter(string search)
        {

            List<MenuRoleViewModel> result = new List<MenuRoleViewModel>();
            {
                var query = _MiniProjDbContext.M_roles
                    .Where(o => (o.name.Contains(search)));

                result = query.Take(5)
                          .Select(o => new MenuRoleViewModel
                          {
                              id = o.id,
                              namaRole = o.name,
                              kodeRole = o.code
                          }).ToList();
            }

            return result;
        }

        public async Task<ResponseResult> CheckList(MenuRoleCheck list)
        {
            try
            {
                foreach (var item in list.List)
                {
                    M_menu_role menuRole = _MiniProjDbContext.M_menu_roles
                        .Where(o => o.role_id == list.RoleId && o.menu_id == item.Id)
                        .FirstOrDefault();
                    if (menuRole != null)
                    {
                        if (!item.Check)
                        {
                            _MiniProjDbContext.M_menu_roles.Remove(menuRole);
                            _MiniProjDbContext.SaveChanges();
                        }
                    }
                    else
                    {
                        if (item.Check)
                        {
                            menuRole = new M_menu_role();
                            menuRole.role_id = list.RoleId;
                            menuRole.menu_id = item.Id;
                            _MiniProjDbContext.M_menu_roles.Add(menuRole);
                            _MiniProjDbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }

    }
}
