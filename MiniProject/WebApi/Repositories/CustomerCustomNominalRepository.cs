﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class CustomerCustomNominalRepository : IRepository<CustomerCustomNominalViewModel>
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();
        private ResponseResult result = new ResponseResult();
        private long _userID;
        private long _customer;
        //private long customer_id;
        public CustomerCustomNominalRepository()
        {
            _userID = 1;
        }
        public CustomerCustomNominalRepository(long userId, long customer_id)
        {
            _userID = userId;
            _customer = customer_id;
        }
        public ResponseResult Create(CustomerCustomNominalViewModel entity)
        {
            //throw new NotImplementedException();

            try
            {
                T_customer_custom_nominal item = new T_customer_custom_nominal();
                item.customer_id = _customer;
                item.nominal = entity.nominal;


                item.created_by = _userID;
                item.created_on = DateTime.Now;

                T_customer_custom_nominal check = _MiniProjDbContext.T_customer_custom_nominals.Where(o => o.nominal == entity.nominal).FirstOrDefault();
                if (check != null)
                {
                    if (item.nominal == check.nominal)
                    {
                        result.Entity = entity;
                        result.Message = "Nominal sudah ada";
                        result.Success = false;
                    }
                    else
                    {
                        _MiniProjDbContext.T_customer_custom_nominals.Add(item);
                        _MiniProjDbContext.SaveChanges();
                        result.Entity = item;
                    }
                }
                else
                {
                    _MiniProjDbContext.T_customer_custom_nominals.Add(item);
                    _MiniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
            }
            catch (Exception e)
            {
                result.Entity = entity;
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(CustomerCustomNominalViewModel entity)
        {
            throw new NotImplementedException();
        }

        public List<CustomerCustomNominalViewModel> GetAll()
        {
            //throw new NotImplementedException();

            List<CustomerCustomNominalViewModel> result = new List<CustomerCustomNominalViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.T_customer_custom_nominals
                          where o.customer_id==_customer
                          select new CustomerCustomNominalViewModel
                          {
                              id = o.id,
                              customer_id=o.customer_id,
                              nominal = o.nominal
                          }
                         ).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public CustomerCustomNominalViewModel GetById(long id)
        {
            //throw new NotImplementedException();
            CustomerCustomNominalViewModel result = new CustomerCustomNominalViewModel();
            try
            {
                result = (from o in _MiniProjDbContext.T_customer_custom_nominals
                          where o.id == id && o.is_delete != true
                          select new CustomerCustomNominalViewModel
                          {
                              id = o.id,
                              customer_id = o.customer_id,
                              nominal = o.nominal
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(CustomerCustomNominalViewModel entity)
        {
            throw new NotImplementedException();
        }
    }
}
