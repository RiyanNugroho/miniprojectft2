﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class CustomerRegisteredCardRepository : IRepository<CustomerRegisteredCardViewModel>
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();
        private ResponseResult result = new ResponseResult();
        private long _userID;
        private long _customer;
        //private long customer_id;
        public CustomerRegisteredCardRepository()
        {
            _userID = 1;
        }
        public CustomerRegisteredCardRepository(long userId, long customer_id)
        {
            _userID = userId;
            _customer = customer_id;
        }
        public ResponseResult Create(CustomerRegisteredCardViewModel entity)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Delete(CustomerRegisteredCardViewModel entity)
        {
            throw new NotImplementedException();
        }

        public List<CustomerRegisteredCardViewModel> GetAll()
        {
            List<CustomerRegisteredCardViewModel> result = new List<CustomerRegisteredCardViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.T_customer_registered_cards
                          where o.customer_id == _customer
                          select new CustomerRegisteredCardViewModel
                          {
                              id = o.id,
                              customer_id = o.customer_id,
                              card_number = o.card_number,
                              validity_period = o.validity_period, 
                              cvv = o.cvv
                          }
                         ).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public CustomerRegisteredCardViewModel GetById(long customer_id)
        {
            CustomerRegisteredCardViewModel result = new CustomerRegisteredCardViewModel();
            try
            {
                result = (from o in _MiniProjDbContext.T_customer_registered_cards
                          where o.customer_id == _customer 
                          select new CustomerRegisteredCardViewModel
                          {
                              id = o.id,
                              customer_id = o.customer_id,
                              card_number = o.card_number,
                              validity_period = o.validity_period,
                              cvv = o.cvv

                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(CustomerRegisteredCardViewModel entity)
        {
            throw new NotImplementedException();
        }
    }
}
