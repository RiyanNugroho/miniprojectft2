﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class itemCategoryRepository : IRepository<ItemCategoryViewModel>
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();
        private ResponseResult result = new ResponseResult();
        private long _userId;
        private long _userName;
        public itemCategoryRepository()
        {
            _userId = 1;
        }
        public itemCategoryRepository(long userId, long biodataId)
        {
            _userName = biodataId;
            _userId = userId;
        }
        public ResponseResult Create(ItemCategoryViewModel entity)
        {
            try
            {
                M_medical_item_category item = new M_medical_item_category();
                item.name = entity.name;

                item.created_by = _userId;
                item.created_on = DateTime.Now;

                M_medical_item_category check = _MiniProjDbContext.M_medical_item_categories.Where(o => o.name == entity.name && !o.is_delete).FirstOrDefault();

                if (check != null)
                {
                    if (item.name == check.name && !check.is_delete)
                    {
                        result.Entity = entity;
                        result.Message = "Nama Sudah Tersedia!";
                        result.Success = false;
                    }
                    else
                    {
                        _MiniProjDbContext.M_medical_item_categories.Add(item);
                        //check.name = check.name + "_old";
                        _MiniProjDbContext.SaveChanges();
                        result.Entity = check;
                    }
                }
                else
                {
                    _MiniProjDbContext.M_medical_item_categories.Add(item);
                    _MiniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
            }
            catch (Exception x)
            {
                result.Entity = entity;
                result.Message = x.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(ItemCategoryViewModel entity)
        {
            try
            {
                M_medical_item_category item = _MiniProjDbContext.M_medical_item_categories.Where(o => o.id == entity.id).FirstOrDefault();
                if (item != null)
                {
                    item.is_delete = true;
                    item.deleted_by = _userId;
                    item.deleted_on = DateTime.Now;

                    result.Entity = item;
                    //_MiniProjDbContext.M_medical_item_categories.Remove(item);
                    _MiniProjDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Data Tidak Ditemukan";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {

                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<ItemCategoryViewModel> GetAll()
        {
            //throw new NotImplementedException();

            List<ItemCategoryViewModel> result = new List<ItemCategoryViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.M_medical_item_categories
                          where o.is_delete != true
                          select new ItemCategoryViewModel
                          {
                              id = o.id,
                              name = o.name,
                              pengubah=_userName
                          }
                         ).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ItemCategoryViewModel GetById(long id)
        {
            ItemCategoryViewModel result = new ItemCategoryViewModel();
            try
            {
                result = (from o in _MiniProjDbContext.M_medical_item_categories
                          where o.id == id && o.is_delete != true
                          select new ItemCategoryViewModel
                          {
                              id = o.id,
                              name = o.name,
                              pengubah = _userName
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(ItemCategoryViewModel entity)
        {
            try
            {
                M_medical_item_category item = _MiniProjDbContext.M_medical_item_categories
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();

                M_medical_item_category check = _MiniProjDbContext.M_medical_item_categories.Where(o => o.name == entity.name && !o.is_delete).FirstOrDefault();

                if (item != null)
                {
                    item.name = entity.name;

                    item.modified_by = _userId;
                    item.modified_on = DateTime.Now;

                    if (check != null)
                    {
                        result.Message = "Nama Tersebut Sudah Tersedia!";
                        result.Success = false;
                    }
                    else
                    {
                        _MiniProjDbContext.SaveChanges();
                        result.Entity = item;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = "Nama Tidak Ditemukan!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }


        public async Task<ResponseResult> All(int page, int rows, string search)
        {
            // Page
            // Berapa baris @ page
            // Total 105. Req page 2, 10 row @ page
            ResponseResult result = new ResponseResult();
            int rowNum = 0;

            try
            {   //bila sudah ada token
                var query = _MiniProjDbContext.M_medical_item_categories
                         .Where(o =>o.name.Contains(search));

                if (_userId != 1)
                {
                    query = query.Where(o => o.is_delete == false);
                }

                //bila tidak pakai token
                //var query = _MiniProjDbContext.M_medical_item_categories
                //         .Where(o => o.is_delete != true && o.name.Contains(search));

                result.Entity = query
                    .Skip((page - 1) * rows)
                    .Take(rows)
                      .Select(o => new ItemCategoryViewModel
                      {
                          id = o.id,
                          name = o.name,
                          pengubah = _userName,
                          is_delete = o.is_delete
                      }).ToList();
                //result = query
                result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                if (result.Pages < page && result.Pages > 0)
                {
                    return await All(1, rows, search);
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                string error = e.Message;
            }

            return result;
        }

    }
}
