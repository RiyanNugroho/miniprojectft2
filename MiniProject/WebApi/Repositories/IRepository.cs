﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using ViewModel;

namespace WebApi.Repositories
{
    public interface IRepository<T>
    {
        List<T> GetAll();

        T GetById(long id);

        ResponseResult Create(T entity);

        ResponseResult Update(T entity);

        ResponseResult Delete(T entity);
    }
}
