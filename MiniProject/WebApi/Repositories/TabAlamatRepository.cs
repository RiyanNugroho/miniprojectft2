﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using ViewModel;
using WebApi.DataModels;
using System;
using System.Net.Http;

namespace WebApi.Repositories
{
    public class TabAlamatRepository : IRepository<TabAlamatViewModel>
    {
        private MiniProjDbContext _miniProjDbContext = new MiniProjDbContext();

        private ResponseResult result = new ResponseResult();

        private long _UserName;
        private long _Biodata;

        public TabAlamatRepository()
        {
            _UserName = 1;
        }

        public TabAlamatRepository(long userId, long biodata_id)
        {
            _UserName = userId;
            _Biodata = biodata_id;
        }

        public ResponseResult ByParent(long id)
        {
            try
            {
                result.Entity = (from o in _miniProjDbContext.M_biodata_address
                                 select new TabAlamatViewModel
                                 {
                                     id = o.id,
                                     biodata_id = o.M_biodata.id,
                                     label = o.label,
                                     recipient = o.recipient,
                                     recipient_phone_number = o.recipient_phone_number,
                                     location_id = o.location_id,
                                     postal_code = o.postal_code,
                                     address = o.address,
                                     kota = o.M_location.name

                                 }).ToList();
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }
        public ResponseResult Create(TabAlamatViewModel entity)
        {
            try
            {

                M_biodata_address item = new M_biodata_address();
                item.label = entity.label;
                item.recipient = entity.recipient;
                item.recipient_phone_number = entity.recipient_phone_number;
                item.location_id = entity.location_id;
                item.postal_code = entity.postal_code;
                item.address = entity.address;

                item.created_by = _UserName;
                item.created_on = DateTime.Now;

                M_biodata_address check = _miniProjDbContext.M_biodata_address.Where(o => o.label == entity.label && !o.is_delete).FirstOrDefault();

                if (check != null)
                {
                    if (item.label == check.label && !check.is_delete)
                    {
                        result.Entity = entity;
                        result.Message = "Item Sudah Tersedia!";
                        result.Success = false;
                    }
                    else
                    {
                        _miniProjDbContext.M_biodata_address.Add(item);
                        //check.name = check.name + "_old";
                        _miniProjDbContext.SaveChanges();
                        result.Entity = check;
                    }
                }
                else
                {
                    _miniProjDbContext.M_biodata_address.Add(item);
                    _miniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
            }
            catch (Exception x)
            {
                result.Entity = entity;
                result.Message = x.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(TabAlamatViewModel entity)
        {
            try
            {
                M_biodata_address item = _miniProjDbContext.M_biodata_address
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();
                if (item != null)
                {
                    item.is_delete = true;

                    item.deleted_by = _UserName;
                    item.deleted_on = DateTime.Now;

                    result.Entity = item;
                    _miniProjDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {

                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<TabAlamatViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public TabAlamatViewModel GetById(long id)
        {
            TabAlamatViewModel result = new TabAlamatViewModel();
            try
            {
                result = (from o in _miniProjDbContext.M_biodata_address
                          where o.id == id && o.is_delete != true
                          select new TabAlamatViewModel
                          {
                              id = o.id,
                              biodata_id = o.M_biodata.id,
                              label = o.label,
                              recipient = o.recipient,
                              recipient_phone_number = o.recipient_phone_number,
                              location_id = o.location_id,
                              postal_code = o.postal_code,
                              address = o.address
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(TabAlamatViewModel entity)
        {
            try
            {
                M_biodata_address item = _miniProjDbContext.M_biodata_address
                    .Where(o => o.id == entity.id)
                     .FirstOrDefault();

                if (item != null)
                {
                    item.label = entity.label;
                    item.recipient = entity.recipient;
                    item.recipient_phone_number = entity.recipient_phone_number;
                    item.location_id = entity.location_id;
                    item.postal_code = entity.postal_code;
                    item.address = entity.address;

                    item.modified_by = _UserName;
                    item.modified_on = DateTime.Now;

                    _miniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {

                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }

        public async Task<ResponseResult> All(int page, int rows, string search, string column, bool ascen)
        {

            ResponseResult result = new ResponseResult();
            int rowNum = 0;

            try
            {
                var query = _miniProjDbContext.M_biodata_address
                         .Where(o => o.address.Contains(search) || o.recipient.Contains(search));

                if (_UserName != 1)
                {
                    query = query.Where(o => o.is_delete == false);
                }

                switch (column)
                {
                    case "label":
                        query = !ascen ? query.OrderByDescending(o => o.label) : query.OrderBy(o => o.label);
                        break;
                    case "recipient":
                        query = !ascen ? query.OrderByDescending(o => o.recipient) : query.OrderBy(o => o.recipient);
                        break;
                    case "address":
                        query = !ascen ? query.OrderByDescending(o => o.address) : query.OrderBy(o => o.address);
                        break;
                    default:
                        break;
                }

                result.Entity = query
                    .Skip((page - 1) * rows)
                    .Take(rows)
                      .Select(o => new TabAlamatViewModel
                      {
                          id = o.id,
                          biodata_id = o.M_biodata.id,
                          label = o.label,
                          recipient = o.recipient,
                          recipient_phone_number = o.recipient_phone_number,
                          location_id = o.location_id,
                          postal_code = o.postal_code,
                          address = o.address,
                          kota = o.M_location.name,
                          Nama = o.M_biodata.fullname
                      }).ToList();
                //result = query
                result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);
                if (result.Pages < page && result.Pages > 0)
                {
                    return await All(1, rows, search, column, ascen);
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                string error = e.Message;
            }

            return result;
        }

        public async Task<ResponseResult> DeleteMany(List<AlamatMultiCheckViewModel> list)
        {
            try
            {
                foreach (var item in list)
                {
                    if (item.Check)
                    {
                        M_biodata_address p = _miniProjDbContext.M_biodata_address
                            .Where(o => o.id == item.id)
                            .FirstOrDefault();

                        if (p != null)
                        {
                            p.is_delete = !item.Check;
                            p.modified_by = _UserName;
                            p.modified_on = DateTime.Now;
                        }
                        _miniProjDbContext.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }

    }
}
