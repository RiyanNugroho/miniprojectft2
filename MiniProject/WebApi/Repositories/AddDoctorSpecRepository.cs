﻿using System;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class AddDoctorSpecRepository : IRepository<DoctorSpecializationViewModel>
    {
        private MiniProjDbContext _miniProjDbContext = new MiniProjDbContext();
        private ResponseResult result = new ResponseResult();
        private long _UserName;
        private long _biodata_id;
        private long _doctor_id;

        public AddDoctorSpecRepository()
        {
            _UserName = 1;
        }

        public AddDoctorSpecRepository(long userName, long doctor_id)
        {
            _UserName = userName;            
            _doctor_id = doctor_id;
            //_biodata_id = biodata_id;   
        }

        public ResponseResult ByParent(long id)
        {
            try
            {
                result.Entity = (from o in _miniProjDbContext.T_current_doctor_specializations
                                 join doctoridd in _miniProjDbContext.M_doctors
                                 on o.id equals doctoridd.id into joindocid

                                 select new DoctorSpecializationViewModel
                                 {
                                     id = o.id,
                                     doctor_id = o.M_doctor.id,
                                     specialization_id = o.specialization_id,
                                     specialization_name = o.M_specialization.name,
                                     doctor_name = o.M_doctor.M_Biodata.fullname,
                                 }).ToList();
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }

        public ResponseResult Create(DoctorSpecializationViewModel entity)
        {
            try
            {
                M_doctor _m_doctor = _miniProjDbContext.M_doctors.Where(o => o.id == entity.id).FirstOrDefault();
                T_current_doctor_specialization item = new T_current_doctor_specialization();
                
                item.doctor_id = _doctor_id;
                item.specialization_id = entity.specialization_id;

                item.created_by = _UserName;
                item.created_on = DateTime.Now;

                T_current_doctor_specialization check = _miniProjDbContext.T_current_doctor_specializations.Where(o => o.created_by == _UserName).FirstOrDefault();
                if (check != null)
                {
                    entity.count_list = true;
                }
                else
                {
                    entity.count_list = false;
                }

                if (entity.count_list != true && entity.specialization_id == null)
                {
                    result.Success = false;
                    result.Entity = entity;
                    result.Message = "Mohon pilih spesialisasi";
                }
                else if (entity.count_list != true)
                {
                    _miniProjDbContext.T_current_doctor_specializations.Add(item);
                    _miniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
                else
                {
                    result.Success = false;
                    result.Entity = entity;
                    result.Message = "Tidak bisa menambahkan spesialisasi lebih dari satu";
                }

            }
            catch (Exception e)
            {
                result.Success = false;
                result.Entity = entity;
                result.Message = e.Message;
            }
            return result;
        }

        public ResponseResult Delete(DoctorSpecializationViewModel entity)
        {
            try
            {
                T_current_doctor_specialization item = _miniProjDbContext.T_current_doctor_specializations
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();
                if (item != null)
                {
                    item.deleted_by = _UserName;
                    item.is_delete = true;
                    item.deleted_on = DateTime.Now;

                    result.Entity = item;
                    _miniProjDbContext.SaveChanges();

                }
                else
                {
                    result.Success = false;
                    result.Message = "Not found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<DoctorSpecializationViewModel> GetAll()
        {
            List<DoctorSpecializationViewModel> result = new List<DoctorSpecializationViewModel>();
            try
            {
                result = (from o in _miniProjDbContext.T_current_doctor_specializations
                          .Where(o => o.is_delete != true && o.created_by == _UserName)

                          select new DoctorSpecializationViewModel
                          {
                              id = o.id,
                              doctor_id = o.doctor_id,
                              specialization_id = o.specialization_id,
                              specialization_name = o.M_specialization.name,
                              doctor_name = o.M_doctor.M_Biodata.fullname
                          }).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public DoctorSpecializationViewModel GetById(long id)
        {
            DoctorSpecializationViewModel result = new DoctorSpecializationViewModel();
            try
            {
                result = (from o in _miniProjDbContext.T_current_doctor_specializations
                          where o.id == id && o.is_delete != true
                          select new DoctorSpecializationViewModel
                          {
                              id = o.id,
                              doctor_id = o.M_doctor.id,
                              specialization_id = o.specialization_id,
                              specialization_name = o.M_specialization.name,
                              doctor_name = o.M_doctor.M_Biodata.fullname
                          }).FirstOrDefault();

            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(DoctorSpecializationViewModel entity)
        {
            try
            {
                T_current_doctor_specialization item = _miniProjDbContext.T_current_doctor_specializations
                    .Where(o => o.id == entity.id)
                    .FirstOrDefault();
                if (item != null)
                {
                    item.id = entity.id;
                    item.doctor_id = _doctor_id;
                    item.specialization_id = entity.specialization_id;

                    item.modified_by = _UserName;
                    item.modified_on = DateTime.Now;

                    T_current_doctor_specialization check = _miniProjDbContext.T_current_doctor_specializations.Where(o => o.id == entity.id && entity.specialization_id == entity.specialization_id).FirstOrDefault();
                    if (check.specialization_id == null)
                    {
                        result.Success = false;
                        result.Message = "Mohon isi spesialisasi";
                        result.Entity = entity;
                        
                    }                    
                    else if (check.specialization_id > 0)
                    {
                        _miniProjDbContext.SaveChanges();
                        result.Entity = item;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }
    }
}
