﻿using System;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class PendaftaranRepository : IRepository<PendaftaranViewModel>
    {
        private MiniProjDbContext _miniProjDbContext = new MiniProjDbContext();
        private ResponseResult result = new ResponseResult();

        public ResponseResult ByParent(long id)
        {
            try
            {
                result.Entity = (from o in _miniProjDbContext.M_users
                                 select new PendaftaranViewModel
                                 {
                                     id = o.id,
                                     email = o.email,
                                     password = o.password,
                                     biodata_id = o.m_biodata.id,
                                     role_id = o.m_role.id
                                 }).ToList();
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }

        public ResponseResult Create(PendaftaranViewModel entity)
        {
            try
            {
                M_user item = new M_user();
                M_biodata item2 = new M_biodata();
                M_role item3 = new M_role();
                M_doctor item4 = new M_doctor();
                M_customer item5 = new M_customer();
                M_admin item6 = new M_admin();
                T_token item7 = new T_token();
                item.email = entity.email;
                item.password = entity.password;
                item2.fullname = entity.fullname;
                item.biodata_id = item2.id;
                item.role_id = entity.role_id;
                item2.mobile_phone = entity.mobile_phone;                
                item.login_attempt = 0;
                item.is_locked = false;
                item.is_delete = false;
                item.last_login = DateTime.Now;

                item.created_by = 1;
                item2.created_by = 1;
                item.created_on = DateTime.Now;
                item2.created_on = DateTime.Now;

                M_user check = _miniProjDbContext.M_users.Where(o => o.email == entity.email).FirstOrDefault();
                T_token checkToken = _miniProjDbContext.T_tokens.Where(o => o.email == entity.email && o.token == entity.token && o.is_expired != true).FirstOrDefault();
                if (check != null)
                {
                    result.Entity = entity;
                    result.Message = "Email Sudah Ada";
                    result.Success = false;             
                }
                else if (checkToken != null && check == null)
                {
                    if (checkToken.email == entity.email && checkToken.token == entity.token && checkToken.is_expired != true)
                    {
                        if (item.role_id == 1)
                        {
                            if (entity.email == null)
                            {
                                result.Entity = entity;
                                result.Message = "Silahkan isi email";
                                result.Success = false;
                            }
                            else
                            {
                                item6.created_by = 1;
                                item6.created_on = DateTime.Now;
                                _miniProjDbContext.M_biodatas.Add(item2);
                                _miniProjDbContext.SaveChanges();
                                item6.biodata_id = item2.id;
                                _miniProjDbContext.M_admins.Add(item6);
                                _miniProjDbContext.SaveChanges();
                                item.biodata_id = item2.id;
                                _miniProjDbContext.M_users.Add(item);
                                _miniProjDbContext.SaveChanges();
                                checkToken.is_expired = true;
                                checkToken.modified_on = DateTime.Now;
                                _miniProjDbContext.SaveChanges();
                                result.Entity = item;
                            }
                        }
                        else if (item.role_id == 2)
                        {
                            if (entity.email == null)
                            {
                                result.Entity = entity;
                                result.Message = "Silahkan isi email";
                                result.Success = false;
                            }
                            else
                            {
                                _miniProjDbContext.M_biodatas.Add(item2);
                                _miniProjDbContext.SaveChanges();
                                item.biodata_id = item2.id;
                                _miniProjDbContext.M_users.Add(item);
                                _miniProjDbContext.SaveChanges();
                                checkToken.is_expired = true;
                                checkToken.modified_on = DateTime.Now;
                                _miniProjDbContext.SaveChanges();
                                result.Entity = item;
                            }
                        }
                        else if (item.role_id == 3)
                        {
                            if (entity.email == null)
                            {
                                result.Entity = entity;
                                result.Message = "Silahkan isi email";
                                result.Success = false;
                            }
                            else
                            {
                                item5.created_by = 1;
                                item5.created_on = DateTime.Now;
                                _miniProjDbContext.M_biodatas.Add(item2);
                                _miniProjDbContext.SaveChanges();
                                item5.biodata_id = item2.id;
                                _miniProjDbContext.M_customers.Add(item5);
                                _miniProjDbContext.SaveChanges();
                                item.biodata_id = item2.id;
                                _miniProjDbContext.M_users.Add(item);
                                _miniProjDbContext.SaveChanges();
                                checkToken.is_expired = true;
                                checkToken.modified_on = DateTime.Now;
                                _miniProjDbContext.SaveChanges();
                                result.Entity = item;
                            }
                        }
                        else if (item.role_id == 4)
                        {
                            if (entity.email == null)
                            {
                                result.Entity = entity;
                                result.Message = "Silahkan isi email";
                                result.Success = false;
                            }
                            else
                            {
                                item4.created_by = 1;
                                item4.created_on = DateTime.Now;
                                _miniProjDbContext.M_biodatas.Add(item2);
                                _miniProjDbContext.SaveChanges();
                                item4.biodata_id = item2.id;
                                _miniProjDbContext.M_doctors.Add(item4);
                                _miniProjDbContext.SaveChanges();
                                item.biodata_id = item2.id;
                                _miniProjDbContext.M_users.Add(item);
                                _miniProjDbContext.SaveChanges();
                                checkToken.is_expired = true;
                                checkToken.modified_on = DateTime.Now;
                                _miniProjDbContext.SaveChanges();
                                result.Entity = item;
                            }
                        }
                        else
                        {
                            if (entity.email == null)
                            {
                                result.Entity = entity;
                                result.Message = "Silahkan isi email";
                                result.Success = false;
                            }
                            else
                            {
                                _miniProjDbContext.M_biodatas.Add(item2);
                                _miniProjDbContext.SaveChanges();
                                item.biodata_id = item2.id;
                                _miniProjDbContext.M_users.Add(item);
                                _miniProjDbContext.SaveChanges();
                                checkToken.is_expired = true;
                                checkToken.modified_on = DateTime.Now;
                                _miniProjDbContext.SaveChanges();
                                result.Entity = item;
                            }
                        }
                    }
                    else
                    {
                        result.Entity = entity;
                        result.Message = "Token Sudah Terpakai";
                        result.Success = false;
                    }
                }                
                else
                {
                    result.Entity = entity;
                    result.Message = "Token atau email anda salah";
                    result.Success = false;
                }
            }
            catch (Exception e)
            {

                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(PendaftaranViewModel entity)
        {
            throw new System.NotImplementedException();
        }

        public List<PendaftaranViewModel> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public PendaftaranViewModel GetById(long id)
        {
            throw new System.NotImplementedException();
        }

        public ResponseResult Update(PendaftaranViewModel entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
