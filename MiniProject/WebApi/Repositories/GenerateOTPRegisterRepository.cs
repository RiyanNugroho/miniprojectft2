﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using ViewModel;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class GenerateOTPRegisterRepository : IRepository<OTPRegisterViewModel>
    {
        private MiniProjDbContext _miniProjDbContext = new MiniProjDbContext();
        private ResponseResult result = new ResponseResult();
        SmtpClient client = new SmtpClient("smtp.gmail.com", 587);


        public ResponseResult Create(OTPRegisterViewModel entity)
        {
            try
            {
                Random rnd = new Random();
                string otp = rnd.Next(1000, 9999).ToString();
                T_token item = new T_token();
                item.email = entity.email;
                item.user_id = entity.user_id;
                item.token = otp;
                item.is_expired = false;
                item.used_for = "Register";

                item.created_on = DateTime.Now;

                T_token checkemail = _miniProjDbContext.T_tokens.Where(o => o.email == entity.email).FirstOrDefault();
                if (checkemail != null)
                {
                    result.Success = false;
                    result.Message = "Email sudah ada";
                    result.Entity = entity;
                }
                else
                {
                    _miniProjDbContext.T_tokens.Add(item);
                    _miniProjDbContext.SaveChanges();
                    result.Entity = item;
                }

            }
            catch (Exception e)
            {

                result.Message = e.Message;
                result.Success = false;
            }
            return result;

        }

        public ResponseResult Delete(OTPRegisterViewModel entity)
        {
            throw new System.NotImplementedException();
        }

        public List<OTPRegisterViewModel> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public OTPRegisterViewModel GetById(long id)
        {
            throw new System.NotImplementedException();
        }

        public ResponseResult Update(OTPRegisterViewModel entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
