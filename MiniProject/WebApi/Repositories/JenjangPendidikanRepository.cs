﻿using System;
using System.Collections.Generic;
using ViewModel;
using System.Linq;
using WebApi.DataModels;
using System.Data;

namespace WebApi.Repositories
{
    public class JenjangPendidikanRepository : IRepository<EducationLevelViewModel>
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();

        private ResponseResult result = new ResponseResult();

        private long _UserName;

        public JenjangPendidikanRepository()
        {
            _UserName = 1;
        }

        public JenjangPendidikanRepository(long username)
        {
            _UserName = username;
        }

        public ResponseResult Create(EducationLevelViewModel entity)
        {
            try
            {
                M_education_level item = new M_education_level();
                item.name = entity.name;

                item.created_by = _UserName;
                item.created_on = DateTime.Now;

                M_education_level check = _MiniProjDbContext.M_education_levels.Where(o => o.name == entity.name && !o.is_delete).FirstOrDefault();

                if (check != null)
                {
                    if (item.name == check.name && !check.is_delete)
                    {
                        result.Entity = entity;
                        result.Message = "Item Sudah Tersedia!";
                        result.Success = false;
                    }
                    else
                    {
                        _MiniProjDbContext.M_education_levels.Add(item);
                        //check.name = check.name + "_old";
                        _MiniProjDbContext.SaveChanges();
                        result.Entity = check;
                    }
                }
                else
                {
                    _MiniProjDbContext.M_education_levels.Add(item);
                    _MiniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
            }
            catch (Exception x)
            {
                result.Entity = entity;
                result.Message = x.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(EducationLevelViewModel entity)
        {
            try
            {
                M_education_level item = _MiniProjDbContext.M_education_levels
                    .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                if (item != null)
                {
                    item.is_delete = true;
                    item.deleted_by = _UserName;
                    item.deleted_on = DateTime.Now;

                    result.Entity = item;
                    _MiniProjDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Update(EducationLevelViewModel entity)
        {
            try
            {
                M_education_level item = _MiniProjDbContext.M_education_levels
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();

                M_education_level check = _MiniProjDbContext.M_education_levels.Where(o => o.name == entity.name && !o.is_delete).FirstOrDefault();

                if (item != null)
                {
                    item.name = entity.name;

                    item.modified_by = _UserName;
                    item.modified_on = DateTime.Now;

                    if (check != null)
                    {
                        result.Message = "Item yang diubah sudah tersedia!";
                        result.Success = false;
                    }
                    else
                    {
                        _MiniProjDbContext.SaveChanges();
                        result.Entity = item;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = "Item Tidak Ditemukan!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<EducationLevelViewModel> GetAll()
        {
            List<EducationLevelViewModel> result = new List<EducationLevelViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.M_education_levels
                          join biodata in _MiniProjDbContext.M_users
                          on o.created_by equals biodata.id into joinrole

                          from biodatas in _MiniProjDbContext.M_users.DefaultIfEmpty()
                          where o.is_delete != true
                          select new EducationLevelViewModel
                          {
                              id = o.id,
                              name = o.name,
                              dibuat_oleh = biodatas.m_biodata.fullname
                          }).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;

        }

        public EducationLevelViewModel GetById(long id)
        {
            EducationLevelViewModel result = new EducationLevelViewModel();
            try
            {
                result = (from o in _MiniProjDbContext.M_education_levels
                          where o.id == id && o.is_delete != true
                          select new EducationLevelViewModel
                          {
                              id = o.id,
                              name = o.name,
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;

        }

        public List<EducationLevelViewModel> ByFilter(string search)
        {

            List<EducationLevelViewModel> result = new List<EducationLevelViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.M_education_levels
                          .Where(o => o.is_delete != true && o.name.Contains(search))
                          select new EducationLevelViewModel
                          {
                              id = o.id,
                              name = o.name
                          }).ToList();
            }
            catch (Exception e)
            {

                string error = e.Message;
            }
            return result;
        }
    }
}
