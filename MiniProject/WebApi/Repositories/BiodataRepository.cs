﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class BiodataRepository : IRepository<BiodataViewModel>
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();
        private ResponseResult result = new ResponseResult();
        private long _userName;
        public BiodataRepository()
        {
            _userName = 1;
        }
        public BiodataRepository(long userName)
        {
            _userName = userName;
        }

        public ResponseResult Create(BiodataViewModel entity)
        {
            try
            {

                byte[] image = null;
                using (var fileStream = entity.FilePath.OpenReadStream())
                using (var memoryStream = new MemoryStream())
                {
                    fileStream.CopyTo(memoryStream);
                    image = memoryStream.ToArray();
                }


                M_biodata item = new M_biodata();
                item.fullname = entity.fullname;
                item.mobile_phone = entity.mobile_phone;
                item.image = image;
                item.image_path = entity.image_path;

                _MiniProjDbContext.M_biodatas.Add(item);
                _MiniProjDbContext.SaveChanges();

                result.Entity = item;
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Entity = entity;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(BiodataViewModel entity)
        {
            try
            {
                M_biodata item = _MiniProjDbContext.M_biodatas.Where(o => o.id == entity.id).FirstOrDefault();
                if (item != null)
                {

                    result.Entity = item;
                    _MiniProjDbContext.M_biodatas.Remove(item);
                    _MiniProjDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public async  Task<List<BiodataViewModel>> All()
        {
            //List<BiodataViewModel> result = new List<BiodataViewModel>();
            //try
            //{
            //    result = (from o in _MiniProjDbContext.M_biodatas
            //              select new BiodataViewModel
            //              {
            //                  id = o.id,
            //                  fullname = o.fullname,
            //                  mobile_phone = o.mobile_phone,
            //                  image = o.image,
            //                  image_path = o.image_path
            //              }).ToList();
            //}
            //catch (Exception e)
            //{
            //    string error = e.Message;
            //}
            return await ByFilter("");
        }

        public BiodataViewModel GetById(long id)
        {
            BiodataViewModel result = new BiodataViewModel();
            try
            {
                result = (from o in _MiniProjDbContext.M_biodatas
                          .Where(o => o.id == id)
                          select new BiodataViewModel
                          {
                              id = o.id,
                              fullname = o.fullname,
                              mobile_phone = o.mobile_phone,
                              image = o.image != null ? String.Format("data:image/png;base64,{0}", Convert.ToBase64String(o.image)) : "",
                              image_path = o.image_path
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(BiodataViewModel entity)
        {
            try
            {

                byte[] image = null;
                using (var fileStream = entity.FilePath.OpenReadStream())
                using (var memoryStream = new MemoryStream())
                {
                    fileStream.CopyTo(memoryStream);
                    image = memoryStream.ToArray();
                }

                M_biodata item = _MiniProjDbContext.M_biodatas
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                if (item != null)
                {
                    item.fullname = entity.fullname;
                    item.mobile_phone = entity.mobile_phone;
                    item.image = image;
                    item.image_path = entity.image_path;

                    _MiniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }
        public async Task<List<BiodataViewModel>> ByFilter(string search)
        {
            List<BiodataViewModel> result = new List<BiodataViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.M_biodatas
                          .Where (o => o.fullname.Contains(search))
                          select new BiodataViewModel
                          {
                              id = o.id,
                              fullname = o.fullname,
                              mobile_phone = o.mobile_phone,
                              image = o.image != null ? String.Format("data:image/png;base64,{0}", Convert.ToBase64String(o.image)) : "",
                              image_path = o.image_path
                          }).ToList();
            }
            catch (Exception e)
            {

                string error = e.Message;
            }
            return result;
        }

        List<BiodataViewModel> IRepository<BiodataViewModel>.GetAll()
        {
            throw new NotImplementedException();
        }


        //public async Task<ResponseResult> all(int page, int rows, string search)
        //{
        //    //List<ProductViewModel> result = new List<ProductViewModel>();
        //    ResponseResult result = new ResponseResult();
        //    try
        //    {
        //        var query = _MiniProjDbContext.M_biodatas
        //            .Where(o => o.fullname.Contains(search));

        //        result.Entity = query.Skip((page - 1) * rows)
        //            .Take(rows)
        //            .Select(o => new BiodataViewModel
        //            {
        //                id = o.id,
        //                fullname = o.fullname,
        //                mobile_phone = o.mobile_phone,
        //                image = o.image,
        //                image_path = o.image_path
        //            }).ToList();

        //        result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);
        //        if (result.Pages < page)
        //        {
        //           return await all(1, rows, search);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        string error = e.Message;
        //    }
        //    return result;
        // }


    }
}
