﻿using System.Collections.Generic;
using System;
using ViewModel;
using WebApi.DataModels;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Repositories
{
    public class MenuRepository : IRepository<MenuViewModel>
    {
        private MiniProjDbContext _MiniProjDbContext = new MiniProjDbContext();

        private ResponseResult result = new ResponseResult();

        private long _UserName;

        public MenuRepository()
        {
            _UserName = 1;
        }

        public MenuRepository(long username)
        {
            _UserName = username;
        }

        public ResponseResult Create(MenuViewModel entity)
        {
            try
            {
                M_menu item = new M_menu();
                item.name = entity.name;

                item.created_by = _UserName;
                item.created_on = DateTime.Now;

                _MiniProjDbContext.M_menus.Add(item);
                _MiniProjDbContext.SaveChanges();

                result.Entity = item;
            }
            catch (Exception e)
            {
                result.Entity = entity;
                result.Message = e.Message;
                result.Success = false;

            }
            return result;
        }

        public ResponseResult Delete(MenuViewModel entity)
        {
            try
            {
                M_menu item = _MiniProjDbContext.M_menus
                    .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                if (item != null)
                {
                    item.is_delete = true;
                    item.deleted_by = _UserName;
                    item.deleted_on = DateTime.Now;

                    result.Entity = item;
                    _MiniProjDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Update(MenuViewModel entity)
        {
            try
            {
                M_menu item = _MiniProjDbContext.M_menus
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                if (item != null)
                {
                    item.name = entity.name;

                    item.created_by = _UserName;
                    item.created_on = DateTime.Now;

                    item.modified_by = _UserName;
                    item.modified_on = DateTime.Now;

                    _MiniProjDbContext.SaveChanges();
                    result.Entity = item;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<MenuViewModel> GetAll()
        {
            List<MenuViewModel> result = new List<MenuViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.M_menus
                          where o.is_delete != true && o.parent_id == null
                          select new MenuViewModel
                          {
                              id = o.id,
                              name = o.name,
                              url = o.url,


                          }).ToList();
                foreach (var item in result)
                {
                    item.SubMenu = (from o in _MiniProjDbContext.M_menus
                                    where o.is_delete != true && o.parent_id == item.id
                                    select new MenuViewModel
                                    {
                                        id = o.id,
                                        name = o.name,
                                        url = o.url,
                                        parent_id = o.parent_id,
                                    }).ToList();
                }
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;

        }

        public MenuViewModel GetById(long id)
        {
            MenuViewModel result = new MenuViewModel();
            try
            {
                result = (from o in _MiniProjDbContext.M_menus
                          where o.id == id && o.is_delete != true
                          select new MenuViewModel
                          {
                              id = o.id,
                              name = o.name,
                              url = o.url,
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;

        }

        public List<MenuViewModel> ByFilter(string search)
        {

            List<MenuViewModel> result = new List<MenuViewModel>();
            try
            {
                result = (from o in _MiniProjDbContext.M_menus
                          .Where(o => o.is_delete != true && o.name.Contains(search))
                          select new MenuViewModel
                          {
                              id = o.id,
                              name = o.name,
                              url = o.url,
                          }).ToList();
            }
            catch (Exception e)
            {

                string error = e.Message;
            }
            return result;
        }
    }
}