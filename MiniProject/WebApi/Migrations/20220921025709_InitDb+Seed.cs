﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class InitDbSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "M_banks",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 100, nullable: true),
                    va_code = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_banks", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "M_biodatas",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    fullname = table.Column<string>(maxLength: 255, nullable: true),
                    mobile_phone = table.Column<string>(maxLength: 15, nullable: true),
                    image = table.Column<byte[]>(nullable: true),
                    image_path = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_biodatas", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "M_blood_groups",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    code = table.Column<string>(maxLength: 5, nullable: true),
                    description = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_blood_groups", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "M_couriers",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    description = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_couriers", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "M_customer_relations",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_customer_relations", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "M_education_levels",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_education_levels", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "M_location_levels",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 50, nullable: true),
                    abbreviation = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_location_levels", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "M_medical_facility_categories",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_medical_facility_categories", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "M_medical_item_categories",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_medical_item_categories", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "M_medical_item_segmentations",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_medical_item_segmentations", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "M_menus",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 20, nullable: true),
                    url = table.Column<string>(maxLength: 50, nullable: true),
                    parent_id = table.Column<long>(nullable: true),
                    big_icon = table.Column<string>(maxLength: 100, nullable: true),
                    small_icon = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_menus", x => x.id);
                    table.ForeignKey(
                        name: "FK_M_menus_M_menus_parent_id",
                        column: x => x.parent_id,
                        principalTable: "M_menus",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "M_payment_methods",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_payment_methods", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "M_roles",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 20, nullable: true),
                    code = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_roles", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "M_specializations",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_specializations", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "M_wallet_default_nominals",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    nominal = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_wallet_default_nominals", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "T_reset_passwords",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    old_password = table.Column<string>(maxLength: 255, nullable: true),
                    new_password = table.Column<string>(maxLength: 255, nullable: true),
                    reset_for = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_reset_passwords", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "M_admins",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    biodata_id = table.Column<long>(nullable: true),
                    code = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_admins", x => x.id);
                    table.ForeignKey(
                        name: "FK_M_admins_M_biodatas_biodata_id",
                        column: x => x.biodata_id,
                        principalTable: "M_biodatas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "M_doctors",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    biodata_id = table.Column<long>(nullable: false),
                    str = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_doctors", x => x.id);
                    table.ForeignKey(
                        name: "FK_M_doctors_M_biodatas_biodata_id",
                        column: x => x.biodata_id,
                        principalTable: "M_biodatas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "M_customers",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    biodata_id = table.Column<long>(nullable: true),
                    dob = table.Column<DateTime>(nullable: true),
                    gender = table.Column<string>(maxLength: 1, nullable: true),
                    blood_group_id = table.Column<long>(nullable: true),
                    rhesus_type = table.Column<string>(maxLength: 5, nullable: true),
                    height = table.Column<decimal>(nullable: true),
                    weight = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_customers", x => x.id);
                    table.ForeignKey(
                        name: "FK_M_customers_M_biodatas_biodata_id",
                        column: x => x.biodata_id,
                        principalTable: "M_biodatas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_M_customers_M_blood_groups_blood_group_id",
                        column: x => x.blood_group_id,
                        principalTable: "M_blood_groups",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "M_courier_types",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    courier_id = table.Column<long>(nullable: true),
                    name = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_courier_types", x => x.id);
                    table.ForeignKey(
                        name: "FK_M_courier_types_M_couriers_courier_id",
                        column: x => x.courier_id,
                        principalTable: "M_couriers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "M_locations",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 100, nullable: true),
                    parent_id = table.Column<long>(nullable: true),
                    location_level_id = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_locations", x => x.id);
                    table.ForeignKey(
                        name: "FK_M_locations_M_location_levels_location_level_id",
                        column: x => x.location_level_id,
                        principalTable: "M_location_levels",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_M_locations_M_locations_parent_id",
                        column: x => x.parent_id,
                        principalTable: "M_locations",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "M_medical_items",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 50, nullable: true),
                    medical_item_category_id = table.Column<long>(nullable: true),
                    composition = table.Column<string>(nullable: true),
                    medical_item_segmentation_id = table.Column<long>(nullable: true),
                    manufacturer = table.Column<string>(maxLength: 100, nullable: true),
                    indication = table.Column<string>(nullable: true),
                    dosage = table.Column<string>(nullable: true),
                    directions = table.Column<string>(nullable: true),
                    contraindication = table.Column<string>(nullable: true),
                    caution = table.Column<string>(nullable: true),
                    packaging = table.Column<string>(maxLength: 50, nullable: true),
                    price_max = table.Column<long>(nullable: false),
                    price_min = table.Column<long>(nullable: false),
                    image = table.Column<byte[]>(nullable: true),
                    image_path = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_medical_items", x => x.id);
                    table.ForeignKey(
                        name: "FK_M_medical_items_M_medical_item_categories_medical_item_category_id",
                        column: x => x.medical_item_category_id,
                        principalTable: "M_medical_item_categories",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_M_medical_items_M_medical_item_segmentations_medical_item_segmentation_id",
                        column: x => x.medical_item_segmentation_id,
                        principalTable: "M_medical_item_segmentations",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "M_biodata_attachments",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    biodata_id = table.Column<long>(nullable: true),
                    file_name = table.Column<string>(maxLength: 50, nullable: true),
                    file_path = table.Column<string>(maxLength: 100, nullable: true),
                    file_size = table.Column<int>(nullable: false),
                    file = table.Column<byte[]>(nullable: true),
                    role_id = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_biodata_attachments", x => x.id);
                    table.ForeignKey(
                        name: "FK_M_biodata_attachments_M_biodatas_biodata_id",
                        column: x => x.biodata_id,
                        principalTable: "M_biodatas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_M_biodata_attachments_M_roles_role_id",
                        column: x => x.role_id,
                        principalTable: "M_roles",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "M_menu_roles",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    menu_id = table.Column<long>(nullable: true),
                    role_id = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_menu_roles", x => x.id);
                    table.ForeignKey(
                        name: "FK_M_menu_roles_M_menus_menu_id",
                        column: x => x.menu_id,
                        principalTable: "M_menus",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_M_menu_roles_M_roles_role_id",
                        column: x => x.role_id,
                        principalTable: "M_roles",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "M_users",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    biodata_id = table.Column<long>(nullable: true),
                    role_id = table.Column<long>(nullable: true),
                    email = table.Column<string>(maxLength: 100, nullable: true),
                    password = table.Column<string>(maxLength: 255, nullable: true),
                    login_attempt = table.Column<int>(nullable: true),
                    is_locked = table.Column<bool>(nullable: true),
                    last_login = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_users", x => x.id);
                    table.ForeignKey(
                        name: "FK_M_users_M_biodatas_biodata_id",
                        column: x => x.biodata_id,
                        principalTable: "M_biodatas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_M_users_M_roles_role_id",
                        column: x => x.role_id,
                        principalTable: "M_roles",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "M_doctor_educations",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    doctor_id = table.Column<long>(nullable: true),
                    education_level_id = table.Column<long>(nullable: true),
                    institution_name = table.Column<string>(maxLength: 100, nullable: true),
                    major = table.Column<string>(maxLength: 100, nullable: true),
                    start_year = table.Column<string>(maxLength: 4, nullable: true),
                    end_year = table.Column<string>(maxLength: 4, nullable: true),
                    is_last_education = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_doctor_educations", x => x.id);
                    table.ForeignKey(
                        name: "FK_M_doctor_educations_M_doctors_doctor_id",
                        column: x => x.doctor_id,
                        principalTable: "M_doctors",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_M_doctor_educations_M_education_levels_education_level_id",
                        column: x => x.education_level_id,
                        principalTable: "M_education_levels",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_current_doctor_specializations",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    doctor_id = table.Column<long>(nullable: true),
                    specialization_id = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_current_doctor_specializations", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_current_doctor_specializations_M_doctors_doctor_id",
                        column: x => x.doctor_id,
                        principalTable: "M_doctors",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_T_current_doctor_specializations_M_specializations_specialization_id",
                        column: x => x.specialization_id,
                        principalTable: "M_specializations",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_doctor_treatments",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    doctor_id = table.Column<long>(nullable: true),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_doctor_treatments", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_doctor_treatments_M_doctors_doctor_id",
                        column: x => x.doctor_id,
                        principalTable: "M_doctors",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "M_customer_members",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    customer_id = table.Column<long>(nullable: true),
                    customer_relation_id = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_customer_members", x => x.id);
                    table.ForeignKey(
                        name: "FK_M_customer_members_M_customers_customer_id",
                        column: x => x.customer_id,
                        principalTable: "M_customers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_M_customer_members_M_customer_relations_customer_relation_id",
                        column: x => x.customer_relation_id,
                        principalTable: "M_customer_relations",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_customer_chats",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    customer_id = table.Column<long>(nullable: true),
                    doctor_id = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_customer_chats", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_customer_chats_M_customers_customer_id",
                        column: x => x.customer_id,
                        principalTable: "M_customers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_T_customer_chats_M_doctors_doctor_id",
                        column: x => x.doctor_id,
                        principalTable: "M_doctors",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_customer_custom_nominals",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    customer_id = table.Column<long>(nullable: true),
                    nominal = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_customer_custom_nominals", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_customer_custom_nominals_M_customers_customer_id",
                        column: x => x.customer_id,
                        principalTable: "M_customers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_customer_registered_cards",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    customer_id = table.Column<long>(nullable: true),
                    card_number = table.Column<string>(maxLength: 20, nullable: true),
                    validity_period = table.Column<DateTime>(nullable: false),
                    cvv = table.Column<string>(maxLength: 5, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_customer_registered_cards", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_customer_registered_cards_M_customers_customer_id",
                        column: x => x.customer_id,
                        principalTable: "M_customers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_customer_vas",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    customer_id = table.Column<long>(nullable: true),
                    va_number = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_customer_vas", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_customer_vas_M_customers_customer_id",
                        column: x => x.customer_id,
                        principalTable: "M_customers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_customer_wallet_withdraws",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    customer_id = table.Column<long>(nullable: true),
                    wallet_default_nominal_id = table.Column<long>(nullable: false),
                    amount = table.Column<int>(nullable: false),
                    bank_name = table.Column<string>(maxLength: 50, nullable: true),
                    account_number = table.Column<string>(maxLength: 50, nullable: true),
                    account_name = table.Column<string>(maxLength: 255, nullable: true),
                    otp = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_customer_wallet_withdraws", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_customer_wallet_withdraws_M_customers_customer_id",
                        column: x => x.customer_id,
                        principalTable: "M_customers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_medical_item_purchases",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    customer_id = table.Column<long>(nullable: true),
                    payment_method_id = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_medical_item_purchases", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_medical_item_purchases_M_customers_customer_id",
                        column: x => x.customer_id,
                        principalTable: "M_customers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_T_medical_item_purchases_M_payment_methods_payment_method_id",
                        column: x => x.payment_method_id,
                        principalTable: "M_payment_methods",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_courier_discounts",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    courier_type_id = table.Column<long>(nullable: true),
                    value = table.Column<decimal>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_courier_discounts", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_courier_discounts_M_courier_types_courier_type_id",
                        column: x => x.courier_type_id,
                        principalTable: "M_courier_types",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "M_biodata_address",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    biodata_id = table.Column<long>(nullable: true),
                    label = table.Column<string>(maxLength: 100, nullable: true),
                    recipient = table.Column<string>(maxLength: 100, nullable: true),
                    recipient_phone_number = table.Column<string>(maxLength: 15, nullable: true),
                    location_id = table.Column<long>(nullable: true),
                    postal_code = table.Column<string>(maxLength: 10, nullable: true),
                    address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_biodata_address", x => x.id);
                    table.ForeignKey(
                        name: "FK_M_biodata_address_M_biodatas_biodata_id",
                        column: x => x.biodata_id,
                        principalTable: "M_biodatas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_M_biodata_address_M_locations_location_id",
                        column: x => x.location_id,
                        principalTable: "M_locations",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "M_medical_facilities",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    name = table.Column<string>(maxLength: 50, nullable: true),
                    medical_facility_category_id = table.Column<long>(nullable: true),
                    location_id = table.Column<long>(nullable: true),
                    full_address = table.Column<string>(nullable: true),
                    email = table.Column<string>(maxLength: 100, nullable: true),
                    phone_code = table.Column<string>(maxLength: 10, nullable: true),
                    phone = table.Column<string>(maxLength: 15, nullable: true),
                    fax = table.Column<string>(maxLength: 15, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_medical_facilities", x => x.id);
                    table.ForeignKey(
                        name: "FK_M_medical_facilities_M_locations_location_id",
                        column: x => x.location_id,
                        principalTable: "M_locations",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_M_medical_facilities_M_medical_facility_categories_medical_facility_category_id",
                        column: x => x.medical_facility_category_id,
                        principalTable: "M_medical_facility_categories",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_tokens",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    email = table.Column<string>(maxLength: 100, nullable: true),
                    user_id = table.Column<long>(nullable: true),
                    token = table.Column<string>(maxLength: 50, nullable: true),
                    expired_on = table.Column<DateTime>(nullable: true),
                    is_expired = table.Column<bool>(nullable: true),
                    used_for = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_tokens", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_tokens_M_users_user_id",
                        column: x => x.user_id,
                        principalTable: "M_users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_doctor_office_treatment_prices",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    doctor_treatment_id = table.Column<long>(nullable: true),
                    price = table.Column<decimal>(nullable: false),
                    price_start_from = table.Column<decimal>(nullable: false),
                    price_until_from = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_doctor_office_treatment_prices", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_doctor_office_treatment_prices_T_doctor_treatments_doctor_treatment_id",
                        column: x => x.doctor_treatment_id,
                        principalTable: "T_doctor_treatments",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_customer_chat_histories",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    customer_chat_id = table.Column<long>(nullable: true),
                    chat_content = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_customer_chat_histories", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_customer_chat_histories_T_customer_chats_customer_chat_id",
                        column: x => x.customer_chat_id,
                        principalTable: "T_customer_chats",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_customer_va_histories",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    customer_va_id = table.Column<long>(nullable: true),
                    amount = table.Column<decimal>(nullable: false),
                    expired_on = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_customer_va_histories", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_customer_va_histories_T_customer_vas_customer_va_id",
                        column: x => x.customer_va_id,
                        principalTable: "T_customer_vas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_customer_wallets",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    customer_id = table.Column<long>(nullable: true),
                    pin = table.Column<string>(maxLength: 6, nullable: true),
                    balance = table.Column<decimal>(nullable: false),
                    barcode = table.Column<string>(maxLength: 50, nullable: true),
                    points = table.Column<decimal>(nullable: false),
                    customer_va_id = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_customer_wallets", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_customer_wallets_T_customer_vas_customer_va_id",
                        column: x => x.customer_va_id,
                        principalTable: "T_customer_vas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "M_medical_facility_schedules",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    medical_facility_id = table.Column<long>(nullable: true),
                    day = table.Column<string>(maxLength: 10, nullable: true),
                    time_schedule_start = table.Column<string>(maxLength: 10, nullable: true),
                    time_schedule_end = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_medical_facility_schedules", x => x.id);
                    table.ForeignKey(
                        name: "FK_M_medical_facility_schedules_M_medical_facilities_medical_facility_id",
                        column: x => x.medical_facility_id,
                        principalTable: "M_medical_facilities",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_doctor_office_schedules",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    doctor_id = table.Column<long>(nullable: true),
                    medical_facility_id = table.Column<long>(nullable: true),
                    specialization = table.Column<string>(maxLength: 100, nullable: true),
                    slot = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_doctor_office_schedules", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_doctor_office_schedules_M_doctors_doctor_id",
                        column: x => x.doctor_id,
                        principalTable: "M_doctors",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_T_doctor_office_schedules_M_medical_facilities_medical_facility_id",
                        column: x => x.medical_facility_id,
                        principalTable: "M_medical_facilities",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_doctor_offices",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    doctor_id = table.Column<long>(nullable: true),
                    medical_facility_id = table.Column<long>(nullable: true),
                    specialization = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_doctor_offices", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_doctor_offices_M_doctors_doctor_id",
                        column: x => x.doctor_id,
                        principalTable: "M_doctors",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_T_doctor_offices_M_medical_facilities_medical_facility_id",
                        column: x => x.medical_facility_id,
                        principalTable: "M_medical_facilities",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_medical_item_purchase_details",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    medical_item_purchase_id = table.Column<long>(nullable: true),
                    medical_item_id = table.Column<long>(nullable: true),
                    qty = table.Column<int>(nullable: false),
                    medical_facility_id = table.Column<long>(nullable: true),
                    courier_id = table.Column<long>(nullable: true),
                    sub_total = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_medical_item_purchase_details", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_medical_item_purchase_details_M_couriers_courier_id",
                        column: x => x.courier_id,
                        principalTable: "M_couriers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_T_medical_item_purchase_details_M_medical_facilities_medical_facility_id",
                        column: x => x.medical_facility_id,
                        principalTable: "M_medical_facilities",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_T_medical_item_purchase_details_M_medical_items_medical_item_id",
                        column: x => x.medical_item_id,
                        principalTable: "M_medical_items",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_T_medical_item_purchase_details_T_medical_item_purchases_medical_item_purchase_id",
                        column: x => x.medical_item_purchase_id,
                        principalTable: "T_medical_item_purchases",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_treatment_discounts",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    doctor_office_treatment_price_id = table.Column<long>(maxLength: 50, nullable: true),
                    value = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_treatment_discounts", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_treatment_discounts_T_doctor_office_treatment_prices_doctor_office_treatment_price_id",
                        column: x => x.doctor_office_treatment_price_id,
                        principalTable: "T_doctor_office_treatment_prices",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_customer_wallet_top_ups",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    customer_wallet_id = table.Column<long>(nullable: true),
                    amount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_customer_wallet_top_ups", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_customer_wallet_top_ups_T_customer_wallets_customer_wallet_id",
                        column: x => x.customer_wallet_id,
                        principalTable: "T_customer_wallets",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_doctor_office_treatments",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    doctor_treatment_id = table.Column<long>(nullable: true),
                    doctor_office_id = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_doctor_office_treatments", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_doctor_office_treatments_T_doctor_offices_doctor_office_id",
                        column: x => x.doctor_office_id,
                        principalTable: "T_doctor_offices",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_T_doctor_office_treatments_T_doctor_treatments_doctor_treatment_id",
                        column: x => x.doctor_treatment_id,
                        principalTable: "T_doctor_treatments",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_appointments",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    customer_id = table.Column<long>(nullable: true),
                    doctor_office_id = table.Column<long>(nullable: true),
                    doctor_office_schedule_id = table.Column<long>(nullable: true),
                    doctor_office_treatment_id = table.Column<long>(nullable: true),
                    appointment_date = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_appointments", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_appointments_M_customers_customer_id",
                        column: x => x.customer_id,
                        principalTable: "M_customers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_T_appointments_T_doctor_offices_doctor_office_id",
                        column: x => x.doctor_office_id,
                        principalTable: "T_doctor_offices",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_T_appointments_T_doctor_office_schedules_doctor_office_schedule_id",
                        column: x => x.doctor_office_schedule_id,
                        principalTable: "T_doctor_office_schedules",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_T_appointments_T_doctor_office_treatments_doctor_office_treatment_id",
                        column: x => x.doctor_office_treatment_id,
                        principalTable: "T_doctor_office_treatments",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_appointment_cancellations",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    appointment_id = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_appointment_cancellations", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_appointment_cancellations_T_appointments_appointment_id",
                        column: x => x.appointment_id,
                        principalTable: "T_appointments",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_appointment_dones",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    appointment_id = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_appointment_dones", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_appointment_dones_T_appointments_appointment_id",
                        column: x => x.appointment_id,
                        principalTable: "T_appointments",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "T_appointment_reschedule_histories",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: true),
                    modified_on = table.Column<DateTime>(nullable: true),
                    deleted_by = table.Column<long>(nullable: true),
                    deleted_on = table.Column<DateTime>(nullable: true),
                    is_delete = table.Column<bool>(nullable: false),
                    appointment_id = table.Column<long>(nullable: true),
                    doctor_office_schedule_id = table.Column<long>(nullable: true),
                    doctor_office_treatment_id = table.Column<long>(nullable: true),
                    appointment_date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_appointment_reschedule_histories", x => x.id);
                    table.ForeignKey(
                        name: "FK_T_appointment_reschedule_histories_T_appointments_appointment_id",
                        column: x => x.appointment_id,
                        principalTable: "T_appointments",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_T_appointment_reschedule_histories_T_doctor_office_schedules_doctor_office_schedule_id",
                        column: x => x.doctor_office_schedule_id,
                        principalTable: "T_doctor_office_schedules",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_T_appointment_reschedule_histories_T_doctor_office_treatments_doctor_office_treatment_id",
                        column: x => x.doctor_office_treatment_id,
                        principalTable: "T_doctor_office_treatments",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "M_banks",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "name", "va_code" },
                values: new object[,]
                {
                    { 1L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(4614), null, null, false, null, null, "Bank BCA", "00834221" },
                    { 2L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(6100), null, null, false, null, null, "Bank BNI", "03127532" },
                    { 3L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(6146), null, null, false, null, null, "Bank Mandiri", "15209312" },
                    { 4L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(6149), null, null, false, null, null, "Bank Permata", "16591734" },
                    { 5L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(6151), null, null, false, null, null, "Bank BTPN", "00682637" },
                    { 6L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(6154), null, null, false, null, null, "Bank NISP", "78231424" }
                });

            migrationBuilder.InsertData(
                table: "M_biodatas",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "fullname", "image", "image_path", "is_delete", "mobile_phone", "modified_by", "modified_on" },
                values: new object[,]
                {
                    { 9L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 149, DateTimeKind.Local).AddTicks(505), null, null, "Eiga Putri", null, "", false, "081603994824", null, null },
                    { 8L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 149, DateTimeKind.Local).AddTicks(503), null, null, "Anisa Ayu", null, "", false, "085683049583", null, null },
                    { 7L, 13L, new DateTime(2022, 9, 21, 9, 57, 8, 149, DateTimeKind.Local).AddTicks(501), null, null, "Tatjana Saphira, Sp. A", null, "", false, "087878909876", null, null },
                    { 6L, 11L, new DateTime(2022, 9, 21, 9, 57, 8, 149, DateTimeKind.Local).AddTicks(499), null, null, "Riyan Eko Nugroho", null, "", false, "087878909876", null, null },
                    { 10L, 10L, new DateTime(2022, 9, 21, 9, 57, 8, 149, DateTimeKind.Local).AddTicks(508), null, null, "Raden Raihan", null, "", false, "089899526623", null, null },
                    { 4L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 149, DateTimeKind.Local).AddTicks(493), null, null, "Samsi Satria Rama", null, "", false, "087878902468", null, null },
                    { 3L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 149, DateTimeKind.Local).AddTicks(490), null, null, "Muhammad Fariz Jusuf", null, "", false, "087878909101", null, null },
                    { 2L, 3L, new DateTime(2022, 9, 21, 9, 57, 8, 149, DateTimeKind.Local).AddTicks(452), null, null, "Muhammad Birril Jasur", null, "", false, "087878905678", null, null },
                    { 1L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 147, DateTimeKind.Local).AddTicks(7422), null, null, "Indra Oloan Simatupang", null, "", false, "087878901234", null, null },
                    { 5L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 149, DateTimeKind.Local).AddTicks(496), null, null, "Sebastian Fajar Juanito", null, "", false, "087878901357", null, null }
                });

            migrationBuilder.InsertData(
                table: "M_blood_groups",
                columns: new[] { "id", "code", "created_by", "created_on", "deleted_by", "deleted_on", "description", "is_delete", "modified_by", "modified_on" },
                values: new object[,]
                {
                    { 1L, "A", 9L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(8672), null, null, "Golongan A", false, null, null },
                    { 2L, "B", 9L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(8715), null, null, "Golongan B", false, null, null },
                    { 3L, "O", 9L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(8718), null, null, "Golongan O", false, null, null },
                    { 4L, "AB", 9L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(8720), null, null, "Golongan AB", false, null, null }
                });

            migrationBuilder.InsertData(
                table: "M_customer_relations",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "name" },
                values: new object[,]
                {
                    { 3L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(7312), null, null, false, null, null, "Anak" },
                    { 2L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(7308), null, null, false, null, null, "Ibu" },
                    { 1L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(7270), null, null, false, null, null, "Ayah" }
                });

            migrationBuilder.InsertData(
                table: "M_education_levels",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "name" },
                values: new object[,]
                {
                    { 1L, 3L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(965), null, null, false, null, null, "S1" },
                    { 2L, 3L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(1003), null, null, false, null, null, "S2" },
                    { 3L, 3L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(1005), null, null, false, null, null, "S3" }
                });

            migrationBuilder.InsertData(
                table: "M_location_levels",
                columns: new[] { "id", "abbreviation", "created_by", "created_on", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "name" },
                values: new object[,]
                {
                    { 5L, "Kec.", 9L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(8864), null, null, false, null, null, "Kecamatan" },
                    { 1L, "Prov.", 9L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(8812), null, null, false, null, null, "Provinsi" },
                    { 2L, "Kot.", 9L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(8858), null, null, false, null, null, "Kota" },
                    { 3L, "Kab.", 9L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(8860), null, null, false, null, null, "Kabupaten" },
                    { 4L, "Kel.", 9L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(8862), null, null, false, null, null, "Kelurahan" }
                });

            migrationBuilder.InsertData(
                table: "M_medical_facility_categories",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "name" },
                values: new object[,]
                {
                    { 3L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(9876), null, null, false, null, null, "Klinik" },
                    { 4L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(9878), null, null, false, null, null, "Puskesmas" },
                    { 1L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(9810), null, null, false, null, null, "Apotek" },
                    { 2L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(9873), null, null, false, null, null, "Rumah Sakit" }
                });

            migrationBuilder.InsertData(
                table: "M_medical_item_categories",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "name" },
                values: new object[,]
                {
                    { 1L, 11L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(3209), null, null, false, null, null, "Demam" },
                    { 2L, 11L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(3248), null, null, false, null, null, "Batuk" },
                    { 3L, 11L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(3250), null, null, false, null, null, "Pilek" },
                    { 4L, 11L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(3253), null, null, false, null, null, "Masuk Angin" },
                    { 5L, 11L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(3255), null, null, false, null, null, "Infeksi" }
                });

            migrationBuilder.InsertData(
                table: "M_medical_item_segmentations",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "name" },
                values: new object[,]
                {
                    { 1L, 11L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(2089), null, null, false, null, null, "Obat Herbal Terstandar (OHT)" },
                    { 2L, 11L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(2128), null, null, false, null, null, "Obat Bebas (Hijau)" },
                    { 3L, 11L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(2131), null, null, false, null, null, "Obat Bebas Terbatas (Biru)" },
                    { 4L, 11L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(2133), null, null, false, null, null, "Obat Keras (Merah)" }
                });

            migrationBuilder.InsertData(
                table: "M_menus",
                columns: new[] { "id", "big_icon", "created_by", "created_on", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "name", "parent_id", "small_icon", "url" },
                values: new object[,]
                {
                    { 1L, " ", 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(1161), null, null, false, null, null, "Master", null, " ", "" },
                    { 11L, " ", 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(1255), null, null, false, null, null, "Transaksi", null, " ", "" }
                });

            migrationBuilder.InsertData(
                table: "M_roles",
                columns: new[] { "id", "code", "created_by", "created_on", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "name" },
                values: new object[,]
                {
                    { 4L, "Dokter", 9L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(3760), null, null, false, null, null, "Dokter" },
                    { 3L, "Pasien", 9L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(3758), null, null, false, null, null, "Pasien" },
                    { 5L, "Umum", 9L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(3763), null, null, false, null, null, "Umum" },
                    { 1L, "Admin", 9L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(3681), null, null, false, null, null, "Administrator" },
                    { 2L, "Faskes", 9L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(3756), null, null, false, null, null, "Fasilitas Kesehatan" }
                });

            migrationBuilder.InsertData(
                table: "M_specializations",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "name" },
                values: new object[,]
                {
                    { 7L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(9365), null, null, false, null, null, "Spesialisasi Jantung" },
                    { 6L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(9363), null, null, false, null, null, "Spesialisasi Mata" },
                    { 5L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(9361), null, null, false, null, null, "Spesialisasi THT" },
                    { 8L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(9367), null, null, false, null, null, "Spesialisasi Paru-Paru" },
                    { 3L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(9358), null, null, false, null, null, "Spesialisasi Gigi" },
                    { 2L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(9355), null, null, false, null, null, "Spesialisasi Tulang" },
                    { 1L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(9285), null, null, false, null, null, "Spesialisasi Anak" },
                    { 4L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(9360), null, null, false, null, null, "Spesialisasi Kulit" }
                });

            migrationBuilder.InsertData(
                table: "M_wallet_default_nominals",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "nominal" },
                values: new object[,]
                {
                    { 1L, 6L, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, 50000 },
                    { 2L, 6L, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, 100000 },
                    { 3L, 6L, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, 150000 },
                    { 4L, 6L, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, null, 200000 }
                });

            migrationBuilder.InsertData(
                table: "T_customer_wallets",
                columns: new[] { "id", "balance", "barcode", "created_by", "created_on", "customer_id", "customer_va_id", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "pin", "points" },
                values: new object[,]
                {
                    { 5L, 1300000m, "L4eiQCrgBA3xmxIISlBzzbqGt3mamKa4llO1ihc5qKqE0nDJvl", 6L, new DateTime(2022, 9, 21, 9, 57, 8, 155, DateTimeKind.Local).AddTicks(2247), 5L, null, null, null, false, null, null, "654321", 100m },
                    { 1L, 1150000m, "hypPLDdUbBFCyqWYZFLzCbU5ke5zvEFCNxvGjHqjopAdZ6ysBh", 6L, new DateTime(2022, 9, 21, 9, 57, 8, 155, DateTimeKind.Local).AddTicks(2170), 1L, null, null, null, false, null, null, "123456", 100m },
                    { 2L, 1150000m, "fgTBfaaafaTxseecKtCtgYQOFfrzRmUhousthSxzdmboFeaxRH", 6L, new DateTime(2022, 9, 21, 9, 57, 8, 155, DateTimeKind.Local).AddTicks(2237), 2L, null, null, null, false, null, null, "123456", 100m },
                    { 3L, 1150000m, "TRgWkmhYtqsXyXbZyZDUNymFArmdIOghxlNqltKCTaTbBuYkcu", 6L, new DateTime(2022, 9, 21, 9, 57, 8, 155, DateTimeKind.Local).AddTicks(2241), 3L, null, null, null, false, null, null, "123456", 100m },
                    { 4L, 1150000m, "NweUlSibshbLaiSRfKIbJoZilUluAYroSEeSDAVeHLKsclzgGr", 6L, new DateTime(2022, 9, 21, 9, 57, 8, 155, DateTimeKind.Local).AddTicks(2244), 4L, null, null, null, false, null, null, "123456", 100m },
                    { 6L, 1500000m, "cnryVXvdO5cgCacL6wiTbjA2tAw15PbZxqhlwRPvOWvo8FYRP0", 6L, new DateTime(2022, 9, 21, 9, 57, 8, 155, DateTimeKind.Local).AddTicks(2250), 6L, null, null, null, false, null, null, "098765", 100m }
                });

            migrationBuilder.InsertData(
                table: "M_customers",
                columns: new[] { "id", "biodata_id", "blood_group_id", "created_by", "created_on", "deleted_by", "deleted_on", "dob", "gender", "height", "is_delete", "modified_by", "modified_on", "rhesus_type", "weight" },
                values: new object[,]
                {
                    { 1L, 3L, 1L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(8090), null, null, new DateTime(1999, 7, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "L", 177m, false, null, null, "Rh+", 70m },
                    { 3L, 9L, 1L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(8177), null, null, new DateTime(2006, 3, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "P", 130m, false, null, null, "Rh+", 40m },
                    { 4L, 10L, 1L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(8181), null, null, new DateTime(1970, 12, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "L", 170m, false, null, null, "Rh-", 60m },
                    { 2L, 8L, 3L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(8173), null, null, new DateTime(1999, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "P", 168m, false, null, null, "Rh-", 64m },
                    { 5L, 4L, 4L, 5L, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, new DateTime(1999, 9, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "L", 167m, false, null, null, "Rh+", 65m },
                    { 6L, 6L, 4L, 5L, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, new DateTime(1999, 8, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "L", 167m, false, null, null, "Rh+", 73m }
                });

            migrationBuilder.InsertData(
                table: "M_doctors",
                columns: new[] { "id", "biodata_id", "created_by", "created_on", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "str" },
                values: new object[,]
                {
                    { 1L, 2L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(4657), null, null, false, null, null, "12345678901" },
                    { 2L, 4L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(4699), null, null, false, null, null, "23456789021" },
                    { 3L, 7L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 152, DateTimeKind.Local).AddTicks(4702), null, null, false, null, null, "34567890312" }
                });

            migrationBuilder.InsertData(
                table: "M_locations",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "is_delete", "location_level_id", "modified_by", "modified_on", "name", "parent_id" },
                values: new object[,]
                {
                    { 1L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(1223), null, null, false, 1L, null, null, "DKI Jakarta", null },
                    { 2L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(1272), null, null, false, 1L, null, null, "Jawa Barat", null }
                });

            migrationBuilder.InsertData(
                table: "M_menus",
                columns: new[] { "id", "big_icon", "created_by", "created_on", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "name", "parent_id", "small_icon", "url" },
                values: new object[,]
                {
                    { 13L, " ", 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(1259), null, null, false, null, null, "Cari Obat", 11L, " ", "CariObat" },
                    { 12L, " ", 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(1257), null, null, false, null, null, "Cari Dokter", 11L, " ", "CariDokter" },
                    { 9L, " ", 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(1250), null, null, false, null, null, "Lokasi", 1L, " ", "Lokasi" },
                    { 8L, " ", 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(1248), null, null, false, null, null, "Golongan Darah", 1L, " ", "GolonganDarah" },
                    { 7L, " ", 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(1245), null, null, false, null, null, "Segmen ProdKes", 1L, " ", "SegmenProdKes" },
                    { 10L, " ", 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(1252), null, null, false, null, null, "Setting", 1L, " ", "Setting" },
                    { 5L, " ", 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(1241), null, null, false, null, null, "Hubungan Pasien", 1L, " ", "HubunganPasien" },
                    { 4L, " ", 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(1238), null, null, false, null, null, "Kategori Faskes", 1L, " ", "KategoriFaskes" },
                    { 3L, " ", 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(1236), null, null, false, null, null, "Jenjang Pendidikan", 1L, " ", "JenjangPendidikan" },
                    { 2L, " ", 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(1233), null, null, false, null, null, "Bank", 1L, " ", "Bank" },
                    { 6L, " ", 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(1243), null, null, false, null, null, "Kategori ProdKes", 1L, " ", "KategoriProdKes" }
                });

            migrationBuilder.InsertData(
                table: "M_users",
                columns: new[] { "id", "biodata_id", "created_by", "created_on", "deleted_by", "deleted_on", "email", "is_delete", "is_locked", "last_login", "login_attempt", "modified_by", "modified_on", "password", "role_id" },
                values: new object[,]
                {
                    { 13L, 7L, 13L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7375), null, null, "tsaphira@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7373), 0, null, null, "akucantik", 4L },
                    { 8L, 4L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7354), null, null, "samsi_option@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7352), 0, null, null, "option1234", 4L },
                    { 4L, 2L, 3L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7265), null, null, "birril_option@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7264), 0, null, null, "option1234", 4L },
                    { 16L, 10L, 16L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7387), null, null, "rraihan@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7385), 0, null, null, "raihan", 3L },
                    { 15L, 9L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7383), null, null, "eiga@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7381), 0, null, null, "eiga", 3L },
                    { 14L, 8L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7379), null, null, "anisa_ayu@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7377), 0, null, null, "anisaayu", 3L },
                    { 6L, 3L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7274), null, null, "fariz_option@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7272), 0, null, null, "option1234", 3L },
                    { 11L, 6L, 11L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7366), null, null, "riyan_admin@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7365), 0, null, null, "admin1234", 1L },
                    { 17L, 1L, 17L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7391), null, null, "simatupangelektro@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7389), 0, null, null, "winner", 1L },
                    { 9L, 5L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7359), null, null, "bastian_admin@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7357), 0, null, null, "admin1234", 1L },
                    { 7L, 4L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7279), null, null, "samsi_admin@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7277), 0, null, null, "admin1234", 1L },
                    { 5L, 3L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7270), null, null, "fariz_admin@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7268), 0, null, null, "admin1234", 1L },
                    { 3L, 2L, 3L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7261), null, null, "birril_admin@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7260), 0, null, null, "admin1234", 1L },
                    { 1L, 1L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7148), null, null, "indra_admin@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(6612), 0, null, null, "admin1234", 1L },
                    { 10L, 5L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7363), null, null, "bastian_option@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7361), 0, null, null, "option1234", 5L },
                    { 2L, 1L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7256), null, null, "indra_option@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7247), 0, null, null, "option1234", 2L },
                    { 12L, 6L, 11L, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7371), null, null, "riyan_option@gmail.com", false, false, new DateTime(2022, 9, 21, 9, 57, 8, 150, DateTimeKind.Local).AddTicks(7369), 0, null, null, "option1234", 5L }
                });

            migrationBuilder.InsertData(
                table: "M_locations",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "is_delete", "location_level_id", "modified_by", "modified_on", "name", "parent_id" },
                values: new object[,]
                {
                    { 3L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(1275), null, null, false, 2L, null, null, "Jakarta Pusat", 1L },
                    { 4L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(1278), null, null, false, 2L, null, null, "Jakarta Selatan", 1L },
                    { 5L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(1280), null, null, false, 2L, null, null, "Jakarta Timur", 1L },
                    { 6L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(1282), null, null, false, 2L, null, null, "Depok", 2L },
                    { 7L, 9L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(1284), null, null, false, 2L, null, null, "Bekasi", 2L }
                });

            migrationBuilder.InsertData(
                table: "M_menu_roles",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "is_delete", "menu_id", "modified_by", "modified_on", "role_id" },
                values: new object[,]
                {
                    { 12L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(3189), null, null, false, 13L, null, null, 3L },
                    { 1L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(3048), null, null, false, 2L, null, null, 1L },
                    { 2L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(3117), null, null, false, 3L, null, null, 1L },
                    { 3L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(3120), null, null, false, 4L, null, null, 1L },
                    { 13L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(3191), null, null, false, 13L, null, null, 5L },
                    { 5L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(3124), null, null, false, 6L, null, null, 1L },
                    { 6L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(3126), null, null, false, 7L, null, null, 1L },
                    { 7L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(3128), null, null, false, 8L, null, null, 1L },
                    { 8L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(3130), null, null, false, 9L, null, null, 1L },
                    { 9L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(3182), null, null, false, 10L, null, null, 1L },
                    { 10L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(3185), null, null, false, 12L, null, null, 3L },
                    { 11L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(3187), null, null, false, 12L, null, null, 5L },
                    { 4L, 1L, new DateTime(2022, 9, 21, 9, 57, 8, 151, DateTimeKind.Local).AddTicks(3122), null, null, false, 5L, null, null, 1L }
                });

            migrationBuilder.InsertData(
                table: "T_customer_custom_nominals",
                columns: new[] { "id", "created_by", "created_on", "customer_id", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "nominal" },
                values: new object[,]
                {
                    { 4L, 6L, new DateTime(2022, 9, 21, 9, 57, 8, 155, DateTimeKind.Local).AddTicks(3796), 4L, null, null, false, null, null, 125000 },
                    { 3L, 6L, new DateTime(2022, 9, 21, 9, 57, 8, 155, DateTimeKind.Local).AddTicks(3794), 3L, null, null, false, null, null, 125000 },
                    { 1L, 6L, new DateTime(2022, 9, 21, 9, 57, 8, 155, DateTimeKind.Local).AddTicks(3738), 1L, null, null, false, null, null, 125000 },
                    { 5L, 6L, new DateTime(2022, 9, 21, 9, 57, 8, 155, DateTimeKind.Local).AddTicks(3798), 5L, null, null, false, null, null, 125000 },
                    { 2L, 6L, new DateTime(2022, 9, 21, 9, 57, 8, 155, DateTimeKind.Local).AddTicks(3790), 2L, null, null, false, null, null, 125000 },
                    { 6L, 6L, new DateTime(2022, 9, 21, 9, 57, 8, 155, DateTimeKind.Local).AddTicks(3801), 6L, null, null, false, null, null, 125000 }
                });

            migrationBuilder.InsertData(
                table: "T_customer_registered_cards",
                columns: new[] { "id", "card_number", "created_by", "created_on", "customer_id", "cvv", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "validity_period" },
                values: new object[,]
                {
                    { 1L, "00834221000000000991", 6L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(8014), 1L, "00991", null, null, false, null, null, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2L, "00834221000000000992", 6L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(8075), 2L, "00992", null, null, false, null, null, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 6L, "00834221000000000996", 6L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(8088), 6L, "00996", null, null, false, null, null, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3L, "00834221000000000993", 6L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(8078), 3L, "00993", null, null, false, null, null, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 4L, "00834221000000000994", 6L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(8082), 4L, "00994", null, null, false, null, null, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 5L, "00834221000000000995", 6L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(8085), 5L, "00995", null, null, false, null, null, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "T_customer_vas",
                columns: new[] { "id", "created_by", "created_on", "customer_id", "deleted_by", "deleted_on", "is_delete", "modified_by", "modified_on", "va_number" },
                values: new object[,]
                {
                    { 2L, 6L, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), 2L, null, null, false, null, null, "00834221000000000992" },
                    { 6L, 6L, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), 6L, null, null, false, null, null, "00834221000000000996" },
                    { 5L, 6L, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), 5L, null, null, false, null, null, "00834221000000000995" },
                    { 3L, 6L, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), 3L, null, null, false, null, null, "00834221000000000993" },
                    { 1L, 6L, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), 1L, null, null, false, null, null, "00834221000000000991" },
                    { 4L, 6L, new DateTime(2022, 5, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), 4L, null, null, false, null, null, "00834221000000000994" }
                });

            migrationBuilder.InsertData(
                table: "T_doctor_treatments",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "doctor_id", "is_delete", "modified_by", "modified_on", "name" },
                values: new object[,]
                {
                    { 2L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2670), null, null, 1L, false, null, null, "Vaksinasi Anak" },
                    { 3L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2673), null, null, 1L, false, null, null, "Konsultasi Alergi Anak" },
                    { 4L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2675), null, null, 1L, false, null, null, "Konsultasi Psikologi Anak" },
                    { 5L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2677), null, null, 1L, false, null, null, "Fisioterapi Anak" },
                    { 6L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2679), null, null, 1L, false, null, null, "Tes Pendengaran Anak" },
                    { 7L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2681), null, null, 2L, false, null, null, "Konsultasi Kesehatan Tulang" },
                    { 8L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2683), null, null, 2L, false, null, null, "Fisioterapi Tulang" },
                    { 9L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2685), null, null, 2L, false, null, null, "Arthoroskopi" },
                    { 10L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2687), null, null, 2L, false, null, null, "Fiksasi Internal" },
                    { 11L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2689), null, null, 2L, false, null, null, "Fusion Tulang" },
                    { 12L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2691), null, null, 2L, false, null, null, "Penggantian Sendi" },
                    { 13L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2693), null, null, 2L, false, null, null, "Osteotomi" },
                    { 15L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2697), null, null, 2L, false, null, null, "Operasi Tulang Belakang" },
                    { 16L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2699), null, null, 2L, false, null, null, "Peremajaan Tulang Rawan" },
                    { 17L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2701), null, null, 2L, false, null, null, "Amputasi" },
                    { 18L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2703), null, null, 3L, false, null, null, "Konsultasi Kesehatan Anak" },
                    { 19L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2705), null, null, 3L, false, null, null, "Vaksinasi Anak" },
                    { 20L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2707), null, null, 3L, false, null, null, "Konsultasi Alergi Anak" },
                    { 21L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2709), null, null, 3L, false, null, null, "Konsultasi Psikologi Anak" },
                    { 22L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2711), null, null, 3L, false, null, null, "Fisioterapi Anak" },
                    { 23L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2713), null, null, 3L, false, null, null, "Tes Pendengaran Anak" },
                    { 24L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2715), null, null, 3L, false, null, null, "Skrinning Tumbuh Kembang Anak" },
                    { 14L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2695), null, null, 2L, false, null, null, "Rekonstruksi Ligamen, Tulang, dan Otot" },
                    { 1L, 7L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(2626), null, null, 1L, false, null, null, "Konsultasi Kesehatan Anak" }
                });

            migrationBuilder.InsertData(
                table: "M_medical_facilities",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "email", "fax", "full_address", "is_delete", "location_id", "medical_facility_category_id", "modified_by", "modified_on", "name", "phone", "phone_code" },
                values: new object[,]
                {
                    { 2L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(4625), null, null, "rsiabunda@gmail.com", "", "Jl. Teuku Cik Ditiro No.28, RT.9/RW.2, Gondangdia, Kec. Menteng, Kota Jakarta Pusat, DKI Jakarta 10350, Indonesia", false, 3L, 2L, null, null, "RSIA Bunda Jakarta", "1 500 799", "+62" },
                    { 7L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(4640), null, null, "gooddoctors@gmail.com", "", "Jl. H. R. Rasuna Said, RT.2/RW.5, Karet Kuningan, Kec. Setiabudi, Kota Jakarta Selatan, DKI Jakarta 12940, Indonesia", false, 4L, 3L, null, null, "Good Doctors Medical Centre ", "21 29912131", "+62" },
                    { 8L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(4643), null, null, "", "", "Pasar Mayestik Lt. Semi B Blok AKS 087-088, Jl. Tebah III No.RT.14, RT.14/RW.3, Kec. Kby. Baru, Kota Jakarta Selatan, DKI Jakarta 12120, Indonesia", false, 4L, 1L, null, null, "Apotek Kemajuan Mayestik ", "21 29395120", "+62" },
                    { 9L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(4646), null, null, "", "", "Jl. Gandaria Tengah V No.2, RW.1, Kramat Pela, Kec. Kby. Baru, Kota Jakarta Selatan, DKI Jakarta 12130, Indonesia", false, 4L, 4L, null, null, "Puskesmas Kramat Pela", "21 7233832", "+62" },
                    { 10L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(4648), null, null, "", "", "Jl. Kebenaran Kav 3s", false, 4L, 2L, null, null, "RS Misu", "82192562323", "+62" },
                    { 11L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(4651), null, null, "", "", "Jl. RA. Kartini No. 8, Cilandak Jakarta Selatan", false, 4L, 2L, null, null, "Siloam Hospital TB Simatupang", "21 2953 1900", "+62" },
                    { 3L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(4629), null, null, "rskojakarta@yahoo.co.id", "87711969", "Jl. Lapangan Tembak No.75 Cibubur Jakarta Timur 13720", false, 5L, 2L, null, null, "RSKO Jakarta", "21 87711968", "+62" },
                    { 4L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(4632), null, null, "rs_hajijakarta@gmail.com", "", "l. Raya Pd. Gede No.4, RT.1/RW.1, Lubang Buaya, Kec. Makasar, Kota Jakarta Timur, DKI Jakarta 13650, Indonesia", false, 5L, 2L, null, null, "RS Haji Jakarta", "21 8000694", "+62" },
                    { 6L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(4637), null, null, "samclinic@gmail.com", "", " Bassura City Tower F Lt. GF No. CL-02, RT.10/RW.2, Cipinang Besar, Kec. Jatinegara, Kota Jakarta Timur, DKI Jakarta 13410, Indonesia", false, 5L, 3L, null, null, "SamMarie Clinic @ Bassura City", "813-3000-5010", "+62" },
                    { 1L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(4542), null, null, "mitra_rumahsakit@gmail.com", "", "Jl. Margonda Raya, Depok, Kec. Pancoran Mas, Kota Depok, Jawa Barat 16424, Indonesia", false, 6L, 2L, null, null, "RS Mitra Keluarga", "21 77210700", "+62" },
                    { 5L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(4635), null, null, "meiliahospital@gmail.com", "", "Jl. Alternatif Cibubur No.1, Harjamukti, Kec. Cimanggis, Kota Depok, Jawa Barat 16454, Indonesia", false, 6L, 2L, null, null, "RS Meilia Cibubur", "21 8444444", "+62" }
                });

            migrationBuilder.InsertData(
                table: "M_medical_facility_schedules",
                columns: new[] { "id", "created_by", "created_on", "day", "deleted_by", "deleted_on", "is_delete", "medical_facility_id", "modified_by", "modified_on", "time_schedule_end", "time_schedule_start" },
                values: new object[,]
                {
                    { 7L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(956), "Senin", null, null, false, 2L, null, null, "20:30", "08:30" },
                    { 47L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1099), "Rabu", null, null, false, 11L, null, null, "20:00", "07:00" },
                    { 48L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1101), "Kamis", null, null, false, 11L, null, null, "20:00", "07:00" },
                    { 49L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1104), "Jumat", null, null, false, 11L, null, null, "18:00", "07:00" },
                    { 50L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1106), "Sabtu", null, null, false, 11L, null, null, "18:00", "07:00" },
                    { 12L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(968), "Rabu", null, null, false, 3L, null, null, "21:00", "09:00" },
                    { 13L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(970), "Sabtu", null, null, false, 3L, null, null, "15:00", "09:00" },
                    { 14L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(972), "Senin", null, null, false, 4L, null, null, "21:00", "07:00" },
                    { 15L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(975), "Kamis", null, null, false, 4L, null, null, "21:00", "07:00" },
                    { 16L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(977), "Jumat", null, null, false, 4L, null, null, "21:00", "07:00" },
                    { 17L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(979), "Sabtu", null, null, false, 4L, null, null, "16:00", "07:00" },
                    { 20L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(986), "Rabu", null, null, false, 5L, null, null, "21:00", "08:00" },
                    { 18L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(981), "Minggu", null, null, false, 4L, null, null, "16:00", "07:00" },
                    { 23L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(992), "Selasa", null, null, false, 6L, null, null, "21:00", "08:00" },
                    { 24L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(995), "Kamis", null, null, false, 6L, null, null, "21:00", "08:00" },
                    { 25L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(997), "Jumat", null, null, false, 6L, null, null, "21:00", "08:00" },
                    { 1L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(884), "Senin", null, null, false, 1L, null, null, "20:00", "08:00" },
                    { 2L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(943), "Selasa", null, null, false, 1L, null, null, "20:00", "08:00" },
                    { 3L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(946), "Rabu", null, null, false, 1L, null, null, "20:00", "08:00" },
                    { 4L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(949), "Kamis", null, null, false, 1L, null, null, "20:00", "08:00" },
                    { 5L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(951), "Jumat", null, null, false, 1L, null, null, "20:00", "08:00" },
                    { 6L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(953), "Minggu", null, null, false, 1L, null, null, "15:00", "08:00" },
                    { 19L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(984), "Senin", null, null, false, 5L, null, null, "21:00", "08:00" },
                    { 22L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(990), "Senin", null, null, false, 6L, null, null, "21:00", "08:00" },
                    { 45L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1095), "Senin", null, null, false, 11L, null, null, "20:00", "07:00" },
                    { 46L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1097), "Selasa", null, null, false, 11L, null, null, "20:00", "07:00" },
                    { 31L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1010), "Senin", null, null, false, 8L, null, null, "20:00", "10:00" },
                    { 8L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(958), "Rabu", null, null, false, 2L, null, null, "20:30", "08:30" },
                    { 9L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(960), "Kamis", null, null, false, 2L, null, null, "20:30", "08:30" },
                    { 10L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(962), "Jumat", null, null, false, 2L, null, null, "20:30", "08:30" },
                    { 11L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(964), "Sabtu", null, null, false, 2L, null, null, "18:30", "08:30" },
                    { 26L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(999), "Selasa", null, null, false, 7L, null, null, "21:00", "08:00" },
                    { 27L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1001), "Rabu", null, null, false, 7L, null, null, "21:00", "08:00" },
                    { 28L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1003), "Jumat", null, null, false, 7L, null, null, "21:00", "08:00" },
                    { 29L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1006), "Sabtu", null, null, false, 7L, null, null, "18:00", "08:00" },
                    { 30L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1008), "Minggu", null, null, false, 7L, null, null, "18:00", "08:00" },
                    { 44L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1092), "Minggu", null, null, false, 10L, null, null, "15:00", "08:00" },
                    { 32L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1065), "Selasa", null, null, false, 8L, null, null, "20:00", "10:00" },
                    { 33L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1068), "Rabu", null, null, false, 8L, null, null, "20:00", "10:00" },
                    { 21L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(988), "Jumat", null, null, false, 5L, null, null, "14:00", "08:00" },
                    { 35L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1072), "Jumat", null, null, false, 8L, null, null, "18:00", "10:00" },
                    { 36L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1074), "Senin", null, null, false, 9L, null, null, "16:00", "07:00" },
                    { 37L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1077), "Rabu", null, null, false, 9L, null, null, "16:00", "07:00" },
                    { 38L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1079), "Jumat", null, null, false, 9L, null, null, "11:00", "07:00" },
                    { 39L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1081), "Sabtu", null, null, false, 9L, null, null, "12:00", "07:00" },
                    { 40L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1083), "Selasa", null, null, false, 10L, null, null, "18:00", "08:00" },
                    { 34L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1070), "Kamis", null, null, false, 8L, null, null, "20:00", "10:00" },
                    { 41L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1086), "Kamis", null, null, false, 10L, null, null, "18:00", "08:00" },
                    { 42L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1088), "Jumat", null, null, false, 10L, null, null, "18:00", "08:00" },
                    { 43L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(1090), "Sabtu", null, null, false, 10L, null, null, "18:00", "08:00" }
                });

            migrationBuilder.InsertData(
                table: "T_doctor_office_schedules",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "doctor_id", "is_delete", "medical_facility_id", "modified_by", "modified_on", "slot", "specialization" },
                values: new object[,]
                {
                    { 3L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(8918), null, null, 2L, false, 11L, null, null, 2, "Dokter Tulang" },
                    { 2L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(8915), null, null, 2L, false, 10L, null, null, 3, "Dokter Tulang" },
                    { 5L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(8923), null, null, 3L, false, 2L, null, null, 2, "Dokter Anak" },
                    { 4L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(8920), null, null, 3L, false, 1L, null, null, 3, "Dokter Anak" },
                    { 1L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(8858), null, null, 1L, false, 1L, null, null, 4, "Dokter Anak" }
                });

            migrationBuilder.InsertData(
                table: "T_doctor_offices",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "doctor_id", "is_delete", "medical_facility_id", "modified_by", "modified_on", "specialization" },
                values: new object[,]
                {
                    { 5L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(6821), null, null, 3L, false, 2L, null, null, "Dokter Anak" },
                    { 1L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(6764), null, null, 1L, false, 1L, null, null, "Dokter Anak" },
                    { 3L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(6817), null, null, 2L, false, 11L, null, null, "Dokter Tulang" },
                    { 4L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(6819), null, null, 3L, false, 1L, null, null, "Dokter Anak" },
                    { 2L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 153, DateTimeKind.Local).AddTicks(6814), null, null, 2L, false, 10L, null, null, "Dokter Tulang" }
                });

            migrationBuilder.InsertData(
                table: "T_doctor_office_treatments",
                columns: new[] { "id", "created_by", "created_on", "deleted_by", "deleted_on", "doctor_office_id", "doctor_treatment_id", "is_delete", "modified_by", "modified_on" },
                values: new object[,]
                {
                    { 36L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4625), null, null, 5L, 18L, false, null, null },
                    { 23L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4595), null, null, 3L, 12L, false, null, null },
                    { 24L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4599), null, null, 3L, 13L, false, null, null },
                    { 25L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4602), null, null, 3L, 14L, false, null, null },
                    { 26L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4604), null, null, 3L, 15L, false, null, null },
                    { 27L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4606), null, null, 3L, 16L, false, null, null },
                    { 28L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4608), null, null, 3L, 17L, false, null, null },
                    { 1L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4503), null, null, 1L, 1L, false, null, null },
                    { 2L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4549), null, null, 1L, 2L, false, null, null },
                    { 3L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4552), null, null, 1L, 3L, false, null, null },
                    { 4L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4554), null, null, 1L, 4L, false, null, null },
                    { 5L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4556), null, null, 1L, 5L, false, null, null },
                    { 6L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4558), null, null, 1L, 6L, false, null, null },
                    { 29L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4610), null, null, 4L, 18L, false, null, null },
                    { 30L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4612), null, null, 4L, 19L, false, null, null },
                    { 31L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4614), null, null, 4L, 20L, false, null, null },
                    { 32L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4616), null, null, 4L, 21L, false, null, null },
                    { 33L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4618), null, null, 4L, 22L, false, null, null },
                    { 22L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4592), null, null, 3L, 11L, false, null, null },
                    { 21L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4590), null, null, 3L, 10L, false, null, null },
                    { 20L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4587), null, null, 3L, 9L, false, null, null },
                    { 19L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4585), null, null, 3L, 8L, false, null, null },
                    { 37L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4627), null, null, 5L, 19L, false, null, null },
                    { 38L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4629), null, null, 5L, 20L, false, null, null },
                    { 39L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4631), null, null, 5L, 21L, false, null, null },
                    { 40L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4633), null, null, 5L, 22L, false, null, null },
                    { 41L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4635), null, null, 5L, 23L, false, null, null },
                    { 42L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4637), null, null, 5L, 24L, false, null, null },
                    { 7L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4560), null, null, 2L, 7L, false, null, null },
                    { 8L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4562), null, null, 2L, 8L, false, null, null },
                    { 34L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4621), null, null, 4L, 23L, false, null, null },
                    { 9L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4565), null, null, 2L, 9L, false, null, null },
                    { 11L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4569), null, null, 2L, 11L, false, null, null },
                    { 12L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4571), null, null, 2L, 12L, false, null, null },
                    { 13L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4573), null, null, 2L, 13L, false, null, null },
                    { 14L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4575), null, null, 2L, 14L, false, null, null },
                    { 15L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4577), null, null, 2L, 15L, false, null, null },
                    { 16L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4579), null, null, 2L, 16L, false, null, null },
                    { 17L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4581), null, null, 2L, 17L, false, null, null },
                    { 18L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4583), null, null, 3L, 7L, false, null, null },
                    { 10L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4567), null, null, 2L, 10L, false, null, null },
                    { 35L, 5L, new DateTime(2022, 9, 21, 9, 57, 8, 154, DateTimeKind.Local).AddTicks(4623), null, null, 4L, 24L, false, null, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_M_admins_biodata_id",
                table: "M_admins",
                column: "biodata_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_biodata_address_biodata_id",
                table: "M_biodata_address",
                column: "biodata_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_biodata_address_location_id",
                table: "M_biodata_address",
                column: "location_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_biodata_attachments_biodata_id",
                table: "M_biodata_attachments",
                column: "biodata_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_biodata_attachments_role_id",
                table: "M_biodata_attachments",
                column: "role_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_courier_types_courier_id",
                table: "M_courier_types",
                column: "courier_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_customer_members_customer_id",
                table: "M_customer_members",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_customer_members_customer_relation_id",
                table: "M_customer_members",
                column: "customer_relation_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_customers_biodata_id",
                table: "M_customers",
                column: "biodata_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_customers_blood_group_id",
                table: "M_customers",
                column: "blood_group_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_doctor_educations_doctor_id",
                table: "M_doctor_educations",
                column: "doctor_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_doctor_educations_education_level_id",
                table: "M_doctor_educations",
                column: "education_level_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_doctors_biodata_id",
                table: "M_doctors",
                column: "biodata_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_locations_location_level_id",
                table: "M_locations",
                column: "location_level_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_locations_parent_id",
                table: "M_locations",
                column: "parent_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_medical_facilities_location_id",
                table: "M_medical_facilities",
                column: "location_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_medical_facilities_medical_facility_category_id",
                table: "M_medical_facilities",
                column: "medical_facility_category_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_medical_facility_schedules_medical_facility_id",
                table: "M_medical_facility_schedules",
                column: "medical_facility_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_medical_items_medical_item_category_id",
                table: "M_medical_items",
                column: "medical_item_category_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_medical_items_medical_item_segmentation_id",
                table: "M_medical_items",
                column: "medical_item_segmentation_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_menu_roles_menu_id",
                table: "M_menu_roles",
                column: "menu_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_menu_roles_role_id",
                table: "M_menu_roles",
                column: "role_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_menus_parent_id",
                table: "M_menus",
                column: "parent_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_users_biodata_id",
                table: "M_users",
                column: "biodata_id");

            migrationBuilder.CreateIndex(
                name: "IX_M_users_email",
                table: "M_users",
                column: "email",
                unique: true,
                filter: "[email] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_M_users_role_id",
                table: "M_users",
                column: "role_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_appointment_cancellations_appointment_id",
                table: "T_appointment_cancellations",
                column: "appointment_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_appointment_dones_appointment_id",
                table: "T_appointment_dones",
                column: "appointment_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_appointment_reschedule_histories_appointment_id",
                table: "T_appointment_reschedule_histories",
                column: "appointment_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_appointment_reschedule_histories_doctor_office_schedule_id",
                table: "T_appointment_reschedule_histories",
                column: "doctor_office_schedule_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_appointment_reschedule_histories_doctor_office_treatment_id",
                table: "T_appointment_reschedule_histories",
                column: "doctor_office_treatment_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_appointments_customer_id",
                table: "T_appointments",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_appointments_doctor_office_id",
                table: "T_appointments",
                column: "doctor_office_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_appointments_doctor_office_schedule_id",
                table: "T_appointments",
                column: "doctor_office_schedule_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_appointments_doctor_office_treatment_id",
                table: "T_appointments",
                column: "doctor_office_treatment_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_courier_discounts_courier_type_id",
                table: "T_courier_discounts",
                column: "courier_type_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_current_doctor_specializations_doctor_id",
                table: "T_current_doctor_specializations",
                column: "doctor_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_current_doctor_specializations_specialization_id",
                table: "T_current_doctor_specializations",
                column: "specialization_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_customer_chat_histories_customer_chat_id",
                table: "T_customer_chat_histories",
                column: "customer_chat_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_customer_chats_customer_id",
                table: "T_customer_chats",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_customer_chats_doctor_id",
                table: "T_customer_chats",
                column: "doctor_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_customer_custom_nominals_customer_id",
                table: "T_customer_custom_nominals",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_customer_registered_cards_customer_id",
                table: "T_customer_registered_cards",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_customer_va_histories_customer_va_id",
                table: "T_customer_va_histories",
                column: "customer_va_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_customer_vas_customer_id",
                table: "T_customer_vas",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_customer_wallet_top_ups_customer_wallet_id",
                table: "T_customer_wallet_top_ups",
                column: "customer_wallet_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_customer_wallet_withdraws_customer_id",
                table: "T_customer_wallet_withdraws",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_customer_wallets_customer_va_id",
                table: "T_customer_wallets",
                column: "customer_va_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_doctor_office_schedules_doctor_id",
                table: "T_doctor_office_schedules",
                column: "doctor_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_doctor_office_schedules_medical_facility_id",
                table: "T_doctor_office_schedules",
                column: "medical_facility_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_doctor_office_treatment_prices_doctor_treatment_id",
                table: "T_doctor_office_treatment_prices",
                column: "doctor_treatment_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_doctor_office_treatments_doctor_office_id",
                table: "T_doctor_office_treatments",
                column: "doctor_office_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_doctor_office_treatments_doctor_treatment_id",
                table: "T_doctor_office_treatments",
                column: "doctor_treatment_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_doctor_offices_doctor_id",
                table: "T_doctor_offices",
                column: "doctor_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_doctor_offices_medical_facility_id",
                table: "T_doctor_offices",
                column: "medical_facility_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_doctor_treatments_doctor_id",
                table: "T_doctor_treatments",
                column: "doctor_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_medical_item_purchase_details_courier_id",
                table: "T_medical_item_purchase_details",
                column: "courier_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_medical_item_purchase_details_medical_facility_id",
                table: "T_medical_item_purchase_details",
                column: "medical_facility_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_medical_item_purchase_details_medical_item_id",
                table: "T_medical_item_purchase_details",
                column: "medical_item_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_medical_item_purchase_details_medical_item_purchase_id",
                table: "T_medical_item_purchase_details",
                column: "medical_item_purchase_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_medical_item_purchases_customer_id",
                table: "T_medical_item_purchases",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_medical_item_purchases_payment_method_id",
                table: "T_medical_item_purchases",
                column: "payment_method_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_tokens_user_id",
                table: "T_tokens",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_T_treatment_discounts_doctor_office_treatment_price_id",
                table: "T_treatment_discounts",
                column: "doctor_office_treatment_price_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "M_admins");

            migrationBuilder.DropTable(
                name: "M_banks");

            migrationBuilder.DropTable(
                name: "M_biodata_address");

            migrationBuilder.DropTable(
                name: "M_biodata_attachments");

            migrationBuilder.DropTable(
                name: "M_customer_members");

            migrationBuilder.DropTable(
                name: "M_doctor_educations");

            migrationBuilder.DropTable(
                name: "M_medical_facility_schedules");

            migrationBuilder.DropTable(
                name: "M_menu_roles");

            migrationBuilder.DropTable(
                name: "M_wallet_default_nominals");

            migrationBuilder.DropTable(
                name: "T_appointment_cancellations");

            migrationBuilder.DropTable(
                name: "T_appointment_dones");

            migrationBuilder.DropTable(
                name: "T_appointment_reschedule_histories");

            migrationBuilder.DropTable(
                name: "T_courier_discounts");

            migrationBuilder.DropTable(
                name: "T_current_doctor_specializations");

            migrationBuilder.DropTable(
                name: "T_customer_chat_histories");

            migrationBuilder.DropTable(
                name: "T_customer_custom_nominals");

            migrationBuilder.DropTable(
                name: "T_customer_registered_cards");

            migrationBuilder.DropTable(
                name: "T_customer_va_histories");

            migrationBuilder.DropTable(
                name: "T_customer_wallet_top_ups");

            migrationBuilder.DropTable(
                name: "T_customer_wallet_withdraws");

            migrationBuilder.DropTable(
                name: "T_medical_item_purchase_details");

            migrationBuilder.DropTable(
                name: "T_reset_passwords");

            migrationBuilder.DropTable(
                name: "T_tokens");

            migrationBuilder.DropTable(
                name: "T_treatment_discounts");

            migrationBuilder.DropTable(
                name: "M_customer_relations");

            migrationBuilder.DropTable(
                name: "M_education_levels");

            migrationBuilder.DropTable(
                name: "M_menus");

            migrationBuilder.DropTable(
                name: "T_appointments");

            migrationBuilder.DropTable(
                name: "M_courier_types");

            migrationBuilder.DropTable(
                name: "M_specializations");

            migrationBuilder.DropTable(
                name: "T_customer_chats");

            migrationBuilder.DropTable(
                name: "T_customer_wallets");

            migrationBuilder.DropTable(
                name: "M_medical_items");

            migrationBuilder.DropTable(
                name: "T_medical_item_purchases");

            migrationBuilder.DropTable(
                name: "M_users");

            migrationBuilder.DropTable(
                name: "T_doctor_office_treatment_prices");

            migrationBuilder.DropTable(
                name: "T_doctor_office_schedules");

            migrationBuilder.DropTable(
                name: "T_doctor_office_treatments");

            migrationBuilder.DropTable(
                name: "M_couriers");

            migrationBuilder.DropTable(
                name: "T_customer_vas");

            migrationBuilder.DropTable(
                name: "M_medical_item_categories");

            migrationBuilder.DropTable(
                name: "M_medical_item_segmentations");

            migrationBuilder.DropTable(
                name: "M_payment_methods");

            migrationBuilder.DropTable(
                name: "M_roles");

            migrationBuilder.DropTable(
                name: "T_doctor_offices");

            migrationBuilder.DropTable(
                name: "T_doctor_treatments");

            migrationBuilder.DropTable(
                name: "M_customers");

            migrationBuilder.DropTable(
                name: "M_medical_facilities");

            migrationBuilder.DropTable(
                name: "M_doctors");

            migrationBuilder.DropTable(
                name: "M_blood_groups");

            migrationBuilder.DropTable(
                name: "M_locations");

            migrationBuilder.DropTable(
                name: "M_medical_facility_categories");

            migrationBuilder.DropTable(
                name: "M_biodatas");

            migrationBuilder.DropTable(
                name: "M_location_levels");
        }
    }
}
