﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class M_menu_role : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        public long? menu_id { get; set; }

        public long? role_id { get; set; }

        [ForeignKey("menu_id")]
        public virtual M_menu menu { get; set; }

        [ForeignKey("role_id")]
        public virtual M_role role { get; set; }
    }
}
