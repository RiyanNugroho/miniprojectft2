﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class T_courier_discount : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        public long? courier_type_id { get; set; }
        
        [MaxLength(20)]
        public decimal value { get; set; }

        [ForeignKey("courier_type_id")]
        public virtual M_courier_type M_courier_type { get; set; }
    }
}
