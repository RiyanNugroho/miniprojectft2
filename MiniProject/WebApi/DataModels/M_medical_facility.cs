﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class M_medical_facility : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        [MaxLength(50)]
        public string name { get; set; }

        public long? medical_facility_category_id { get; set; }
        
        public long? location_id { get; set; }
        
        public string full_address { get; set; }
        
        [MaxLength(100)]
        public string email { get; set; }
        
        [MaxLength(10)]
        public string phone_code { get; set; }
        
        [MaxLength(15)]
        public string phone { get; set; }
        
        [MaxLength(15)]
        public string fax { get; set; }
        
        [ForeignKey("medical_facility_category_id")]
        public virtual M_medical_facility_category M_medical_facility_category { get; set; }
        
        [ForeignKey("location_id")]
        public virtual M_location M_location { get; set; }
    }
}
