﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class T_doctor_office_treatment_price : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        public long? doctor_treatment_id { get; set; }
        
        public decimal price { get; set; }
        
        public decimal price_start_from { get; set; }
       
        public decimal price_until_from { get; set; }

        [ForeignKey("doctor_treatment_id")]
        public virtual T_doctor_treatment T_doctor_treatment { get; set; }
    }
}
