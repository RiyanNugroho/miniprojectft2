﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class T_doctor_office_schedule : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        public long? doctor_id { get; set; }

        public long? medical_facility_id { get; set; }
        
        [MaxLength(100)]
        public string specialization { get; set; }

        public int slot { get; set; }

        [ForeignKey("doctor_id")]
        public virtual M_doctor M_doctor { get; set; }
        
        [ForeignKey("medical_facility_id")]
        public virtual M_medical_facility M_medical_facility { get; set; }
    }
}
