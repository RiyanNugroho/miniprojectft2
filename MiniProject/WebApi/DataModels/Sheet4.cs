﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class T_appointment_done : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        
        public long id { get; set; }
        public long? appointment_id { get; set; }

        [ForeignKey("appointment_id")]
        public virtual T_appointment T_appointment { get; set; }
    }

    public class T_appointment_reschedule_history : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        public long? appointment_id { get; set; }
        
        public long? doctor_office_schedule_id { get; set; }
        
        public long? doctor_office_treatment_id { get; set; }
        
        public DateTime appointment_date { get; set; }

        [ForeignKey("appointment_id")]
        public virtual T_appointment appointment { get; set; }

        [ForeignKey("doctor_office_schedule_id")]
        public virtual T_doctor_office_schedule doctor_office_schedule { get; set; }

        [ForeignKey("doctor_office_treatment_id")]
        public virtual T_doctor_office_treatment doctor_office_treatment { get; set; }
    }
    

    public class T_customer_chat : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        public long? customer_id { get; set; }
        
        public long? doctor_id { get; set; }

        [ForeignKey("customer_id")]
        public virtual M_customer M_customer { get; set; }

        [ForeignKey("doctor_id")]
        public virtual M_doctor M_doctor { get; set; }
    }

    public class T_customer_chat_history : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        public long? customer_chat_id { get; set; }
        
        public string chat_content { get; set; }

        [ForeignKey("customer_chat_id")]
        public virtual T_customer_chat customer_chat { get; set; }
    }

    public class T_customer_custom_nominal : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        public long? customer_id { get; set; }
        
        public int nominal { get; set; }

        [ForeignKey("customer_id")]
        public virtual M_customer M_customer { get; set; }
    }

    public class T_customer_registered_card : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        public long? customer_id { get; set; }
        
        [MaxLength(20)]
        public string card_number { get; set; }
        
        public DateTime validity_period { get; set; }
        
        [MaxLength(5)]
        public string cvv { get; set; }

        [ForeignKey("customer_id")]
        public virtual M_customer M_customer { get; set; }
    }

    public class T_customer_va : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        public long? customer_id { get; set; }
        
        [MaxLength(20)]
        public string va_number { get; set; }

        [ForeignKey("customer_id")]
        public virtual M_customer M_customer { get; set; }
    }

    public class T_customer_va_history : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        public long? customer_va_id { get; set; }
        
        public decimal amount { get; set; }
        
        public DateTime expired_on { get; set; }

        [ForeignKey("customer_va_id")]
        public virtual T_customer_va T_customer_va { get; set; }
    }

    public class T_customer_wallet : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        public long? customer_id { get; set; }
        
        [MaxLength(6)]
        public string pin { get; set; }
        
        public decimal balance { get; set; }
        
        [MaxLength(50)]
        public string barcode { get; set; }
        
        public decimal points { get; set; }

        [ForeignKey("customer_va_id")]
        public virtual T_customer_va T_customer_va { get; set; }
    }
}
