﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class M_medical_facility_schedule : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        public long? medical_facility_id { get; set; }

        [MaxLength(10)]
        public string day { get; set; }
        
        [MaxLength(10)]
        public string time_schedule_start { get; set; }
        
        [MaxLength(10)]
        public string time_schedule_end { get; set; }
        
        [ForeignKey("medical_facility_id")]
        public virtual M_medical_facility M_medical_facility { get; set; }
    }
}
