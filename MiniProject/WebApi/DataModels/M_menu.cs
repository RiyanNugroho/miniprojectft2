﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class M_menu : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        [MaxLength(20)]
        public string name { get; set; }

        [MaxLength(50)]
        public string url { get; set; }

        public long? parent_id { get; set; }

        [MaxLength(100)]
        public string big_icon { get; set; }

        [MaxLength(100)]
        public string small_icon { get; set; }

        [ForeignKey ("parent_id")]
        public virtual M_menu m_Menu { get; set; }
    }
}
