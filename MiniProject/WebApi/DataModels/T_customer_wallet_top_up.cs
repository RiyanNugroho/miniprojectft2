﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class T_customer_wallet_top_up : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        public long? customer_wallet_id { get; set; }

        public decimal amount { get; set; }

        [ForeignKey("customer_wallet_id")]
        public virtual T_customer_wallet T_customer_wallet { get; set; }
    }
}
