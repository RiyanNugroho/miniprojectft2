﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class M_biodata_address : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        public long? biodata_id { get; set; }

        [MaxLength(100)]
        public string label { get; set; }

        [MaxLength(100)]
        public string recipient { get; set; }

        [MaxLength(15)]
        public string recipient_phone_number { get; set; }

        public long? location_id { get; set; }

        [MaxLength(10)]
        public string postal_code { get; set; }

        public string address { get; set; }

        [ForeignKey("biodata_id")]
        public virtual M_biodata M_biodata { get; set; }

        [ForeignKey("location_id")]
        public virtual M_location M_location { get; set; }
    }
}
