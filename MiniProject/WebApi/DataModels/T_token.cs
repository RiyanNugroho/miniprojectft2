﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class T_token : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        [MaxLength(100)]
        public string? email { get; set; }
        
        public long? user_id { get; set; }
        
        [MaxLength(50)]
        public string? token { get; set; }
        
        public DateTime? expired_on { get; set; }
        
        public bool? is_expired { get; set; }
        
        [MaxLength(20)]
        public string? used_for { get; set; }

        [ForeignKey("user_id")]
        public virtual M_user M_user { get; set; }
    }
}
