﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class M_payment_method : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        [MaxLength(50)]
        public string name { get; set; }
    }
    

    public class M_biodata_attachment : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        public long? biodata_id { get; set; }

        [MaxLength(50)]
        public string file_name { get; set; }

        [MaxLength(100)]
        public string file_path { get; set; }

        public int file_size { get; set; }

        [DataType("varbinary(max)")]
        public byte[] file { get; set; }

        [ForeignKey("biodata_id")]
        public virtual M_biodata biodata { get; set; }

        [ForeignKey("role_id")]
        public virtual M_role role { get; set; }
    }

    public class M_wallet_default_nominal : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        public int nominal { get; set; }
    }

    public class T_appointment : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        public long? customer_id { get; set; }

        public long? doctor_office_id { get; set; }

        public long? doctor_office_schedule_id { get; set; }

        public long? doctor_office_treatment_id { get; set; }

        public DateTime? appointment_date { get; set; }

        [ForeignKey("customer_id")]
        public virtual M_customer M_customer { get; set; }

        [ForeignKey("doctor_office_id")]
        public virtual T_doctor_office T_doctor_office { get; set; }

        [ForeignKey("doctor_office_schedule_id")]
        public virtual T_doctor_office_schedule T_doctor_office_schedule { get; set; }

        [ForeignKey("doctor_office_treatment_id")]
        public virtual T_doctor_office_treatment T_doctor_office_treatment { get; set; }
    }

    public class T_appointment_cancellation : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        public long? appointment_id { get; set; }

        [ForeignKey("appointment_id")]
        public virtual T_appointment T_appointment { get; set; }
    }
}
