﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class M_customer : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        public long? biodata_id { get; set; }

        public DateTime? dob { get; set; }

        [MaxLength(1)]
        public string gender { get; set; }

        public long? blood_group_id { get; set; }

        [MaxLength(5)]
        public string rhesus_type { get; set; }

        public decimal? height { get; set; }

        public decimal? weight { get; set; }

        [ForeignKey("biodata_id")]
        public virtual M_biodata M_biodata { get; set; }

        [ForeignKey("blood_group_id")]
        public virtual M_blood_group M_blood_group { get; set; }
    }
}
