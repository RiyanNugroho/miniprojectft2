﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public static class ModelBuilderExtentions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<M_biodata>()
                .HasData(
                    new M_biodata() { id = 1, fullname = "Indra Oloan Simatupang", mobile_phone = "087878901234", image = null, image_path = "", created_by = 1, created_on = DateTime.Now },
                    new M_biodata() { id = 2, fullname = "Muhammad Birril Jasur", mobile_phone = "087878905678", image = null, image_path = "", created_by = 3, created_on = DateTime.Now },
                    new M_biodata() { id = 3, fullname = "Muhammad Fariz Jusuf", mobile_phone = "087878909101", image = null, image_path = "", created_by = 5, created_on = DateTime.Now },
                    new M_biodata() { id = 4, fullname = "Samsi Satria Rama", mobile_phone = "087878902468", image = null, image_path = "", created_by = 7, created_on = DateTime.Now },
                    new M_biodata() { id = 5, fullname = "Sebastian Fajar Juanito", mobile_phone = "087878901357", image = null, image_path = "", created_by = 9, created_on = DateTime.Now },
                    new M_biodata() { id = 6, fullname = "Riyan Eko Nugroho", mobile_phone = "087878909876", image = null, image_path = "", created_by = 11, created_on = DateTime.Now },
                    new M_biodata() { id = 7, fullname = "Tatjana Saphira, Sp. A", mobile_phone = "087878909876", image = null, image_path = "", created_by = 13, created_on = DateTime.Now },
                    new M_biodata() { id = 8, fullname = "Anisa Ayu", mobile_phone = "085683049583", image = null, image_path = "", created_by = 5, created_on = DateTime.Now },
                    new M_biodata() { id = 9, fullname = "Eiga Putri", mobile_phone = "081603994824", image = null, image_path = "", created_by = 5, created_on = DateTime.Now },
                    new M_biodata() { id = 10, fullname = "Raden Raihan", mobile_phone = "089899526623", image = null, image_path = "", created_by = 10, created_on = DateTime.Now }
                );

            modelBuilder.Entity<M_role>()
                .HasData(
                    new M_role() { id = 1, name = "Administrator", code = "Admin", created_by = 9, created_on = DateTime.Now },
                    new M_role() { id = 2, name = "Fasilitas Kesehatan", code = "Faskes", created_by = 9, created_on = DateTime.Now },
                    new M_role() { id = 3, name = "Pasien", code = "Pasien", created_by = 9, created_on = DateTime.Now },
                    new M_role() { id = 4, name = "Dokter", code = "Dokter", created_by = 9, created_on = DateTime.Now },
                    new M_role() { id = 5, name = "Umum", code = "Umum", created_by = 9, created_on = DateTime.Now }
                );

            modelBuilder.Entity<M_user>()
                .HasData(
                    new M_user() { id = 1, biodata_id = 1, role_id = 1, email = "indra_admin@gmail.com", password = "admin1234", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 1, created_on = DateTime.Now },
                    new M_user() { id = 2, biodata_id = 1, role_id = 2, email = "indra_option@gmail.com", password = "option1234", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 1, created_on = DateTime.Now },
                    new M_user() { id = 3, biodata_id = 2, role_id = 1, email = "birril_admin@gmail.com", password = "admin1234", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 3, created_on = DateTime.Now },
                    new M_user() { id = 4, biodata_id = 2, role_id = 4, email = "birril_option@gmail.com", password = "option1234", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 3, created_on = DateTime.Now },
                    new M_user() { id = 5, biodata_id = 3, role_id = 1, email = "fariz_admin@gmail.com", password = "admin1234", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 5, created_on = DateTime.Now },
                    new M_user() { id = 6, biodata_id = 3, role_id = 3, email = "fariz_option@gmail.com", password = "option1234", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 5, created_on = DateTime.Now },
                    new M_user() { id = 7, biodata_id = 4, role_id = 1, email = "samsi_admin@gmail.com", password = "admin1234", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 7, created_on = DateTime.Now },
                    new M_user() { id = 8, biodata_id = 4, role_id = 4, email = "samsi_option@gmail.com", password = "option1234", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 7, created_on = DateTime.Now },
                    new M_user() { id = 9, biodata_id = 5, role_id = 1, email = "bastian_admin@gmail.com", password = "admin1234", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 9, created_on = DateTime.Now },
                    new M_user() { id = 10, biodata_id = 5, role_id = 5, email = "bastian_option@gmail.com", password = "option1234", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 9, created_on = DateTime.Now },
                    new M_user() { id = 11, biodata_id = 6, role_id = 1, email = "riyan_admin@gmail.com", password = "admin1234", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 11, created_on = DateTime.Now },
                    new M_user() { id = 12, biodata_id = 6, role_id = 5, email = "riyan_option@gmail.com", password = "option1234", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 11, created_on = DateTime.Now },
                    new M_user() { id = 13, biodata_id = 7, role_id = 4, email = "tsaphira@gmail.com", password = "akucantik", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 13, created_on = DateTime.Now },
                    new M_user() { id = 14, biodata_id = 8, role_id = 3, email = "anisa_ayu@gmail.com", password = "anisaayu", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 5, created_on = DateTime.Now },
                    new M_user() { id = 15, biodata_id = 9, role_id = 3, email = "eiga@gmail.com", password = "eiga", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 5, created_on = DateTime.Now },
                    new M_user() { id = 16, biodata_id = 10, role_id = 3, email = "rraihan@gmail.com", password = "raihan", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 16, created_on = DateTime.Now },
                    new M_user() { id = 17, biodata_id = 1, role_id = 1, email = "simatupangelektro@gmail.com", password = "winner", login_attempt = 0, is_locked = false, last_login = DateTime.Now, created_by = 17, created_on = DateTime.Now }
                );

            modelBuilder.Entity<M_location_level>()
                .HasData(
                    new M_location_level() { id = 1, name = "Provinsi", abbreviation = "Prov.", created_by = 9, created_on = DateTime.Now },
                    new M_location_level() { id = 2, name = "Kota", abbreviation = "Kot.", created_by = 9, created_on = DateTime.Now },
                    new M_location_level() { id = 3, name = "Kabupaten", abbreviation = "Kab.", created_by = 9, created_on = DateTime.Now },
                    new M_location_level() { id = 4, name = "Kelurahan", abbreviation = "Kel.", created_by = 9, created_on = DateTime.Now },
                    new M_location_level() { id = 5, name = "Kecamatan", abbreviation = "Kec.", created_by = 9, created_on = DateTime.Now }
                );

            modelBuilder.Entity<M_menu>()
                .HasData(
                    new M_menu() { id = 1, name = "Master", url = "", parent_id = null, big_icon = " ", small_icon = " ", created_by = 1, created_on = DateTime.Now },
                    new M_menu() { id = 2, name = "Bank", url = "Bank", parent_id = 1, big_icon = " ", small_icon = " ", created_by = 1, created_on = DateTime.Now },
                    new M_menu() { id = 3, name = "Jenjang Pendidikan", url = "JenjangPendidikan", parent_id = 1, big_icon = " ", small_icon = " ", created_by = 1, created_on = DateTime.Now },
                    new M_menu() { id = 4, name = "Kategori Faskes", url = "KategoriFaskes", parent_id = 1, big_icon = " ", small_icon = " ", created_by = 1, created_on = DateTime.Now },
                    new M_menu() { id = 5, name = "Hubungan Pasien", url = "HubunganPasien", parent_id = 1, big_icon = " ", small_icon = " ", created_by = 1, created_on = DateTime.Now },
                    new M_menu() { id = 6, name = "Kategori ProdKes", url = "KategoriProdKes", parent_id = 1, big_icon = " ", small_icon = " ", created_by = 1, created_on = DateTime.Now },
                    new M_menu() { id = 7, name = "Segmen ProdKes", url = "SegmenProdKes", parent_id = 1, big_icon = " ", small_icon = " ", created_by = 1, created_on = DateTime.Now },
                    new M_menu() { id = 8, name = "Golongan Darah", url = "GolonganDarah", parent_id = 1, big_icon = " ", small_icon = " ", created_by = 1, created_on = DateTime.Now },
                    new M_menu() { id = 9, name = "Lokasi", url = "Lokasi", parent_id = 1, big_icon = " ", small_icon = " ", created_by = 1, created_on = DateTime.Now },
                    new M_menu() { id = 10, name = "Setting", url = "Setting", parent_id = 1, big_icon = " ", small_icon = " ", created_by = 1, created_on = DateTime.Now },
                    new M_menu() { id = 11, name = "Transaksi", url = "", parent_id = null, big_icon = " ", small_icon = " ", created_by = 1, created_on = DateTime.Now },
                    new M_menu() { id = 12, name = "Cari Dokter", url = "CariDokter", parent_id = 11, big_icon = " ", small_icon = " ", created_by = 1, created_on = DateTime.Now },
                    new M_menu() { id = 13, name = "Cari Obat", url = "CariObat", parent_id = 11, big_icon = " ", small_icon = " ", created_by = 1, created_on = DateTime.Now }
                );

            modelBuilder.Entity<M_menu_role>()
                .HasData(
                    new M_menu_role() { id = 1, menu_id = 2, role_id = 1, created_by = 1, created_on = DateTime.Now },
                    new M_menu_role() { id = 2, menu_id = 3, role_id = 1, created_by = 1, created_on = DateTime.Now },
                    new M_menu_role() { id = 3, menu_id = 4, role_id = 1, created_by = 1, created_on = DateTime.Now },
                    new M_menu_role() { id = 4, menu_id = 5, role_id = 1, created_by = 1, created_on = DateTime.Now },
                    new M_menu_role() { id = 5, menu_id = 6, role_id = 1, created_by = 1, created_on = DateTime.Now },
                    new M_menu_role() { id = 6, menu_id = 7, role_id = 1, created_by = 1, created_on = DateTime.Now },
                    new M_menu_role() { id = 7, menu_id = 8, role_id = 1, created_by = 1, created_on = DateTime.Now },
                    new M_menu_role() { id = 8, menu_id = 9, role_id = 1, created_by = 1, created_on = DateTime.Now },
                    new M_menu_role() { id = 9, menu_id = 10, role_id = 1, created_by = 1, created_on = DateTime.Now },
                    new M_menu_role() { id = 10, menu_id = 12, role_id = 3, created_by = 1, created_on = DateTime.Now },
                    new M_menu_role() { id = 11, menu_id = 12, role_id = 5, created_by = 1, created_on = DateTime.Now },
                    new M_menu_role() { id = 12, menu_id = 13, role_id = 3, created_by = 1, created_on = DateTime.Now },
                    new M_menu_role() { id = 13, menu_id = 13, role_id = 5, created_by = 1, created_on = DateTime.Now }
                );

            modelBuilder.Entity<M_bank>()
                .HasData(
                    new M_bank() { id = 1, name = "Bank BCA", va_code = "00834221", created_by = 1, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_bank() { id = 2, name = "Bank BNI", va_code = "03127532", created_by = 1, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_bank() { id = 3, name = "Bank Mandiri", va_code = "15209312", created_by = 1, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_bank() { id = 4, name = "Bank Permata", va_code = "16591734", created_by = 1, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_bank() { id = 5, name = "Bank BTPN", va_code = "00682637", created_by = 1, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_bank() { id = 6, name = "Bank NISP", va_code = "78231424", created_by = 1, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false }
                );

            modelBuilder.Entity<M_customer_relation>()
                .HasData(
                    new M_customer_relation() { id = 1, name = "Ayah", created_by = 5, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_customer_relation() { id = 2, name = "Ibu", created_by = 5, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_customer_relation() { id = 3, name = "Anak", created_by = 5, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false }
                );

            modelBuilder.Entity<M_blood_group>()
                .HasData(
                    new M_blood_group() { id = 1, code = "A", description = "Golongan A", created_by = 9, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_blood_group() { id = 2, code = "B", description = "Golongan B", created_by = 9, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_blood_group() { id = 3, code = "O", description = "Golongan O", created_by = 9, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_blood_group() { id = 4, code = "AB", description = "Golongan AB", created_by = 9, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false }
                );

            modelBuilder.Entity<M_medical_facility_category>()
                .HasData(
                    new M_medical_facility_category() { id = 1, name = "Apotek", created_by = 7, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_medical_facility_category() { id = 2, name = "Rumah Sakit", created_by = 7, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_medical_facility_category() { id = 3, name = "Klinik", created_by = 7, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_medical_facility_category() { id = 4, name = "Puskesmas", created_by = 7, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false }
                );

            modelBuilder.Entity<M_education_level>()
                .HasData(
                    new M_education_level() { id = 1, name = "S1", created_by = 3, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_education_level() { id = 2, name = "S2", created_by = 3, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_education_level() { id = 3, name = "S3", created_by = 3, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false }
                );

            modelBuilder.Entity<M_medical_item_segmentation>()
                .HasData(
                    new M_medical_item_segmentation() { id = 1, name = "Obat Herbal Terstandar (OHT)", created_by = 11, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_medical_item_segmentation() { id = 2, name = "Obat Bebas (Hijau)", created_by = 11, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_medical_item_segmentation() { id = 3, name = "Obat Bebas Terbatas (Biru)", created_by = 11, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_medical_item_segmentation() { id = 4, name = "Obat Keras (Merah)", created_by = 11, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false }
                );

            modelBuilder.Entity<M_medical_item_category>()
                .HasData(
                    new M_medical_item_category() { id = 1, name = "Demam", created_by = 11, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_medical_item_category() { id = 2, name = "Batuk", created_by = 11, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_medical_item_category() { id = 3, name = "Pilek", created_by = 11, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_medical_item_category() { id = 4, name = "Masuk Angin", created_by = 11, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_medical_item_category() { id = 5, name = "Infeksi", created_by = 11, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false }
                );

            modelBuilder.Entity<M_doctor>()
                .HasData(
                    new M_doctor() { id = 1, biodata_id = 2, str = "12345678901", created_by = 7, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_doctor() { id = 2, biodata_id = 4, str = "23456789021", created_by = 7, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new M_doctor() { id = 3, biodata_id = 7, str = "34567890312", created_by = 7, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false }
                );

            modelBuilder.Entity<M_customer>()
                .HasData(
                    new M_customer() { id = 1, biodata_id = 3, dob = new DateTime(1999, 07, 09), gender = "L", blood_group_id = 1, rhesus_type = "Rh+", height = 177, weight = 70, created_by = 5, created_on = DateTime.Now },
                    new M_customer() { id = 2, biodata_id = 8, dob = new DateTime(1999, 10, 15), gender = "P", blood_group_id = 3, rhesus_type = "Rh-", height = 168, weight = 64, created_by = 5, created_on = DateTime.Now },
                    new M_customer() { id = 3, biodata_id = 9, dob = new DateTime(2006, 03, 11), gender = "P", blood_group_id = 1, rhesus_type = "Rh+", height = 130, weight = 40, created_by = 5, created_on = DateTime.Now },
                    new M_customer() { id = 4, biodata_id = 10, dob = new DateTime(1970, 12, 31), gender = "L", blood_group_id = 1, rhesus_type = "Rh-", height = 170, weight = 60, created_by = 5, created_on = DateTime.Now },
                    new M_customer() { id = 5, biodata_id = 4, dob = new DateTime(1999, 09, 07), gender = "L", blood_group_id = 4, rhesus_type = "Rh+", height = 167, weight = 65, created_by = 5, created_on = new DateTime(2022, 05, 09) },
                    new M_customer() { id = 6, biodata_id = 6, dob = new DateTime(1999, 08, 07), gender = "L", blood_group_id = 4, rhesus_type = "Rh+", height = 167, weight = 73, created_by = 5, created_on = new DateTime(2022, 05, 09) }
                );

            modelBuilder.Entity<M_specialization>()
                .HasData(
                    new M_specialization() { id = 1, name = "Spesialisasi Anak", created_by = 9, created_on = DateTime.Now },
                    new M_specialization() { id = 2, name = "Spesialisasi Tulang", created_by = 9, created_on = DateTime.Now },
                    new M_specialization() { id = 3, name = "Spesialisasi Gigi", created_by = 9, created_on = DateTime.Now },
                    new M_specialization() { id = 4, name = "Spesialisasi Kulit", created_by = 9, created_on = DateTime.Now },
                    new M_specialization() { id = 5, name = "Spesialisasi THT", created_by = 9, created_on = DateTime.Now },
                    new M_specialization() { id = 6, name = "Spesialisasi Mata", created_by = 9, created_on = DateTime.Now },
                    new M_specialization() { id = 7, name = "Spesialisasi Jantung", created_by = 9, created_on = DateTime.Now },
                    new M_specialization() { id = 8, name = "Spesialisasi Paru-Paru", created_by = 9, created_on = DateTime.Now }
                );

            modelBuilder.Entity<M_location>()
                .HasData(
                    new M_location() { id = 1, name = "DKI Jakarta", parent_id = null, location_level_id = 1, created_by = 9, created_on = DateTime.Now},
                    new M_location() { id = 2, name = "Jawa Barat", parent_id = null, location_level_id = 1, created_by = 9, created_on = DateTime.Now},
                    new M_location() { id = 3, name = "Jakarta Pusat", parent_id = 1, location_level_id = 2, created_by = 9, created_on = DateTime.Now},
                    new M_location() { id = 4, name = "Jakarta Selatan", parent_id = 1, location_level_id = 2, created_by = 9, created_on = DateTime.Now},
                    new M_location() { id = 5, name = "Jakarta Timur", parent_id = 1, location_level_id = 2, created_by = 9, created_on = DateTime.Now},
                    new M_location() { id = 6, name = "Depok", parent_id = 2, location_level_id = 2, created_by = 9, created_on = DateTime.Now},
                    new M_location() { id = 7, name = "Bekasi", parent_id = 2, location_level_id = 2, created_by = 9, created_on = DateTime.Now}
                );

            modelBuilder.Entity<M_medical_facility>()
                .HasData(
                    new M_medical_facility() { id = 1, name = "RS Mitra Keluarga", medical_facility_category_id = 2, location_id = 6, full_address = "Jl. Margonda Raya, Depok, Kec. Pancoran Mas, Kota Depok, Jawa Barat 16424, Indonesia", email = "mitra_rumahsakit@gmail.com", phone_code = "+62", phone = "21 77210700", fax = "", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility() { id = 2, name = "RSIA Bunda Jakarta", medical_facility_category_id = 2, location_id = 3, full_address = "Jl. Teuku Cik Ditiro No.28, RT.9/RW.2, Gondangdia, Kec. Menteng, Kota Jakarta Pusat, DKI Jakarta 10350, Indonesia", email = "rsiabunda@gmail.com", phone_code = "+62", phone = "1 500 799", fax = "", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility() { id = 3, name = "RSKO Jakarta", medical_facility_category_id = 2, location_id = 5, full_address = "Jl. Lapangan Tembak No.75 Cibubur Jakarta Timur 13720", email = "rskojakarta@yahoo.co.id", phone_code = "+62", phone = "21 87711968", fax = "87711969", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility() { id = 4, name = "RS Haji Jakarta", medical_facility_category_id = 2, location_id = 5, full_address = "l. Raya Pd. Gede No.4, RT.1/RW.1, Lubang Buaya, Kec. Makasar, Kota Jakarta Timur, DKI Jakarta 13650, Indonesia", email = "rs_hajijakarta@gmail.com", phone_code = "+62", phone = "21 8000694", fax = "", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility() { id = 5, name = "RS Meilia Cibubur", medical_facility_category_id = 2, location_id = 6, full_address = "Jl. Alternatif Cibubur No.1, Harjamukti, Kec. Cimanggis, Kota Depok, Jawa Barat 16454, Indonesia", email = "meiliahospital@gmail.com", phone_code = "+62", phone = "21 8444444", fax = "", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility() { id = 6, name = "SamMarie Clinic @ Bassura City", medical_facility_category_id = 3, location_id = 5, full_address = " Bassura City Tower F Lt. GF No. CL-02, RT.10/RW.2, Cipinang Besar, Kec. Jatinegara, Kota Jakarta Timur, DKI Jakarta 13410, Indonesia", email = "samclinic@gmail.com", phone_code = "+62", phone = "813-3000-5010", fax = "", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility() { id = 7, name = "Good Doctors Medical Centre ", medical_facility_category_id = 3, location_id = 4, full_address = "Jl. H. R. Rasuna Said, RT.2/RW.5, Karet Kuningan, Kec. Setiabudi, Kota Jakarta Selatan, DKI Jakarta 12940, Indonesia", email = "gooddoctors@gmail.com", phone_code = "+62", phone = "21 29912131", fax = "", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility() { id = 8, name = "Apotek Kemajuan Mayestik ", medical_facility_category_id = 1, location_id = 4, full_address = "Pasar Mayestik Lt. Semi B Blok AKS 087-088, Jl. Tebah III No.RT.14, RT.14/RW.3, Kec. Kby. Baru, Kota Jakarta Selatan, DKI Jakarta 12120, Indonesia", email = "", phone_code = "+62", phone = "21 29395120", fax = "", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility() { id = 9, name = "Puskesmas Kramat Pela", medical_facility_category_id = 4, location_id = 4, full_address = "Jl. Gandaria Tengah V No.2, RW.1, Kramat Pela, Kec. Kby. Baru, Kota Jakarta Selatan, DKI Jakarta 12130, Indonesia", email = "", phone_code = "+62", phone = "21 7233832", fax = "", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility() { id = 10, name = "RS Misu", medical_facility_category_id = 2, location_id = 4, full_address = "Jl. Kebenaran Kav 3s", email = "", phone_code = "+62", phone = "82192562323", fax = "", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility() { id = 11, name = "Siloam Hospital TB Simatupang", medical_facility_category_id = 2, location_id = 4, full_address = "Jl. RA. Kartini No. 8, Cilandak Jakarta Selatan", email = "", phone_code = "+62", phone = "21 2953 1900", fax = "", created_by = 5, created_on = DateTime.Now }
                );

            modelBuilder.Entity<T_doctor_office>()
                .HasData(
                    new T_doctor_office() { id = 1, doctor_id = 1, medical_facility_id = 1, specialization = "Dokter Anak", created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office() { id = 2, doctor_id = 2, medical_facility_id = 10, specialization = "Dokter Tulang", created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office() { id = 3, doctor_id = 2, medical_facility_id = 11, specialization = "Dokter Tulang", created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office() { id = 4, doctor_id = 3, medical_facility_id = 1, specialization = "Dokter Anak", created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office() { id = 5, doctor_id = 3, medical_facility_id = 2, specialization = "Dokter Anak", created_by = 5, created_on = DateTime.Now }
                );

            modelBuilder.Entity<T_doctor_office_schedule>()
                .HasData(
                    new T_doctor_office_schedule() { id = 1, doctor_id = 1, medical_facility_id = 1, specialization = "Dokter Anak", slot = 4, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_schedule() { id = 2, doctor_id = 2, medical_facility_id = 10, specialization = "Dokter Tulang", slot = 3, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_schedule() { id = 3, doctor_id = 2, medical_facility_id = 11, specialization = "Dokter Tulang", slot = 2, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_schedule() { id = 4, doctor_id = 3, medical_facility_id = 1, specialization = "Dokter Anak", slot = 3, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_schedule() { id = 5, doctor_id = 3, medical_facility_id = 2, specialization = "Dokter Anak", slot = 2, created_by = 5, created_on = DateTime.Now }
                );

            modelBuilder.Entity<M_medical_facility_schedule>()
                .HasData(
                    new M_medical_facility_schedule() { id = 1, medical_facility_id = 1, day = "Senin", time_schedule_start = "08:00", time_schedule_end = "20:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 2, medical_facility_id = 1, day = "Selasa", time_schedule_start = "08:00", time_schedule_end = "20:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 3, medical_facility_id = 1, day = "Rabu", time_schedule_start = "08:00", time_schedule_end = "20:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 4, medical_facility_id = 1, day = "Kamis", time_schedule_start = "08:00", time_schedule_end = "20:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 5, medical_facility_id = 1, day = "Jumat", time_schedule_start = "08:00", time_schedule_end = "20:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 6, medical_facility_id = 1, day = "Minggu", time_schedule_start = "08:00", time_schedule_end = "15:00", created_by = 5, created_on = DateTime.Now },

                    new M_medical_facility_schedule() { id = 7, medical_facility_id = 2, day = "Senin", time_schedule_start = "08:30", time_schedule_end = "20:30", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 8, medical_facility_id = 2, day = "Rabu", time_schedule_start = "08:30", time_schedule_end = "20:30", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 9, medical_facility_id = 2, day = "Kamis", time_schedule_start = "08:30", time_schedule_end = "20:30", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 10, medical_facility_id = 2, day = "Jumat", time_schedule_start = "08:30", time_schedule_end = "20:30", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 11, medical_facility_id = 2, day = "Sabtu", time_schedule_start = "08:30", time_schedule_end = "18:30", created_by = 5, created_on = DateTime.Now },

                    new M_medical_facility_schedule() { id = 12, medical_facility_id = 3, day = "Rabu", time_schedule_start = "09:00", time_schedule_end = "21:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 13, medical_facility_id = 3, day = "Sabtu", time_schedule_start = "09:00", time_schedule_end = "15:00", created_by = 5, created_on = DateTime.Now },

                    new M_medical_facility_schedule() { id = 14, medical_facility_id = 4, day = "Senin", time_schedule_start = "07:00", time_schedule_end = "21:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 15, medical_facility_id = 4, day = "Kamis", time_schedule_start = "07:00", time_schedule_end = "21:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 16, medical_facility_id = 4, day = "Jumat", time_schedule_start = "07:00", time_schedule_end = "21:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 17, medical_facility_id = 4, day = "Sabtu", time_schedule_start = "07:00", time_schedule_end = "16:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 18, medical_facility_id = 4, day = "Minggu", time_schedule_start = "07:00", time_schedule_end = "16:00", created_by = 5, created_on = DateTime.Now },

                    new M_medical_facility_schedule() { id = 19, medical_facility_id = 5, day = "Senin", time_schedule_start = "08:00", time_schedule_end = "21:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 20, medical_facility_id = 5, day = "Rabu", time_schedule_start = "08:00", time_schedule_end = "21:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 21, medical_facility_id = 5, day = "Jumat", time_schedule_start = "08:00", time_schedule_end = "14:00", created_by = 5, created_on = DateTime.Now },

                    new M_medical_facility_schedule() { id = 22, medical_facility_id = 6, day = "Senin", time_schedule_start = "08:00", time_schedule_end = "21:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 23, medical_facility_id = 6, day = "Selasa", time_schedule_start = "08:00", time_schedule_end = "21:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 24, medical_facility_id = 6, day = "Kamis", time_schedule_start = "08:00", time_schedule_end = "21:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 25, medical_facility_id = 6, day = "Jumat", time_schedule_start = "08:00", time_schedule_end = "21:00", created_by = 5, created_on = DateTime.Now },

                    new M_medical_facility_schedule() { id = 26, medical_facility_id = 7, day = "Selasa", time_schedule_start = "08:00", time_schedule_end = "21:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 27, medical_facility_id = 7, day = "Rabu", time_schedule_start = "08:00", time_schedule_end = "21:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 28, medical_facility_id = 7, day = "Jumat", time_schedule_start = "08:00", time_schedule_end = "21:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 29, medical_facility_id = 7, day = "Sabtu", time_schedule_start = "08:00", time_schedule_end = "18:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 30, medical_facility_id = 7, day = "Minggu", time_schedule_start = "08:00", time_schedule_end = "18:00", created_by = 5, created_on = DateTime.Now },

                    new M_medical_facility_schedule() { id = 31, medical_facility_id = 8, day = "Senin", time_schedule_start = "10:00", time_schedule_end = "20:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 32, medical_facility_id = 8, day = "Selasa", time_schedule_start = "10:00", time_schedule_end = "20:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 33, medical_facility_id = 8, day = "Rabu", time_schedule_start = "10:00", time_schedule_end = "20:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 34, medical_facility_id = 8, day = "Kamis", time_schedule_start = "10:00", time_schedule_end = "20:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 35, medical_facility_id = 8, day = "Jumat", time_schedule_start = "10:00", time_schedule_end = "18:00", created_by = 5, created_on = DateTime.Now },

                    new M_medical_facility_schedule() { id = 36, medical_facility_id = 9, day = "Senin", time_schedule_start = "07:00", time_schedule_end = "16:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 37, medical_facility_id = 9, day = "Rabu", time_schedule_start = "07:00", time_schedule_end = "16:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 38, medical_facility_id = 9, day = "Jumat", time_schedule_start = "07:00", time_schedule_end = "11:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 39, medical_facility_id = 9, day = "Sabtu", time_schedule_start = "07:00", time_schedule_end = "12:00", created_by = 5, created_on = DateTime.Now },

                    new M_medical_facility_schedule() { id = 40, medical_facility_id = 10, day = "Selasa", time_schedule_start = "08:00", time_schedule_end = "18:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 41, medical_facility_id = 10, day = "Kamis", time_schedule_start = "08:00", time_schedule_end = "18:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 42, medical_facility_id = 10, day = "Jumat", time_schedule_start = "08:00", time_schedule_end = "18:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 43, medical_facility_id = 10, day = "Sabtu", time_schedule_start = "08:00", time_schedule_end = "18:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 44, medical_facility_id = 10, day = "Minggu", time_schedule_start = "08:00", time_schedule_end = "15:00", created_by = 5, created_on = DateTime.Now },

                    new M_medical_facility_schedule() { id = 45, medical_facility_id = 11, day = "Senin", time_schedule_start = "07:00", time_schedule_end = "20:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 46, medical_facility_id = 11, day = "Selasa", time_schedule_start = "07:00", time_schedule_end = "20:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 47, medical_facility_id = 11, day = "Rabu", time_schedule_start = "07:00", time_schedule_end = "20:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 48, medical_facility_id = 11, day = "Kamis", time_schedule_start = "07:00", time_schedule_end = "20:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 49, medical_facility_id = 11, day = "Jumat", time_schedule_start = "07:00", time_schedule_end = "18:00", created_by = 5, created_on = DateTime.Now },
                    new M_medical_facility_schedule() { id = 50, medical_facility_id = 11, day = "Sabtu", time_schedule_start = "07:00", time_schedule_end = "18:00", created_by = 5, created_on = DateTime.Now }
                );

            modelBuilder.Entity<T_doctor_treatment>()
                .HasData(
                    new T_doctor_treatment() { id = 1, doctor_id = 1, name = "Konsultasi Kesehatan Anak", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 2, doctor_id = 1, name = "Vaksinasi Anak", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 3, doctor_id = 1, name = "Konsultasi Alergi Anak", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 4, doctor_id = 1, name = "Konsultasi Psikologi Anak", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 5, doctor_id = 1, name = "Fisioterapi Anak", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 6, doctor_id = 1, name = "Tes Pendengaran Anak", created_by = 7, created_on = DateTime.Now },

                    new T_doctor_treatment() { id = 7, doctor_id = 2, name = "Konsultasi Kesehatan Tulang", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 8, doctor_id = 2, name = "Fisioterapi Tulang", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 9, doctor_id = 2, name = "Arthoroskopi", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 10, doctor_id = 2, name = "Fiksasi Internal", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 11, doctor_id = 2, name = "Fusion Tulang", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 12, doctor_id = 2, name = "Penggantian Sendi", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 13, doctor_id = 2, name = "Osteotomi", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 14, doctor_id = 2, name = "Rekonstruksi Ligamen, Tulang, dan Otot", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 15, doctor_id = 2, name = "Operasi Tulang Belakang", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 16, doctor_id = 2, name = "Peremajaan Tulang Rawan", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 17, doctor_id = 2, name = "Amputasi", created_by = 7, created_on = DateTime.Now },

                    new T_doctor_treatment() { id = 18, doctor_id = 3, name = "Konsultasi Kesehatan Anak", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 19, doctor_id = 3, name = "Vaksinasi Anak", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 20, doctor_id = 3, name = "Konsultasi Alergi Anak", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 21, doctor_id = 3, name = "Konsultasi Psikologi Anak", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 22, doctor_id = 3, name = "Fisioterapi Anak", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 23, doctor_id = 3, name = "Tes Pendengaran Anak", created_by = 7, created_on = DateTime.Now },
                    new T_doctor_treatment() { id = 24, doctor_id = 3, name = "Skrinning Tumbuh Kembang Anak", created_by = 7, created_on = DateTime.Now }
                );

            modelBuilder.Entity<T_doctor_office_treatment>()
                .HasData(
                    new T_doctor_office_treatment() { id = 1, doctor_treatment_id = 1, doctor_office_id = 1, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 2, doctor_treatment_id = 2, doctor_office_id = 1, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 3, doctor_treatment_id = 3, doctor_office_id = 1, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 4, doctor_treatment_id = 4, doctor_office_id = 1, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 5, doctor_treatment_id = 5, doctor_office_id = 1, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 6, doctor_treatment_id = 6, doctor_office_id = 1, created_by = 5, created_on = DateTime.Now },
                                                            
                    new T_doctor_office_treatment() { id = 7, doctor_treatment_id = 7, doctor_office_id = 2, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 8, doctor_treatment_id = 8, doctor_office_id = 2, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 9, doctor_treatment_id = 9, doctor_office_id = 2, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 10, doctor_treatment_id = 10, doctor_office_id = 2, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 11, doctor_treatment_id = 11, doctor_office_id = 2, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 12, doctor_treatment_id = 12, doctor_office_id = 2, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 13, doctor_treatment_id = 13, doctor_office_id = 2, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 14, doctor_treatment_id = 14, doctor_office_id = 2, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 15, doctor_treatment_id = 15, doctor_office_id = 2, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 16, doctor_treatment_id = 16, doctor_office_id = 2, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 17, doctor_treatment_id = 17, doctor_office_id = 2, created_by = 5, created_on = DateTime.Now },

                    new T_doctor_office_treatment() { id = 18, doctor_treatment_id = 7, doctor_office_id = 3, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 19, doctor_treatment_id = 8, doctor_office_id = 3, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 20, doctor_treatment_id = 9, doctor_office_id = 3, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 21, doctor_treatment_id = 10, doctor_office_id = 3, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 22, doctor_treatment_id = 11, doctor_office_id = 3, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 23, doctor_treatment_id = 12, doctor_office_id = 3, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 24, doctor_treatment_id = 13, doctor_office_id = 3, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 25, doctor_treatment_id = 14, doctor_office_id = 3, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 26, doctor_treatment_id = 15, doctor_office_id = 3, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 27, doctor_treatment_id = 16, doctor_office_id = 3, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 28, doctor_treatment_id = 17, doctor_office_id = 3, created_by = 5, created_on = DateTime.Now },

                    new T_doctor_office_treatment() { id = 29, doctor_treatment_id = 18, doctor_office_id = 4, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 30, doctor_treatment_id = 19, doctor_office_id = 4, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 31, doctor_treatment_id = 20, doctor_office_id = 4, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 32, doctor_treatment_id = 21, doctor_office_id = 4, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 33, doctor_treatment_id = 22, doctor_office_id = 4, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 34, doctor_treatment_id = 23, doctor_office_id = 4, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 35, doctor_treatment_id = 24, doctor_office_id = 4, created_by = 5, created_on = DateTime.Now },

                    new T_doctor_office_treatment() { id = 36, doctor_treatment_id = 18, doctor_office_id = 5, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 37, doctor_treatment_id = 19, doctor_office_id = 5, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 38, doctor_treatment_id = 20, doctor_office_id = 5, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 39, doctor_treatment_id = 21, doctor_office_id = 5, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 40, doctor_treatment_id = 22, doctor_office_id = 5, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 41, doctor_treatment_id = 23, doctor_office_id = 5, created_by = 5, created_on = DateTime.Now },
                    new T_doctor_office_treatment() { id = 42, doctor_treatment_id = 24, doctor_office_id = 5, created_by = 5, created_on = DateTime.Now }
                );

            modelBuilder.Entity<M_wallet_default_nominal>()
            .HasData(
                new M_wallet_default_nominal() { id = 1, nominal = 50000, created_by = 6, created_on = new DateTime(2022, 05, 09), modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                new M_wallet_default_nominal() { id = 2, nominal = 100000, created_by = 6, created_on = new DateTime(2022, 05, 09), modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                new M_wallet_default_nominal() { id = 3, nominal = 150000, created_by = 6, created_on = new DateTime(2022, 05, 09), modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                new M_wallet_default_nominal() { id = 4, nominal = 200000, created_by = 6, created_on = new DateTime(2022, 05, 09), modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false }
            );

            modelBuilder.Entity<T_customer_registered_card>()
                .HasData(
                    new T_customer_registered_card() { id = 1, customer_id = 1, card_number = "00834221000000000991", validity_period = new DateTime(2022, 05, 09), cvv = "00991", created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_registered_card() { id = 2, customer_id = 2, card_number = "00834221000000000992", validity_period = new DateTime(2022, 05, 09), cvv = "00992", created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_registered_card() { id = 3, customer_id = 3, card_number = "00834221000000000993", validity_period = new DateTime(2022, 05, 09), cvv = "00993", created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_registered_card() { id = 4, customer_id = 4, card_number = "00834221000000000994", validity_period = new DateTime(2022, 05, 09), cvv = "00994", created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_registered_card() { id = 5, customer_id = 5, card_number = "00834221000000000995", validity_period = new DateTime(2022, 05, 09), cvv = "00995", created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_registered_card() { id = 6, customer_id = 6, card_number = "00834221000000000996", validity_period = new DateTime(2022, 05, 09), cvv = "00996", created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false }
                );

            modelBuilder.Entity<T_customer_va>()
                .HasData(
                    new T_customer_va() { id = 1, customer_id = 1, va_number = "00834221000000000991", created_by = 6, created_on = new DateTime(2022, 05, 09), modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_va() { id = 2, customer_id = 2, va_number = "00834221000000000992", created_by = 6, created_on = new DateTime(2022, 05, 09), modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_va() { id = 3, customer_id = 3, va_number = "00834221000000000993", created_by = 6, created_on = new DateTime(2022, 05, 09), modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_va() { id = 4, customer_id = 4, va_number = "00834221000000000994", created_by = 6, created_on = new DateTime(2022, 05, 09), modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_va() { id = 5, customer_id = 5, va_number = "00834221000000000995", created_by = 6, created_on = new DateTime(2022, 05, 09), modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_va() { id = 6, customer_id = 6, va_number = "00834221000000000996", created_by = 6, created_on = new DateTime(2022, 05, 09), modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false }
                );

            modelBuilder.Entity<T_customer_wallet>()
                .HasData(
                    new T_customer_wallet() { id = 1, customer_id = 1, pin = "123456", balance = 1150000, barcode = "hypPLDdUbBFCyqWYZFLzCbU5ke5zvEFCNxvGjHqjopAdZ6ysBh", points = 100, created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_wallet() { id = 2, customer_id = 2, pin = "123456", balance = 1150000, barcode = "fgTBfaaafaTxseecKtCtgYQOFfrzRmUhousthSxzdmboFeaxRH", points = 100, created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_wallet() { id = 3, customer_id = 3, pin = "123456", balance = 1150000, barcode = "TRgWkmhYtqsXyXbZyZDUNymFArmdIOghxlNqltKCTaTbBuYkcu", points = 100, created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_wallet() { id = 4, customer_id = 4, pin = "123456", balance = 1150000, barcode = "NweUlSibshbLaiSRfKIbJoZilUluAYroSEeSDAVeHLKsclzgGr", points = 100, created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_wallet() { id = 5, customer_id = 5, pin = "654321", balance = 1300000, barcode = "L4eiQCrgBA3xmxIISlBzzbqGt3mamKa4llO1ihc5qKqE0nDJvl", points = 100, created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_wallet() { id = 6, customer_id = 6, pin = "098765", balance = 1500000, barcode = "cnryVXvdO5cgCacL6wiTbjA2tAw15PbZxqhlwRPvOWvo8FYRP0", points = 100, created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false }
                );

            modelBuilder.Entity<T_customer_custom_nominal>()
                .HasData(
                    new T_customer_custom_nominal() { id = 1, customer_id = 1, nominal = 125000, created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_custom_nominal() { id = 2, customer_id = 2, nominal = 125000, created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_custom_nominal() { id = 3, customer_id = 3, nominal = 125000, created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_custom_nominal() { id = 4, customer_id = 4, nominal = 125000, created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_custom_nominal() { id = 5, customer_id = 5, nominal = 125000, created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false },
                    new T_customer_custom_nominal() { id = 6, customer_id = 6, nominal = 125000, created_by = 6, created_on = DateTime.Now, modified_by = null, modified_on = null, deleted_by = null, deleted_on = null, is_delete = false }
                );

        }
    }

    public class MiniProjDbContext : DbContext
    {
        public DbSet<M_bank> M_banks { get; set; }

        public DbSet<M_biodata> M_biodatas { get; set; }

        public DbSet<M_blood_group> M_blood_groups { get; set; }

        public DbSet<M_customer_relation> M_customer_relations { get; set; }

        public DbSet<M_education_level> M_education_levels { get; set; }

        public DbSet<M_medical_facility_category> M_medical_facility_categories { get; set; }

        public DbSet<M_medical_item_category> M_medical_item_categories { get; set; }

        public DbSet<M_medical_item_segmentation> M_medical_item_segmentations { get; set; }

        public DbSet<M_medical_item> M_medical_items { get; set; }

        public DbSet<M_role> M_roles { get; set; }

        public DbSet<M_user> M_users { get; set; }

        public DbSet<M_admin> M_admins { get; set; }

        public DbSet<M_biodata_address> M_biodata_address { get; set; }

        public DbSet<M_courier> M_couriers { get; set; }

        public DbSet<M_customer> M_customers { get; set; }

        public DbSet<M_customer_member> M_customer_members { get; set; }

        public DbSet<M_doctor> M_doctors { get; set; }

        public DbSet<M_doctor_education> M_doctor_educations { get; set; }

        public DbSet<M_location> M_locations { get; set; }

        public DbSet<M_location_level> M_location_levels { get; set; }

        public DbSet<M_medical_facility> M_medical_facilities { get; set; }

        public DbSet<M_medical_facility_schedule> M_medical_facility_schedules { get; set; }

        public DbSet<T_treatment_discount> T_treatment_discounts { get; set; }

        public DbSet<M_courier_type> M_courier_types { get; set; }

        public DbSet<T_courier_discount> T_courier_discounts { get; set; }

        public DbSet<T_customer_wallet_withdraw> T_customer_wallet_withdraws { get; set; }

        public DbSet<T_customer_wallet_top_up> T_customer_wallet_top_ups { get; set; }

        public DbSet<T_doctor_office> T_doctor_offices { get; set; }

        public DbSet<T_doctor_office_schedule> T_doctor_office_schedules { get; set; }

        public DbSet<T_doctor_office_treatment> T_doctor_office_treatments { get; set; }

        public DbSet<T_doctor_office_treatment_price> T_doctor_office_treatment_prices { get; set; }

        public DbSet<T_doctor_treatment> T_doctor_treatments { get; set; }

        public DbSet<T_medical_item_purchase> T_medical_item_purchases { get; set; }

        public DbSet<T_medical_item_purchase_detail> T_medical_item_purchase_details { get; set; }

        public DbSet<T_reset_password> T_reset_passwords { get; set; }

        public DbSet<T_token> T_tokens { get; set; }

        public DbSet<T_appointment_done> T_appointment_dones { get; set; }

        public DbSet<T_appointment_reschedule_history> T_appointment_reschedule_histories { get; set; }

        public DbSet<T_current_doctor_specialization> T_current_doctor_specializations { get; set; }

        public DbSet<T_customer_chat> T_customer_chats { get; set; }

        public DbSet<T_customer_chat_history> T_customer_chat_histories { get; set; }

        public DbSet<T_customer_custom_nominal> T_customer_custom_nominals { get; set; }

        public DbSet<T_customer_registered_card> T_customer_registered_cards { get; set; }

        public DbSet<T_customer_va> T_customer_vas { get; set; }

        public DbSet<T_customer_va_history> T_customer_va_histories { get; set; }

        public DbSet<T_customer_wallet> T_customer_wallets { get; set; }

        public DbSet<M_menu> M_menus { get; set; }

        public DbSet<M_menu_role> M_menu_roles { get; set; }

        public DbSet<M_payment_method> M_payment_methods { get; set; }

        public DbSet<M_specialization> M_specializations { get; set; }

        public DbSet<M_biodata_attachment> M_biodata_attachments { get; set; }

        public DbSet<M_wallet_default_nominal> M_wallet_default_nominals { get; set; }

        public DbSet<T_appointment> T_appointments { get; set; }

        public DbSet<T_appointment_cancellation> T_appointment_cancellations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IConfigurationRoot builder = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();

            optionsBuilder.UseSqlServer(builder.GetConnectionString("DB_XPOS_CONN"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();

            modelBuilder.Entity<M_user>()
                .HasIndex(o => o.email)
                .IsUnique();

            //modelBuilder.Entity<M_bank>()
            //    .HasIndex(o => o.name)
            //    .IsUnique();

            //modelBuilder.Entity<M_blood_group>()
            //    .HasIndex(o => o.code)
            //    .IsUnique();

            //modelBuilder.Entity<M_customer_relation>()
            //    .HasIndex(o => o.name)
            //    .IsUnique();

            //modelBuilder.Entity<M_medical_facility_category>()
            //    .HasIndex(o => o.name)
            //    .IsUnique();

            //modelBuilder.Entity<M_medical_item_segmentation>()
            //    .HasIndex(o => o.name)
            //    .IsUnique();

            //modelBuilder.Entity<M_medical_item_category>()
            //    .HasIndex(o => o.name)
            //    .IsUnique();

            //modelBuilder.Entity<M_education_level>()
            //    .HasIndex(o => o.name)
            //    .IsUnique();

            //modelBuilder.Entity<M_location_level>()
            //    .HasIndex(o => o.name)
            //    .IsUnique();

            //modelBuilder.Entity<M_location>()
            //    .HasIndex(o => o.name)
            //    .IsUnique();
        }
    }
}
