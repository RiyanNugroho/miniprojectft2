﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class M_doctor_education : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        public long? doctor_id { get; set; }

        public long? education_level_id { get; set; }

        [MaxLength(100)]
        public string institution_name { get; set; }

        [MaxLength(100)]
        public string major { get; set; }

        [MaxLength(4)]
        public string start_year { get; set; }

        [MaxLength(4)]
        public string end_year { get; set; }

        public bool is_last_education { get; set; }

        [ForeignKey("doctor_id")]
        public virtual M_doctor M_doctor { get; set; }

        [ForeignKey("education_level_id")]
        public virtual M_education_level M_education_level { get; set; }
    }
}
