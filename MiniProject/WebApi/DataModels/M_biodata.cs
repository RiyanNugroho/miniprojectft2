﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class M_biodata : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        [MaxLength(255)]
        public string fullname { get; set; }

        [MaxLength(15)]
        public string mobile_phone { get; set; }

        [DataType("varbinary(max)")]
        public byte[] image { get; set; }

        [MaxLength(255)]
        public string image_path { get; set; }
    }
}
