﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class M_customer_member : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        public long? customer_id { get; set; }

        public long? customer_relation_id { get; set; }

        [ForeignKey("customer_id")]
        public virtual M_customer M_customer { get; set; }
        
        [ForeignKey("customer_relation_id")]
        public virtual M_customer_relation M_customer_relation { get; set; }
    }
}
