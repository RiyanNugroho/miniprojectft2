﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class M_location : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        [MaxLength(100)]
        public string name { get; set; }

        public long? parent_id { get; set; }

        public long? location_level_id { get; set; }

        [ForeignKey("location_level_id")]
        public virtual M_location_level M_location_level { get; set; }

        [ForeignKey("parent_id")]
        public virtual M_location location { get; set; }
    }
}
