﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class BaseProperties
    {
        public BaseProperties()
        {
            is_delete = false;
        }

        [Required]
        public long created_by { get; set; }

        public DateTime created_on { get; set; }

        public long? modified_by { get; set; }

        public DateTime? modified_on { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_on { get; set; }

        [Required]
        public bool is_delete { get; set; }

        //[ForeignKey("created_by")]
        //public virtual M_user M_userCreated { get; set; }
    }
}
