﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class M_medical_item : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        [MaxLength(50)]
        public string name { get; set; }

        public long? medical_item_category_id { get; set; }

        public string composition { get; set; }

        public long? medical_item_segmentation_id { get; set; }

        [MaxLength(100)]
        public string manufacturer { get; set; }

        public string indication { get; set; }

        public string dosage { get; set; }

        public string directions { get; set; }

        public string contraindication { get; set; }

        public string caution { get; set; }

        [MaxLength(50)]
        public string packaging { get; set; }

        public long price_max { get; set; }

        public long price_min { get; set; }

        [DataType("varbinay(max)")]
        public byte[] image { get; set; }

        [MaxLength(100)]
        public string image_path { get; set; }

        [ForeignKey("medical_item_category_id")]
        public virtual M_medical_item_category M_medical_item_category { get; set; }

        [ForeignKey("medical_item_segmentation_id")]
        public virtual M_medical_item_segmentation M_medical_item_segmentation { get; set; }
    }
}
