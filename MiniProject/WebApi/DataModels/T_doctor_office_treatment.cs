﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class T_doctor_office_treatment : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
       
        public long? doctor_treatment_id { get; set; }
       
        public long? doctor_office_id { get; set; }

        [ForeignKey("doctor_treatment_id")]
        public virtual T_doctor_treatment T_doctor_treatment { get; set; }
      
        [ForeignKey("doctor_office_id")]
        public virtual T_doctor_office T_doctor_office { get; set; }
    }
}
