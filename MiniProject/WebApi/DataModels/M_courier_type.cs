﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class M_courier_type : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        public long? courier_id { get; set; }

        [MaxLength(20)]
        public string? name { get; set; }

        [ForeignKey("courier_id")]
        public virtual M_courier M_courier { get; set; }
    }
}
