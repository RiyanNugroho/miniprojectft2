﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class T_medical_item_purchase : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        public long? customer_id { get; set; }
        
        public long? payment_method_id { get; set; }

        [ForeignKey("customer_id")]
        public virtual M_customer customer { get; set; }

        [ForeignKey("payment_method_id")]
        public virtual M_payment_method M_payment_method { get; set; }
    }
}
