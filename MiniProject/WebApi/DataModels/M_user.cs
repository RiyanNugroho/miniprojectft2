﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi.DataModels
{
    public class M_user : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        public long? biodata_id { get; set; }

        public long? role_id { get; set; }

        [MaxLength(100)]
        public string email { get; set; }

        [MaxLength(255)]
        public string password { get; set; }

        public int? login_attempt { get; set; }

        public bool? is_locked { get; set; }

        public DateTime? last_login { get; set; }

        [ForeignKey("biodata_id")]
        public virtual M_biodata m_biodata { get; set; }

        [ForeignKey("role_id")]
        public virtual M_role m_role { get; set; }
    }
}
