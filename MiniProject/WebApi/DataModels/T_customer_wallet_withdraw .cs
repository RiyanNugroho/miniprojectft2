﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class T_customer_wallet_withdraw : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        public long? customer_id { get; set; }
       
        public long wallet_default_nominal_id { get; set; }
        
        public int amount { get; set; }

        [MaxLength(50)]
        
        public string bank_name { get; set; }
        
        [MaxLength(50)]
        public string account_number { get; set; }
        
        [MaxLength(255)]
        public string account_name { get; set; }
        
        public int otp { get; set; }

        [ForeignKey("customer_id")]
        public virtual M_customer M_customer { get; set; }
    }
}
