﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi.DataModels
{
    public class T_current_doctor_specialization : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        public long? doctor_id { get; set; }

        public long? specialization_id { get; set; }

        [ForeignKey("doctor_id")]
        public virtual M_doctor M_doctor { get; set; }

        [ForeignKey("specialization_id")]
        public virtual M_specialization M_specialization { get; set; }
    }
}
