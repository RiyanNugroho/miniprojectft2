﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class T_medical_item_purchase_detail : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        public long? medical_item_purchase_id { get; set; }
        
        public long? medical_item_id { get; set; }
        
        public int qty { get; set; }
        
        public long? medical_facility_id { get; set; }
        
        public long? courier_id { get; set; }
        
        public decimal sub_total { get; set; }

        [ForeignKey("medical_item_purchase_id")]
        public virtual T_medical_item_purchase T_medical_item_purchase { get; set; }

        [ForeignKey("medical_item_id")]
        public virtual M_medical_item M_medical_item { get; set; }

        [ForeignKey("medical_facility_id")]
        public virtual M_medical_facility M_medical_facility { get; set; }

        [ForeignKey("courier_id")]
        public virtual M_courier M_courier { get; set; }
    }
}
