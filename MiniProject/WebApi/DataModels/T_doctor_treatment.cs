﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class T_doctor_treatment : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        public long? doctor_id { get; set; }
        
        public string name { get; set; }

        [ForeignKey("doctor_id")]
        public virtual M_doctor M_doctor { get; set; }
    }
}
