﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class T_treatment_discount : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        [MaxLength(50)]
        public long? doctor_office_treatment_price_id { get; set; }
        
        public decimal value { get; set; }

        [ForeignKey("doctor_office_treatment_price_id")]
        public virtual T_doctor_office_treatment_price T_doctor_office_treatment_price { get; set; }
    }
}
