﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class T_reset_password : BaseProperties
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        
        [MaxLength(255)]
        public string old_password { get; set; }
        
        [MaxLength(255)]
        public string new_password { get; set; }
        
        [MaxLength(20)]
        public string reset_for { get; set; }
    }
}
