﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataModels
{
    public class M_admin : BaseProperties
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        public long? biodata_id { get; set; }

        [MaxLength(10)]
        public string code { get; set; }

        [ForeignKey("biodata_id")]
        public virtual M_biodata M_biodata { get; set; }

    }
}
