﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Security;

namespace WebApp.Services
{
    public class CustomerWalletService : IService<CustomerWalletViewModel>
    {
        private readonly IConfiguration _configuration;
        private readonly string webApiBaseUrl;
        private ResponseResult result = new ResponseResult();

        public CustomerWalletService(IConfiguration configuration)
        {
            _configuration = configuration;
            webApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
        }

        public Task<ResponseResult> Create(CustomerWalletViewModel ent)
        {
            throw new NotImplementedException();
        }

        public Task<ResponseResult> Delete(CustomerWalletViewModel ent)
        {
            throw new NotImplementedException();
        }

        public async Task<ResponseResult> GetAll()
        {
            //throw new NotImplementedException();

            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(webApiBaseUrl + "/CustomerWallet"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<List<CustomerWalletViewModel>>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> GetById(long customer_id)
        {
            //throw new NotImplementedException();
            ResponseResult resutl = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(webApiBaseUrl + "/CustomerWallet/" + customer_id))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<CustomerWalletViewModel>(apiResponse);
                }
            }
            return result;
        }

        public Task<ResponseResult> Update(CustomerWalletViewModel ent)
        {
            throw new NotImplementedException();
        }
    }
}
