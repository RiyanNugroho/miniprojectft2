﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Controllers;
using WebApp.Security;

namespace WebApp.Services
{
    public class DoctorService : IService<DoctorViewModel>
    {
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;
        private ResponseResult result = new ResponseResult();

        public DoctorService(IConfiguration configuration)
        {
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
        }

        public async Task<ResponseResult> Create(DoctorViewModel ent)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                    string strPayLoad = JsonConvert.SerializeObject(ent);
                    HttpContent content = new StringContent(strPayLoad, Encoding.UTF8, "application/json");
                    using (var response = await httpClient.PostAsync(WebApiBaseUrl + "/Doctor", content))
                    {
                        var apiResponse = await response.Content.ReadAsStringAsync();
                        result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                    }
                }
            }

            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
                result.Entity = ent;
            }
            return result;
        }

        public async Task<ResponseResult> Delete(DoctorViewModel ent)
        {
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.DeleteAsync(WebApiBaseUrl + "/Doctor/" + ent.id))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> GetAll()
        {
            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                //string token = ContextAccessor.GetToken();
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(WebApiBaseUrl + "/Doctor"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<List<DoctorViewModel>>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> Search(string name)
        {
            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(WebApiBaseUrl + $"/Doctor/search/{name}"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<List<DoctorViewModel>>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> GetById(long id)
        {
            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(WebApiBaseUrl + $"/Doctor/{id}"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<DoctorViewModel>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> Update(DoctorViewModel ent)
        {
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                string strPayLoad = JsonConvert.SerializeObject(ent);
                HttpContent content = new StringContent(strPayLoad, Encoding.UTF8, "application/json");
                using (var response = await httpClient.PutAsync(WebApiBaseUrl + "/Doctor", content))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                }
                return result;
            }
        }

        public async Task<ResponseResult> Page(int page, int rows, string name)
        {
            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                name = string.IsNullOrEmpty(name) ? "--all" : name;
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(WebApiBaseUrl + $"/Doctor/page/{page}/{rows}/{name}"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                }
            }
            return result;
        }
    }
}
