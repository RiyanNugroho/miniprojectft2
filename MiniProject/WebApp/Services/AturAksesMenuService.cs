﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Controllers;
using WebApp.Security;

namespace WebApp.Services
{
    public class AturaksesMenuService : IService<MenuRoleViewModel>
    {
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;
        private ResponseResult result = new ResponseResult();


        public AturaksesMenuService(IConfiguration configuration)
        {
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
        }


        public async Task<ResponseResult> Create(MenuRoleViewModel ent)
        {
            throw new NotImplementedException();
        }

        public Task<ResponseResult> Delete(MenuRoleViewModel ent)
        {
            throw new NotImplementedException();
        }

        public async Task<ResponseResult> GetAll()
        {
            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {

                using (var response = await httpClient.GetAsync(WebApiBaseUrl + "/AturAksesMenu"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<List<MenuRoleViewModel>>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> GetById(long id)
        {
            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(WebApiBaseUrl + $"/AturAksesMenu/{id}"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<MenuRoleViewModel>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> Update(MenuRoleViewModel ent)
        {
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                string strPayLoad = JsonConvert.SerializeObject(ent);
                HttpContent content = new StringContent(strPayLoad, Encoding.UTF8, "application/json");
                using (var response = await httpClient.PutAsync(WebApiBaseUrl + "/AturAksesMenu", content))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                }
                return result;
            }

        }

        public async Task<ResponseResult> Search(string name)
        {
            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(WebApiBaseUrl + $"/AturAksesMenu/search/{name}"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<List<MenuRoleViewModel>>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> CheckList(List<MenuCheckedViewModel> ent)
        {
            using (var httpClient = new HttpClient())
            {

                string strPayload = JsonConvert.SerializeObject(ent, Formatting.Indented);
                HttpContent content = new StringContent(strPayload, Encoding.UTF8, "application/json");
                using (var response = await httpClient.PostAsync(WebApiBaseUrl + "/AturAksesMenu", content))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                }
            }
            return result;
        }

    }
}
