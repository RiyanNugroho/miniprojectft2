﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System;
using System.Threading.Tasks;
using ViewModel;
using System.Collections.Generic;

namespace WebApp.Services
{
    public class LocationLevelService: IService<LocationLevelViewModel>
    {
        private readonly IConfiguration _configuration;
        private readonly string webApiBaseUrl;
        private ResponseResult result = new ResponseResult();

        public LocationLevelService(IConfiguration configuration)
        {
            _configuration = configuration;
            webApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
        }

        public async Task<ResponseResult> Create(LocationLevelViewModel ent)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                    string strPayload = JsonConvert.SerializeObject(ent);
                    HttpContent content = new StringContent(strPayload, Encoding.UTF8, "application/json");
                    using (var response = await httpClient.PostAsync(webApiBaseUrl + "/LocationLevel", content))
                    {
                        var apiResponse = await response.Content.ReadAsStringAsync();
                        result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                    }
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
                result.Entity = ent;
            }
            return result;
        }

        public async Task<ResponseResult> Delete(LocationLevelViewModel ent)
        {
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.DeleteAsync(webApiBaseUrl + $"/LocationLevel/{ent.id}"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> GetAll()
        {
            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(webApiBaseUrl + "/LocationLevel"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<List<LocationLevelViewModel>>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> GetById(long id)
        {
            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(webApiBaseUrl + $"/LocationLevel/{id}"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<LocationLevelViewModel>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> Update(LocationLevelViewModel ent)
        {
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                string strPayload = JsonConvert.SerializeObject(ent);
                HttpContent content = new StringContent(strPayload, Encoding.UTF8, "application/json");
                using (var response = await httpClient.PutAsync(webApiBaseUrl + "/LocationLevel", content))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                }
            }
            return result;
        }
    }
}
