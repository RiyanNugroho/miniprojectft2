﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Security;

namespace WebApp.Services
{
    public class RoleService : IService<RoleViewModel>
    {
        private readonly IConfiguration _configuration;
        private readonly string webApiBaseUrl;
        private ResponseResult result = new ResponseResult();

        public RoleService(IConfiguration configuration)
        {
            _configuration = configuration;
            webApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
        }

        public async Task<ResponseResult> Create(RoleViewModel ent)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                    string strPayload = JsonConvert.SerializeObject(ent);
                    HttpContent content = new StringContent(strPayload, Encoding.UTF8, "application/json");
                    using (var response = await httpClient.PostAsync(webApiBaseUrl + "/Role", content))
                    {
                        var apiResponse = await response.Content.ReadAsStringAsync();
                        result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                    }
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
                result.Entity = ent;
            }
            return result;
        }

        public async Task<ResponseResult> Delete(RoleViewModel ent)
        {
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.DeleteAsync(webApiBaseUrl + $"/Role/{ent.id}"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> GetAll()
        {
            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(webApiBaseUrl + "/Role"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<List<RoleViewModel>>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> GetById(long id)
        {
            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(webApiBaseUrl + $"/Role/{id}"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<RoleViewModel>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> Update(RoleViewModel ent)
        {
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                string strPayload = JsonConvert.SerializeObject(ent);
                HttpContent content = new StringContent(strPayload, Encoding.UTF8, "application/json");
                using (var response = await httpClient.PutAsync(webApiBaseUrl + "/Role", content))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                }
            }
            return result;
        }
    }
}
