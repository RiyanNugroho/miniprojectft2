﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ViewModel;

namespace WebApp.Services
{
    public class BiodataService : IService<BiodataViewModel>
    {
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;
        private ResponseResult result = new ResponseResult();

        public BiodataService(IConfiguration configuration)
        {
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
        }

        public Task<ResponseResult> Create(BiodataViewModel ent)
        {
            throw new System.NotImplementedException();
        }

        public Task<ResponseResult> Delete(BiodataViewModel ent)
        {
            throw new System.NotImplementedException();
        }

        public async Task<ResponseResult> GetAll()
        {
            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                //string token = ContextAccessor.GetToken();
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(WebApiBaseUrl + "/Biodata"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<List<BiodataViewModel>>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> Search(string name)
        {
            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(WebApiBaseUrl + $"/Biodata/search/{name}"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<List<BiodataViewModel>>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> GetById(long id)
        {
            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(WebApiBaseUrl + $"/Biodata/{id}"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<BiodataViewModel>(apiResponse);
                }
            }
            return result;
        }

        public Task<ResponseResult> Update(BiodataViewModel ent)
        {
            throw new System.NotImplementedException();
        }
    }
}
