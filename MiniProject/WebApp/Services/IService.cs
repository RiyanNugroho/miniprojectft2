﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ViewModel;

namespace WebApp.Services
{
    public interface IService<T>
    {
        Task<ResponseResult> GetAll();

        Task<ResponseResult> GetById(long id);

        Task<ResponseResult> Create(T ent);

        Task<ResponseResult> Update(T ent);

        Task<ResponseResult> Delete(T ent);
    }
}
