﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Security;

namespace WebApp.Services
{
    public class itemSegmentationService : IService<ItemSegmentationViewModel>
    {
        private readonly IConfiguration _configuration;
        private readonly string webApiBaseUrl;
        private ResponseResult result = new ResponseResult();
        public itemSegmentationService(IConfiguration configuration)
        {
            _configuration = configuration;
            webApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
        }
        public async Task<ResponseResult> Create(ItemSegmentationViewModel ent)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                    string strPayLoad = JsonConvert.SerializeObject(ent);
                    HttpContent content = new StringContent(strPayLoad, Encoding.UTF8, "application/json");
                    using (var response = await httpClient.PostAsync(webApiBaseUrl + "/itemSegmentation", content))
                    {
                        var apiResponse = await response.Content.ReadAsStringAsync();
                        result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                    }
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
                result.Entity = ent;
            }
            return result;
        }

        public async Task<ResponseResult> Delete(ItemSegmentationViewModel ent)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.DeleteAsync(webApiBaseUrl + $"/itemSegmentation/{ent.id}"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                }
            }
            return result;
        }
        //public async Task<List<ItemSegmentationViewModel>> GetAll()
        public async Task<ResponseResult> GetAll()
        {
            throw new NotImplementedException();

            ////List<ItemSegmentationViewModel> result = new List<ItemSegmentationViewModel>();
            //ResponseResult result = new ResponseResult();
            //using (var httpClient = new HttpClient())
            //{
            //    //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccesor.GetToken());
            //    using (var response = await httpClient.GetAsync(webApiBaseUrl + "/itemSegmentation"))
            //    {
            //        var apiResponse = await response.Content.ReadAsStringAsync();
            //        result.Entity= JsonConvert.DeserializeObject<List<ItemSegmentationViewModel>>(apiResponse);
            //        //result = JsonConvert.DeserializeObject<List<ItemSegmentationViewModel>>(apiResponse);
            //    }
            //}
            //return result;
        }

        public async Task<ResponseResult> GetById(long id)
        {
            //ItemSegmentationViewModel result = new ItemSegmentationViewModel();
            ResponseResult resutl = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(webApiBaseUrl + "/itemSegmentation/" + id))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<ItemSegmentationViewModel>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> Update(ItemSegmentationViewModel ent)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                string strPayLoad = JsonConvert.SerializeObject(ent);
                HttpContent content = new StringContent(strPayLoad, Encoding.UTF8, "application/json");
                using (var response = await httpClient.PutAsync(webApiBaseUrl + "/itemSegmentation", content))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                }
            }
            return result;
        }
        //default
        //public async Task<ResponseResult> Search(string name)
        //{
        //    ResponseResult result = new ResponseResult();
        //    using (var httpClient = new HttpClient())
        //    {
        //        //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
        //        using (var response = await httpClient.GetAsync(webApiBaseUrl + $"/itemSegmentation/search/{name}"))
        //        {
        //            var apiResponse = await response.Content.ReadAsStringAsync();
        //            result.Entity = JsonConvert.DeserializeObject<List<ItemSegmentationViewModel>>(apiResponse);
        //        }
        //    }
        //    return result;
        //}
        //public async Task<ResponseResult> Page(int page, int rows)
        //{
        //    ResponseResult result = new ResponseResult();
        //    using (var httpClient = new HttpClient())
        //    {
        //        //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
        //        using (var response = await httpClient.GetAsync(webApiBaseUrl + $"/itemSegmentation/page/{page}/{rows}"))
        //        {
        //            var apiResponse = await response.Content.ReadAsStringAsync();
        //            result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
        //        }
        //    }
        //    return result;
        //}

        public async Task<ResponseResult> Page(int page, int rows, string name)
        {
            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                name = string.IsNullOrEmpty(name) ? "--all" : name;
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(webApiBaseUrl + $"/itemSegmentation/page/{page}/{rows}/{name}"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                }
            }
            return result;
        }

    }
}
