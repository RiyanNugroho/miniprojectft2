﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Security;

namespace WebApp.Services
{
    public class M_medical_itemService : IService<MedItemViewModel>
    {
        private readonly IConfiguration _configuration;
        private readonly string webApiBaseUrl;
        private ResponseResult result = new ResponseResult();
        public M_medical_itemService(IConfiguration configuration)
        {
            _configuration = configuration;
            webApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
        }
        public async Task<ResponseResult> Create(StreamContent streamContent, MedItemViewModel ent)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var httpClient = new HttpClient())
                {
                    using (var multipartFormContent = new MultipartFormDataContent())
                    {
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                        //using (var streamContent = new StreamContent(model.FilePath.OpenReadStream()))
                        //{ }
                        using (var fileContent = new ByteArrayContent(await streamContent.ReadAsByteArrayAsync()))
                        {
                            fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
                            multipartFormContent.Add(streamContent, "FilePath", ent.FilePath.FileName);
                            multipartFormContent.Add(new StringContent(ent.name), "name");
                            multipartFormContent.Add(new StringContent(ent.medical_item_category_id.ToString()), "medical_item_category_id");
                            multipartFormContent.Add(new StringContent(ent.composition), "composition");
                            multipartFormContent.Add(new StringContent(ent.medical_item_segmentation_id.ToString()), "medical_item_segmentation_id");
                            multipartFormContent.Add(new StringContent(ent.manufacturer), "manufacturer");
                            multipartFormContent.Add(new StringContent(ent.indication), "indication");
                            multipartFormContent.Add(new StringContent(ent.dosage), "dosage");
                            multipartFormContent.Add(new StringContent(ent.directions), "directions");
                            multipartFormContent.Add(new StringContent(ent.contraindication), "contraindication");
                            multipartFormContent.Add(new StringContent(ent.caution), "caution");
                            multipartFormContent.Add(new StringContent(ent.packaging), "packaging");
                            multipartFormContent.Add(new StringContent(ent.price_max.ToString()), "price_max");
                            multipartFormContent.Add(new StringContent(ent.price_min.ToString()), "price_min");
                            multipartFormContent.Add(new StringContent(ent.packaging), "packaging");

                            using (var response = await httpClient.PostAsync(webApiBaseUrl + "/M_medical_item", multipartFormContent))
                            {
                                if (response.IsSuccessStatusCode)
                                {
                                    var apiResponse = await response.Content.ReadAsStringAsync();
                                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                                }
                                else
                                {
                                    result.Success = false;
                                    result.Message = response.ReasonPhrase;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
                result.Entity = ent;
            }
            return result;
        }

        public async Task<ResponseResult> Delete(MedItemViewModel ent)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.DeleteAsync(webApiBaseUrl + $"/M_medical_item/{ent.id}"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                }
            }
            return result;
        }
        //public async Task<List<MedItemViewModel>> GetAll()
        public async Task<ResponseResult> GetAll()
        {
            throw new NotImplementedException();

            ////List<MedItemViewModel> result = new List<MedItemViewModel>();
            //ResponseResult result = new ResponseResult();
            //using (var httpClient = new HttpClient())
            //{
            //    //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccesor.GetToken());
            //    using (var response = await httpClient.GetAsync(webApiBaseUrl + "/M_medical_item"))
            //    {
            //        var apiResponse = await response.Content.ReadAsStringAsync();
            //        result.Entity= JsonConvert.DeserializeObject<List<MedItemViewModel>>(apiResponse);
            //        //result = JsonConvert.DeserializeObject<List<MedItemViewModel>>(apiResponse);
            //    }
            //}
            //return result;
        }

        public async Task<ResponseResult> GetById(long id)
        {
            //M_medical_item_segmentationViewModel result = new M_medical_item_segmentationViewModel();
            ResponseResult resutl = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(webApiBaseUrl + "/M_medical_item/" + id))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity = JsonConvert.DeserializeObject<MedItemViewModel>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> Update(MedItemViewModel ent)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                string strPayLoad = JsonConvert.SerializeObject(ent);
                HttpContent content = new StringContent(strPayLoad, Encoding.UTF8, "application/json");
                using (var response = await httpClient.PutAsync(webApiBaseUrl + "/M_medical_item", content))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> Page(int page, int rows, string name)
        {
            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                name = string.IsNullOrEmpty(name) ? "--all" : name;
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(webApiBaseUrl + $"/M_medical_item/page/{page}/{rows}/{name}"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                }
            }
            return result;
        }

        public Task<ResponseResult> Create(MedItemViewModel ent)
        {
            throw new NotImplementedException();
        }
    }
}
