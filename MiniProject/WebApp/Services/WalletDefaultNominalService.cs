﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Security;

namespace WebApp.Services
{
    public class WalletDefaultNominalService : IService<WalletDefaultNominalViewModel>
    {
        private readonly IConfiguration _configuration;
        private readonly string webApiBaseUrl;
        private ResponseResult result = new ResponseResult();

        public WalletDefaultNominalService(IConfiguration configuration)
        {
            _configuration = configuration;
            webApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
        }
        public async Task<ResponseResult> Create(WalletDefaultNominalViewModel ent)
        {
            //throw new NotImplementedException();
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                    string strPayload = JsonConvert.SerializeObject(ent);
                    HttpContent content = new StringContent(strPayload, Encoding.UTF8, "application/json");
                    using (var response = await httpClient.PostAsync(webApiBaseUrl + "/CustomerCustomNominal", content))
                    {
                        var apiResponse = await response.Content.ReadAsStringAsync();
                        result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                    }
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
                result.Entity = ent;
            }
            return result;
        }

        public Task<ResponseResult> Delete(WalletDefaultNominalViewModel ent)
        {
            throw new NotImplementedException();
        }

        public async Task<ResponseResult> GetAll()
        {
            //throw new NotImplementedException();

            ResponseResult result = new ResponseResult();
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(webApiBaseUrl + "/WalletDefaultNominal"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result.Entity= JsonConvert.DeserializeObject<List<WalletDefaultNominalViewModel>>(apiResponse);
                }
            }
            return result;
        }

        public Task<ResponseResult> GetById(long id)
        {
            throw new NotImplementedException();
        }

        public Task<ResponseResult> Update(WalletDefaultNominalViewModel ent)
        {
            throw new NotImplementedException();
        }
    }
}
