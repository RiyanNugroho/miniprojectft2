﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class PendaftaranController : Controller
    {
        private readonly ILogger<PendaftaranController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;

        private readonly PendaftaranService daftarServ;
        private readonly RoleService roleServ;

        public PendaftaranController(ILogger<PendaftaranController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            daftarServ = new PendaftaranService(configuration);
            roleServ = new RoleService(configuration);
            
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Create()
        {
            ResponseResult roleId = await roleServ.GetAll();
            ViewBag.RoleId = new SelectList((List<RoleViewModel>)roleId.Entity, "id", "name");
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(PendaftaranViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await daftarServ.Create(model);

                if (result.Success)
                {
                    ViewBag.Title = "Create";
                    ViewBag.Sub = "Created";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            ResponseResult roleId = await roleServ.GetAll();
            ViewBag.RoleId = new SelectList((List<RoleViewModel>)roleId.Entity, "id", "name");
            //ResponseResult locationRegion = await locserv.GetAll();
            //ViewBag.LocationRegion = new SelectList((List<LocationViewModel>)locationRegion.Entity, "id", "regionlist");            
            return PartialView("_Create", model);
        }
    }
}
