﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class CariitemController : Controller
    {
        private readonly ILogger<CariitemController> _logger;
        private readonly M_medical_itemService meditsrv;
        private readonly itemCategoryService medcatsrv;
        private readonly itemSegmentationService medsegsrv;
        public CariitemController(ILogger<CariitemController> logger, IConfiguration configuration)
        {
            _logger = logger;
            meditsrv = new M_medical_itemService(configuration);
            medcatsrv = new itemCategoryService(configuration);
            medsegsrv = new itemSegmentationService(configuration);
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Cari()
        {
            return PartialView("_Cari");
        }
        public IActionResult Tambahkeranjang()
        {
            return PartialView("_Tambahkeranjang");
        }

        public async Task<IActionResult> Details(int id)
        {
            ResponseResult result = await meditsrv.GetById(id);
            return PartialView("_Details", result);
        }


        [HttpGet]
        public async Task<IActionResult> ListPage(int page, int rows, string name)
        {
            //search = (String.IsNullOrEmpty(search) ? "--all" : search);
            ResponseResult result = await meditsrv.Page(page, rows, name);
            ViewBag.Pages = result.Pages;
            ViewData["SearchText"] = name;
            List<MedItemViewModel> list = JsonConvert.DeserializeObject<List<MedItemViewModel>>(result.Entity.ToString());
            return PartialView("_List", list);
        }

    }
}
