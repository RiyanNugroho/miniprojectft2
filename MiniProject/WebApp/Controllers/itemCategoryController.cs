﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class itemCategoryController : Controller
    {
        private readonly ILogger<itemCategoryController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;
        private readonly itemCategoryService itcatsrv;
        public itemCategoryController(ILogger<itemCategoryController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            itcatsrv = new itemCategoryService(configuration);
        }
        public IActionResult Index()
        {
            return View();
        }
        //public async Task<IActionResult> List()
        //{
        //    ResponseResult list = await itcatsrv.GetAll();
        //    //List<ItemCategoryViewModel> list = await medcatsrv.GetAll();
        //    return PartialView("_List", list.Entity);
        //}
        public IActionResult Create()
        {
            return PartialView("_Create");
        }
        [HttpPost]
        public async Task<IActionResult> Create(ItemCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await itcatsrv.Create(model);
                if (result.Success)
                {
                    ViewBag.Title = "Tambah";
                    ViewBag.Status = "Ditambahkan";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                    //ViewBag.ErrorMessage = "Nama Sudah Ada di Database";
                }
            }
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Edit(int id)
        {
            ResponseResult result = await itcatsrv.GetById(id);
            if (result != null)
            {
                return PartialView("_Edit", result.Entity);
            }
            return PartialView("_Edit", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ItemCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await itcatsrv.Update(model);
                if (result.Success)
                {
                    ViewBag.Title = "Ubah";
                    ViewBag.Status = "Diubah";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                    //ViewBag.ErrorMessage = "Nama Sudah Ada di Database";
                }
            }
            return PartialView("_Edit", model);
        }
        //public async Task<IActionResult> Details(int id)
        //{
        //    ResponseResult result = await itcatsrv.GetById(id);
        //    return PartialView("_Details", result.Entity);
        //}
        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await itcatsrv.GetById(id);
            if (result != null)
            {
                return PartialView("_Delete", result.Entity);
            }
            return PartialView("_Delete", result.Entity);
        }
        [HttpPost]
        public async Task<IActionResult> Delete(ItemCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await itcatsrv.Delete(model);
                if (result.Success)
                {
                    ViewBag.Title = "Hapus";
                    ViewBag.Status = "Dihapus";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            return PartialView("_Delete", model);
        }

        [HttpGet]
        public async Task<IActionResult> ListPage(int page, int rows, string name)
        {
            ResponseResult result = await itcatsrv.Page(page, rows, name);
            ViewBag.Pages = result.Pages;
            ViewData["SearchText"] = name;
            List<ItemCategoryViewModel> list = JsonConvert.DeserializeObject<List<ItemCategoryViewModel>>(result.Entity.ToString());
            if (list.Count == 0)
            {
                ViewBag.Cari = "Nama Tidak Ditemukan";
            }
            return PartialView("_List", list);
        }

    }
}
