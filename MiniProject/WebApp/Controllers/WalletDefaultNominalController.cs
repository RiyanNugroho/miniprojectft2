﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class WalletDefaultNominalController : Controller
    {
        private readonly ILogger<WalletDefaultNominalController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;
        private readonly WalletDefaultNominalService DefNomsrv;
        private readonly CustomerCustomNominalService CustNomsrv;
        private readonly CustomerWalletService CustWallsrv;
        private readonly CustomerRegisteredCardService CustRegsrv;
        public WalletDefaultNominalController(ILogger<WalletDefaultNominalController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            DefNomsrv = new WalletDefaultNominalService(configuration);
            CustNomsrv = new CustomerCustomNominalService(configuration);
            CustWallsrv = new CustomerWalletService(configuration);
            CustRegsrv = new CustomerRegisteredCardService(configuration);
        }
        public async Task<IActionResult> Index()
        {
            //ResponseResult CustWallet = await CustWallsrv.GetAll();
            return View();
        }

        public async Task<IActionResult> List()
        {
            ResponseResult DefNomList = await DefNomsrv.GetAll();
            return PartialView("_List", DefNomList.Entity);
        }
        public async Task<IActionResult> List2()
        {
            ResponseResult DefNomList = await CustNomsrv.GetAll();
            return PartialView("_List2", DefNomList.Entity);
        }

        public async Task<IActionResult> Identitas()
        {
            ResponseResult Identitas = await CustWallsrv.GetAll();
            return PartialView("_Identitas", Identitas.Entity);
        }
        public async Task<IActionResult> Identitas1()
        {
            ResponseResult Identitas1 = await CustRegsrv.GetAll();
            return PartialView("_Identitas1", Identitas1.Entity);
        }
        public async Task<IActionResult> Member()
        {
            ResponseResult Identitas = await CustWallsrv.GetAll();
            return PartialView("_Member", Identitas.Entity);
        }

        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(CustomerCustomNominalViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await CustNomsrv.Create(model);

                if (result.Success)
                {
                    ViewBag.Title = "Create";
                    ViewBag.Status = "Created";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            return PartialView("_Create", model);
        }

    }
}
