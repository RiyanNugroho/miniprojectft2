﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class LocationLevelController : Controller
    {
        private readonly ILogger<LocationController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;

        private readonly LocationLevelService locLevServ;

        public LocationLevelController(ILogger<LocationController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            locLevServ = new LocationLevelService(configuration);
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> List()
        {
            ResponseResult list = await locLevServ.GetAll();
            return PartialView("_List", list.Entity);
        }

        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(LocationLevelViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await locLevServ.Create(model);

                if (result.Success)
                {
                    ViewBag.Title = "Create";
                    ViewBag.Sub = "Created";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Edit(int id)
        {
            ResponseResult result = await locLevServ.GetById(id);
            return PartialView("_Edit", result);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(LocationLevelViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await locLevServ.Update(model);
                if (result.Success)
                {
                    ViewBag.Title = "Edit";
                    ViewBag.Sub = "Edited";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            return PartialView("_Edit", model);
        }

        public async Task<IActionResult> Details(int id)
        {
            ResponseResult result = await locLevServ.GetById(id);

            if (result != null)
            {
                return PartialView("_Details", result);
            }
            return PartialView("_Details", result);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await locLevServ.GetById(id);           
            return PartialView("_Delete", result);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(LocationLevelViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await locLevServ.Delete(model);
                if (result.Success)
                {
                    ViewBag.Title = "Delete";
                    ViewBag.Sub = "Deleted";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            return PartialView("_Delete", model);
        }
    }
}
