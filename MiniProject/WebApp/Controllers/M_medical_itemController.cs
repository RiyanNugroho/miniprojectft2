﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class M_medical_itemController : Controller
    {
        private readonly ILogger<M_medical_itemController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;
        private readonly M_medical_itemService meditsrv;
        private readonly itemCategoryService medcatsrv;
        private readonly itemSegmentationService medsegsrv;
        public M_medical_itemController(ILogger<M_medical_itemController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            meditsrv = new M_medical_itemService(configuration);
            medcatsrv = new itemCategoryService(configuration);
            medsegsrv = new itemSegmentationService(configuration);
        }
        public async Task<IActionResult> Index()
        {
            ResponseResult itemCat = await medcatsrv.Page(1, 100000, "");
            ViewBag.itemCat = new SelectList((List<ItemCategoryViewModel>)JsonConvert.DeserializeObject<List<ItemCategoryViewModel>>(itemCat.Entity.ToString()), "id", "name");
            ViewData["kategori"] = new SelectList((List<ItemCategoryViewModel>)JsonConvert.DeserializeObject<List<ItemCategoryViewModel>>(itemCat.Entity.ToString()), "id", "name");
            return View();
        }
        public async Task<IActionResult> Profile()
        {
            return View();
        }
        public async Task<IActionResult> Cari()
        {
            //ResponseResult categoryList = await medcatsrv.GetAll();
            //ViewBag.categoryList = new SelectList((List<ItemCategoryViewModel>)categoryList.Entity, "id", "name");
            return PartialView("_Cari");
        }
        public IActionResult Tambahkeranjang()
        {
            return PartialView("_Tambahkeranjang");
        }

        public async Task<IActionResult> Create()
        {
            ResponseResult categoryList = await medcatsrv.GetAll();
            ResponseResult segmentationList = await medsegsrv.GetAll();
            ViewBag.CategoryList = new SelectList((List<ItemCategoryViewModel>)categoryList.Entity, "id", "name");
            ViewBag.SegmentationList = new SelectList((List<ItemSegmentationViewModel>)segmentationList.Entity, "id", "name");
            return PartialView("_Create");
        }
        [HttpPost]
        public async Task<IActionResult> Create(MedItemViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await meditsrv.Create(model);
                if (result.Success)
                {
                    ViewBag.Title = "Create";
                    ViewBag.Status = "Created";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            ResponseResult categoryList = await medcatsrv.GetAll();
            ResponseResult segmentationList = await medsegsrv.GetAll();
            ViewBag.CategoryList = new SelectList((List<ItemCategoryViewModel>)categoryList.Entity, "id", "name");
            ViewBag.SegmentationList = new SelectList((List<ItemSegmentationViewModel>)segmentationList.Entity, "id", "name");
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Edit(int id)
        {
            ResponseResult categoryList = await medcatsrv.GetAll();
            ResponseResult segmentationList = await medsegsrv.GetAll();
            ViewBag.CategoryList = new SelectList((List<ItemCategoryViewModel>)categoryList.Entity, "id", "name");
            ViewBag.SegmentationList = new SelectList((List<ItemSegmentationViewModel>)segmentationList.Entity, "id", "name");
            ResponseResult result = await meditsrv.GetById(id);
            return PartialView("_Edit", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(MedItemViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await meditsrv.Update(model);
                if (result.Success)
                {
                    ViewBag.Title = "Edit";
                    ViewBag.Status = "Edited";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            ResponseResult categoryList = await medcatsrv.GetAll();
            ResponseResult segmentationList = await medsegsrv.GetAll();
            ViewBag.CategoryList = new SelectList((List<ItemCategoryViewModel>)categoryList.Entity, "id", "name");
            ViewBag.SegmentationList = new SelectList((List<ItemSegmentationViewModel>)segmentationList.Entity, "id", "name");
            return PartialView("_Edit", model);
        }
        public async Task<IActionResult> Details(int id)
        {
            ResponseResult result = await meditsrv.GetById(id);
            return PartialView("_Details", result.Entity);
        }
        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await meditsrv.GetById(id);
            return PartialView("_Delete", result.Entity);
        }
        [HttpPost]
        public async Task<IActionResult> Delete(MedItemViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await meditsrv.Delete(model);
                if (result.Success)
                {
                    ViewBag.Title = "Delete";
                    ViewBag.Status = "Deleted";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            return PartialView("_Delete", model);
        }

        [HttpGet]
        public async Task<IActionResult> ListPage(int page, int rows, string name)
        {
            //search = (String.IsNullOrEmpty(search) ? "--all" : search);
            ResponseResult result = await meditsrv.Page(page, rows, name);
            ViewBag.Pages = result.Pages;
            ViewData["SearchText"] = name;
            List<MedItemViewModel> list = JsonConvert.DeserializeObject<List<MedItemViewModel>>(result.Entity.ToString());
            if (list.Count == 0)
            {
                ViewBag.Cari = "Nama Tidak Ditemukan";
            }
            return PartialView("_List", list);
        }

        [HttpGet]
        public async Task<IActionResult> ListCategory(int page, int rows, string name)
        {
            //search = (String.IsNullOrEmpty(search) ? "--all" : search);
            ResponseResult result = await medcatsrv.Page(page, rows, name);
            return PartialView("_ListCategory", result);
        }

    }
}
