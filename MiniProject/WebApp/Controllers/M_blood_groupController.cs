﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;


namespace WebApp.Controllers
{
    [Authorize(Roles = "Administrator, Pasien, Dokter")]
    public class M_blood_groupController : Controller
    {
        private readonly ILogger<M_blood_groupController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;

        private readonly M_blood_groupService bloodserv;

        public M_blood_groupController(ILogger<M_blood_groupController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            bloodserv = new M_blood_groupService(configuration);
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> List()
        {
            ResponseResult list = await bloodserv.GetAll();
            return PartialView("_List", list.Entity);
        }

        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(BloodGroupViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await bloodserv.Create(model);

                if (result.Success)
                {
                    ViewBag.Title = "Create";
                    ViewBag.Sub = "Created";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Edit(long id)
        {
            ResponseResult result = await bloodserv.GetById(id);
            return PartialView("_Edit", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(BloodGroupViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await bloodserv.Update(model);
                if (result.Success)
                {
                    ViewBag.Title = "Edit";
                    ViewBag.Sub = "Edited";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            return PartialView("_Edit", model);

        }       

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await bloodserv.GetById(id);

            if (result != null)
            {
                return PartialView("_Delete", result.Entity);
            }


            return PartialView("_Delete", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(BloodGroupViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await bloodserv.Delete(model);
                if (result.Success)
                {
                    ViewBag.Title = "Delete";
                    ViewBag.Sub = "Deleted";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            return PartialView("_Delete", model);
        }

        [HttpGet]
        public async Task<IActionResult> Search(string name)
        {
            ResponseResult result = await bloodserv.Search(name);

            if (result != null)
            {
                return PartialView("_List", result.Entity);
            }


            return PartialView("_List", result.Entity);
        }
    }
}
