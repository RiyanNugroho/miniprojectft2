﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class CustomerWalletController : Controller
    {
        private readonly ILogger<CustomerWalletController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;
        private readonly CustomerWalletService CustWalletsrv;
        public CustomerWalletController(ILogger<CustomerWalletController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            CustWalletsrv = new CustomerWalletService(configuration);
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> List()
        {
            ResponseResult CustWalletList = await CustWalletsrv.GetAll();
            return PartialView("_List", CustWalletList.Entity);
        }

        public async Task<IActionResult> Details(int customer_id)
        {
            ResponseResult result = await CustWalletsrv.GetById(customer_id);
            return PartialView("_Details", result.Entity);
        }
    }
}
