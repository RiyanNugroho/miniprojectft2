﻿using Microsoft.AspNetCore.Mvc;
using ViewModel;

namespace WebApp.Controllers
{
    public class UtilitiesController : Controller
    {
        public IActionResult GetListRow()
        {
            return PartialView("_ListRow", Utilities.GetListRow());
        }
        
        public IActionResult Sorting()
        {
            return PartialView("_Sorting", Utilities.GetSortList());
        }
    }
}
