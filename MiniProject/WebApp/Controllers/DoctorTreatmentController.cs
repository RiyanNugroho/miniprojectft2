﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class DoctorTreatmentController : Controller
    {
        private readonly ILogger<DoctorTreatmentController> _logger;
        private readonly DoctorTreatmentService DocTreatServ;

        public DoctorTreatmentController(ILogger<DoctorTreatmentController> logger, IConfiguration configuration)
        {
            _logger = logger;
            //_configuration = configuration;
            //XPosWebApiBaseUrl = _configuration.GetValue<string>("XPosWebApiBaseUrl");
            DocTreatServ = new DoctorTreatmentService(configuration);
            //new ContextAccessor(accessor);
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> List()
        {
            ResponseResult list = await DocTreatServ.GetAll();
            return PartialView("_List", list.Entity);
        }
        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpGet]
        public async Task<IActionResult> Search(string name)
        {
            ResponseResult result = await DocTreatServ.Search(name);
            if (result != null)
            {
                return PartialView("_List", result.Entity);
            }
            return PartialView("_List", result.Entity);

        }

        [HttpPost]
        public async Task<IActionResult> Create(DoctorTreatmentViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await DocTreatServ.Create(model);
                if (result.Success)
                {
                    //ViewBag.Title = "Tambah";
                    ViewBag.Body = "Tindakan berhasil ditambahkan";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Details(int id)
        {
            ResponseResult result = await DocTreatServ.GetById(id);
            return PartialView("_Details", result);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await DocTreatServ.GetById(id);
            return PartialView("_Delete", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(DoctorTreatmentViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await DocTreatServ.Delete(model);
                if (result.Success)
                {
                    ViewBag.Title = "Sukses";
                    ViewBag.Body = "Tindakan berhasil dihapus!";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Delete", model);
        }
    }
}
