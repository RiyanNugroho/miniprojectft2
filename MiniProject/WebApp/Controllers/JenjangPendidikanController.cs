﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class JenjangPendidikanController : Controller
    {
        private readonly ILogger<JenjangPendidikanController> _logger;
        private readonly JenjangPendidikanService JPServ;

        public JenjangPendidikanController(ILogger<JenjangPendidikanController> logger, IConfiguration configuration)
        {
            _logger = logger;
            JPServ = new JenjangPendidikanService(configuration);
        }

        public IActionResult Index()
        {
            return View();
        }


        public async Task<IActionResult> List()
        {
            ResponseResult list = await JPServ.GetAll();
            return PartialView("_List", list.Entity);
        }


        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(EducationLevelViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await JPServ.Create(model);
                if (result.Success)
                {
                    ViewBag.Title = "Create";
                    ViewBag.Body = "Created!";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Edit(int id)
        {
            ResponseResult result = await JPServ.GetById(id);
            return PartialView("_Edit", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(EducationLevelViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await JPServ.Update(model);
                if (result.Success)
                {
                    ViewBag.Title = "Edit";
                    ViewBag.Body = "Changed!";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Edit", model);
        }

        public async Task<IActionResult> Details(int id)
        {
            ResponseResult result = await JPServ.GetById(id);
            return PartialView("_Details", result);
        }

        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await JPServ.GetById(id);
            return PartialView("_Delete", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(EducationLevelViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await JPServ.Delete(model);
                if (result.Success)
                {
                    ViewBag.Title = "Delete";
                    ViewBag.Body = "Deleted!";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Delete", model);
        }

        [HttpGet]
        public async Task<IActionResult> Search(string name)
        {
            List<EducationLevelViewModel> result = await JPServ.Search(name);
            if (result != null)
            {
                if (result.Count == 0)
                    ViewBag.Error = "Item tidak ditemukan!";
                else
                    ViewBag.Result = "Hasil pencarian untuk " + name + " : " + result.Count + " hasil!";

                return PartialView("_List", result);
            }
            return PartialView("_List", result);
        }

    }
}
