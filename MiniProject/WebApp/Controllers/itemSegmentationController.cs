﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class itemSegmentationController : Controller
    {
        private readonly ILogger<itemSegmentationController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;
        private readonly itemSegmentationService itsegsrv;
        public itemSegmentationController(ILogger<itemSegmentationController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            itsegsrv = new itemSegmentationService(configuration);
        }
        public IActionResult Index()
        {
            ViewBag.Teks = "Masyarakat";
            return View();
        }
        //public async Task<IActionResult> List()
        //{
        //    ResponseResult list = await itsegsrv.GetAll();
        //    //List<ItemSegmentationViewModel> list = await medsegsrv.GetAll();
        //    return PartialView("_List", list.Entity);
        //}
        public IActionResult Create()
        {
            return PartialView("_Create");
        }
        [HttpPost]
        public async Task<IActionResult> Create(ItemSegmentationViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await itsegsrv.Create(model);
                if (result.Success)
                {
                    ViewBag.Title = "Tambah";
                    ViewBag.Status = "Ditambahkan";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                    //ViewBag.ErrorMessage = "Nama Sudah Ada di Database";
                }
            }
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Edit(int id)
        {
            ResponseResult result = await itsegsrv.GetById(id);
            return PartialView("_Edit", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ItemSegmentationViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await itsegsrv.Update(model);
                if (result.Success)
                {
                    ViewBag.Title = "Ubah";
                    ViewBag.Status = "Diubah";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                    //ViewBag.ErrorMessage = "Nama Sudah Ada di Database";
                }
            }
            return PartialView("_Edit", model);
        }
        //public async Task<IActionResult> Details(int id)
        //{
        //    ResponseResult result = await itsegsrv.GetById(id);
        //    return PartialView("_Details", result.Entity);
        //}
        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await itsegsrv.GetById(id);
            return PartialView("_Delete", result.Entity);
        }
        [HttpPost]
        public async Task<IActionResult> Delete(ItemSegmentationViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await itsegsrv.Delete(model);
                if (result.Success)
                {
                    ViewBag.Title = "Hapus";
                    ViewBag.Status = "Dihapus";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            return PartialView("_Delete", model);
        }
        //[HttpGet]
        //public async Task<IActionResult> Search(string name)
        //{
        //    ResponseResult result = await itsegsrv.Search(name);
        //    //if (result != null)
        //    if (!string.IsNullOrEmpty(name))
        //    {
        //        return PartialView("_List", result.Entity);
        //    }
        //    return PartialView("_List", result.Entity);

        //}
        [HttpGet]
        public async Task<IActionResult> ListPage(int page, int rows, string name)
        {
            ResponseResult result = await itsegsrv.Page(page, rows, name);
            ViewBag.Pages = result.Pages;
            ViewData["SearchText"] = name;
            List<ItemSegmentationViewModel> list = JsonConvert.DeserializeObject<List<ItemSegmentationViewModel>>(result.Entity.ToString());
            if (list.Count == 0)
            {
                ViewBag.Cari = "Nama Tidak Ditemukan";
            }
            return PartialView("_List", list);
        }
    }
}
