﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class CustomerRelationController : Controller
    {
        private readonly ILogger<CustomerRelationController> _logger;
        private readonly CustomerRelationService custRelServ;

        public CustomerRelationController(ILogger<CustomerRelationController> logger, IConfiguration configuration)
        {
            _logger = logger;
            custRelServ = new CustomerRelationService(configuration);
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Search(string name)
        {
            List<CustRelationViewModel> result = await custRelServ.Search(name);
            if (result != null)
            {
                if (result.Count == 0)
                    ViewBag.Error = "Item tidak ditemukan!";
                else if (result.Count <= 4)
                    ViewBag.Result = "Hasil pencarian untuk " + name + " : " + result.Count + " hasil!";
                else
                    ViewBag.Result = "Hasil pencarian untuk " + name + " : " + result.Count + " hasil teratas!";

                return PartialView("_List", result);
            }
            return PartialView("_List", result);
        }

        public async Task<IActionResult> List()
        {
            //List<CustRelationViewModel> list = await custRelServ.GetAll();
            ResponseResult list = await custRelServ.GetAll();
            return PartialView("_List", list.Entity);
        }

        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(CustRelationViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await custRelServ.Create(model);
                if (result.Success)
                {
                    ViewBag.Title = "Menambah";
                    ViewBag.Body = "Ditambah";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Edit(int id)
        {
            ResponseResult result = await custRelServ.GetById(id);
            return PartialView("_Edit", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CustRelationViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await custRelServ.Update(model);
                if (result.Success)
                {
                    ViewBag.Title = "Mengubah";
                    ViewBag.Body = "Diubah";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Edit", model);
        }

        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await custRelServ.GetById(id);
            return PartialView("_Delete", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(CustRelationViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await custRelServ.Delete(model);
                if (result.Success)
                {
                    ViewBag.Title = "Menghapus";
                    ViewBag.Body = "Dihapus";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Delete", model);
        }
    }
}
