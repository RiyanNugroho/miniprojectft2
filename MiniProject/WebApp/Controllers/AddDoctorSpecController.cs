﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Dokter")]

    public class AddDoctorSpecController : Controller
    {
        private readonly ILogger<AddDoctorSpecController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;

        private readonly SpecializationService specServ;
        private readonly AddDoctorSpecService docspecServ;  

        public AddDoctorSpecController(ILogger<AddDoctorSpecController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            specServ = new SpecializationService(configuration);
            docspecServ = new AddDoctorSpecService(configuration);
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> List()
        {
            ResponseResult list = await docspecServ.GetAll();
            return PartialView("_List", list.Entity);
        }

        public async Task<IActionResult> Create()
        {            
            ResponseResult specializationList = await specServ.GetAll();
            ViewBag.specializationList = new SelectList((List<SpecializationViewModel>)specializationList.Entity, "id", "name");
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(DoctorSpecializationViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await docspecServ.Create(model);

                if (result.Success)
                {
                    ViewBag.Title = "Create";
                    ViewBag.Sub = "Created";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            ResponseResult specializationList = await specServ.GetAll();
            ViewBag.specializationList = new SelectList((List<SpecializationViewModel>)specializationList.Entity, "id", "name");
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Edit(long id)
        {
            ResponseResult specializationList = await specServ.GetAll();
            ViewBag.specializationList = new SelectList((List<SpecializationViewModel>)specializationList.Entity, "id", "name");
            ResponseResult result = await docspecServ.GetById(id);

            return PartialView("_Edit", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(DoctorSpecializationViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await docspecServ.Update(model);
                if (result.Success)
                {
                    ViewBag.Title = "Edit";
                    ViewBag.Sub = "Edited";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            ResponseResult specializationList = await specServ.GetAll();
            ViewBag.specializationList = new SelectList((List<SpecializationViewModel>)specializationList.Entity, "id", "name");
            return PartialView("_Edit", model);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await docspecServ.GetById(id);

            if (result != null)
            {
                return PartialView("_Delete", result.Entity);
            }
            return PartialView("_Delete", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(DoctorSpecializationViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await docspecServ.Delete(model);
                if (result.Success)
                {
                    ViewBag.Title = "Delete";
                    ViewBag.Sub = "Deleted";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            return PartialView("_Delete", model);
        }
    }
}
