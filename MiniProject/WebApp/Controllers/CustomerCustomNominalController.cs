﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class CustomerCustomNominalController : Controller
    {
        private readonly ILogger<CustomerCustomNominalController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;
        private readonly CustomerCustomNominalService CustNomsrv;
        public CustomerCustomNominalController(ILogger<CustomerCustomNominalController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            CustNomsrv = new CustomerCustomNominalService(configuration);
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(CustomerCustomNominalViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await CustNomsrv.Create(model);

                if (result.Success)
                {
                    ViewBag.Title = "Create";
                    ViewBag.Status = "Created";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> List()
        {
            ResponseResult DefNomList = await CustNomsrv.GetAll();
            return PartialView("_List", DefNomList.Entity);
        }
    }
}
