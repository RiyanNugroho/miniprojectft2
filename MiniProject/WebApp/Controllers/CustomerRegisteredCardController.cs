﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class CustomerRegisteredCardController : Controller
    {
        private readonly ILogger<CustomerRegisteredCardController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;
        private readonly CustomerRegisteredCardService CustRegsrv;
        public CustomerRegisteredCardController(ILogger<CustomerRegisteredCardController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            CustRegsrv = new CustomerRegisteredCardService(configuration);
        }
        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> List()
        {
            ResponseResult CustWalletList = await CustRegsrv.GetAll();
            return PartialView("_List", CustWalletList.Entity);
        }

        public async Task<IActionResult> Details(int customer_id)
        {
            ResponseResult result = await CustRegsrv.GetById(customer_id);
            return PartialView("_Details", result.Entity);
        }
    }
}
