﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;


namespace WebApp.Controllers
{
    [Authorize(Roles = "Administrator, Dokter")]
    public class KategoriFasilitasKesehatanController : Controller
    {
        private readonly ILogger<KategoriFasilitasKesehatanController> _logger;
        private readonly KategoriFasilitasKesehatanService FasKesServ;
        private readonly DoctorTreatmentService DocTreatServ;

        public KategoriFasilitasKesehatanController(ILogger<KategoriFasilitasKesehatanController> logger, IConfiguration configuration)
        {
            _logger = logger;
            //_configuration = configuration;
            //XPosWebApiBaseUrl = _configuration.GetValue<string>("XPosWebApiBaseUrl");
            FasKesServ = new KategoriFasilitasKesehatanService(configuration);
            DocTreatServ = new DoctorTreatmentService(configuration);
            //new ContextAccessor(accessor);
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task <IActionResult> Profile()
        {
            ResponseResult List = await DocTreatServ.GetAll();
            return View(List.Entity);
        }

        public async Task<IActionResult> List()
        {
            ResponseResult list = await FasKesServ.GetAll();
            return PartialView("_List", list.Entity);
        }
        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpGet]
        public async Task<IActionResult> Search(string name)
        {
            List<FacilityCategoryViewModel> result = await FasKesServ.Search(name);
            if (result != null)
            {
                if (result.Count == 0)
                    ViewBag.Cari = "Nama Tidak Ditemukan";
                
                return PartialView("_List", result);
            }
            return PartialView("_List", result);
        }

        [HttpPost]
        public async Task<IActionResult> Create(FacilityCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await FasKesServ.Create(model);
                if (result.Success)
                {
                    //ViewBag.Title = "Tambah";
                    ViewBag.Body = "Berhasil Ditambahkan";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Create", model);
        }
        public async Task<IActionResult> Edit(int id)
        {
            ResponseResult result = await FasKesServ.GetById(id);
            return PartialView("_Edit", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(FacilityCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await FasKesServ.Update(model);
                if (result.Success)
                {
                    //ViewBag.Title = "Ubah";
                    ViewBag.Body = "Berhasil Diubah";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Edit", model);
        }

        public async Task<IActionResult> Details(int id)
        {
            ResponseResult result = await FasKesServ.GetById(id);
            return PartialView("_Details", result);
        }

        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await FasKesServ.GetById(id);
            return PartialView("_Delete", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(FacilityCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await FasKesServ.Delete(model);
                if (result.Success)
                {
                    //ViewBag.Title = "Hapus";
                    ViewBag.Body = "Berhasil Dihapus!";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Delete", model);
        }
    }
}
