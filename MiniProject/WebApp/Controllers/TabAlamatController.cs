﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class TabAlamatController : Controller
    {
        private readonly ILogger<TabAlamatController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;

        private readonly TabAlamatService TabServ;
        private readonly LocationService locserv;

        private List<ListValue> colList = new List<ListValue>()
            {
                new ListValue(){ Id = 1, Value = "biodata_id"},
                new ListValue(){ Id = 2, Value = "label"},
                new ListValue(){ Id = 3, Value = "recipient"},
                new ListValue(){ Id = 4, Value = "recipient_phone_number"},
                new ListValue(){ Id = 5, Value = "location_id"},
                new ListValue(){ Id = 6, Value = "postal_code"},
                new ListValue(){ Id = 7, Value = "address"}
            };

        public TabAlamatController(ILogger<TabAlamatController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            TabServ = new TabAlamatService(configuration);
            locserv = new LocationService(configuration);
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Create()
        {
            ResponseResult location = await locserv.Page(1, 100000, "");
            ViewBag.Location = new SelectList((List<LocationViewModel>)JsonConvert.DeserializeObject<List<LocationViewModel>>(location.Entity.ToString()), "id", "name");
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(TabAlamatViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await TabServ.Create(model);
                if (result.Success)
                {
                    ViewBag.title = "Create";
                    ViewBag.subtitle = "Created";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }

            }
            ResponseResult location = await locserv.Page(1, 100000, "");
            ViewBag.Location = new SelectList((List<LocationViewModel>)JsonConvert.DeserializeObject<List<LocationViewModel>>(location.Entity.ToString()), "id", "name");
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Edit(int id)
        {

            ResponseResult result = await TabServ.GetById(id);
            if (result != null)
            {
                return PartialView("_Edit", result.Entity);
            }
            ResponseResult location = await locserv.Page(1, 100000, "");
            ViewBag.Location = new SelectList((List<LocationViewModel>)JsonConvert.DeserializeObject<List<LocationViewModel>>(location.Entity.ToString()), "id", "name");
            return PartialView("_Edit", result.Entity);

        }


        [HttpPost]
        public async Task<IActionResult> Edit(TabAlamatViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await TabServ.Update(model);

                if (result.Success)
                {
                    ViewBag.title = "Edit";
                    ViewBag.subtitle = "Edited";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }


            }
            ResponseResult location = await locserv.Page(1, 100000, "");
            ViewBag.Location = new SelectList((List<LocationViewModel>)JsonConvert.DeserializeObject<List<LocationViewModel>>(location.Entity.ToString()), "id", "name");
            return PartialView("_Edit", model);
        }


        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {

            ResponseResult result = await TabServ.GetById(id);
            if (result != null)
            {
                return PartialView("_Delete", result.Entity);
            }
            return PartialView("_Delete", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(TabAlamatViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await TabServ.Delete(model);
                if (result.Success)
                {
                    ViewBag.title = "Delete";
                    ViewBag.subtitle = "Deleted";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }

            }
            return PartialView("_Delete", model);
        }

        //[HttpGet]
        //public async Task<IActionResult> Search(string name)
        //{
        //    ResponseResult result = await bankServ.Search(name);
        //    if (result != null)
        //    {
        //        return PartialView("_List", result.Entity);
        //    }
        //    return PartialView("_List", result.Entity);

        //}

        [HttpGet]
        public async Task<IActionResult> ListPage(int page, int rows, string search, string column, bool ascen)
        {

            ResponseResult result = await TabServ.Page(page, rows, search, column, ascen);
            //TupleListCount listAccount = result.<>Entity;
            if (result.Success)
            {
                ViewBag.Pages = result.Pages;
                ViewData["SearchText"] = search;
            }
            else
            {
                ViewBag.ErrorMessage = result.Message;
            }
            List<TabAlamatViewModel> list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TabAlamatViewModel>>(result.Entity.ToString());
            return PartialView("_List", list);
        }

        public async Task<IActionResult> Columns()
        {

            return PartialView("_Columns", colList);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteMany(List<AlamatMultiCheckViewModel> model)
        {
            //if (ModelState.IsValid)
            //{
            //    ResponseResult result = await proServ.Delete(model);
            //    if (result.Success)
            //    {
            //        ViewBag.Title = "Delete";
            //        ViewBag.SubTitle = "deleted";
            //        return PartialView("_Success", model);
            //    }
            //    else
            //        ViewBag.ErrorMessage = result.Message;
            //}
            ResponseResult result = await TabServ.DeleteMany(model);
            if (result.Success)
            {
                return PartialView("_Success", new TabAlamatViewModel());
            }
            return PartialView("_Success", new TabAlamatViewModel());
        }

    }
}
