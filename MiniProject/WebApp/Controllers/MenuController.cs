﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class MenuController : Controller
    {
        private readonly ILogger<MenuController> _logger;
        private readonly MenuService MenServ;

        public MenuController(ILogger<MenuController> logger, IConfiguration configuration)
        {
            _logger = logger;
            MenServ = new MenuService(configuration);
        }

        public IActionResult Index()
        {
            return View();
        }


        public async Task<IActionResult> List()
        {
            ResponseResult list = await MenServ.GetAll();
            return PartialView("_List", list.Entity);
        }


        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(MenuViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await MenServ.Create(model);
                if (result.Success)
                {
                    ViewBag.Title = "Create";
                    ViewBag.Body = "Created!";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Edit(int id)
        {
            ResponseResult result = await MenServ.GetById(id);
            return PartialView("_Edit", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(MenuViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await MenServ.Update(model);
                if (result.Success)
                {
                    ViewBag.Title = "Edit";
                    ViewBag.Body = "Changed!";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Edit", model);
        }

        public async Task<IActionResult> Details(int id)
        {
            ResponseResult result = await MenServ.GetById(id);
            return PartialView("_Details", result);
        }

        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await MenServ.GetById(id);
            return PartialView("_Delete", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(MenuViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await MenServ.Delete(model);
                if (result.Success)
                {
                    ViewBag.Title = "Delete";
                    ViewBag.Body = "Deleted!";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Delete", model);
        }

    }
}
