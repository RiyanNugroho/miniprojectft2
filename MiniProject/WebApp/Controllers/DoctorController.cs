﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class DoctorController : Controller
    {
        private readonly ILogger<DoctorController> _logger;
        private readonly DoctorService doctorServ;

        public DoctorController(ILogger<DoctorController> logger, IConfiguration configuration)
        {
            _logger = logger;
            //_configuration = configuration;
            //XPosWebApiBaseUrl = _configuration.GetValue<string>("XPosWebApiBaseUrl");
            doctorServ = new DoctorService(configuration);
            //new ContextAccessor(accessor);
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ListPage(int page, int rows, string name)
        {
            //search = (String.IsNullOrEmpty(search) ? "--all" : search);
            ResponseResult result = await doctorServ.Page(page, rows, name);
            ViewBag.Pages = result.Pages;
            ViewData["SearchText"] = name;
            List<DoctorViewModel> list = JsonConvert.DeserializeObject<List<DoctorViewModel>>(result.Entity.ToString());
            return PartialView("_List", list);
        }

        [HttpPost]
        public async Task<IActionResult> Create(DoctorViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await doctorServ.Create(model);
                if (result.Success)
                {
                    //ViewBag.Title = "Tambah";
                    ViewBag.Body = "Tindakan berhasil ditambahkan";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Details(int id)
        {
            ResponseResult result = await doctorServ.GetById(id);
            return PartialView("_Details", result);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await doctorServ.GetById(id);
            return PartialView("_Delete", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(DoctorViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await doctorServ.Delete(model);
                if (result.Success)
                {
                    ViewBag.Title = "Sukses";
                    ViewBag.Body = "Tindakan berhasil dihapus!";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Delete", model);
        }
    }
}
