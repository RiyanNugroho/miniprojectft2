﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Security;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILogger<UserViewModel> _logger;
        private readonly LoginService _loginServ;

        public AccountController(IConfiguration configuration, ILogger<UserViewModel> logger, IHttpContextAccessor accessor)
        {
            _logger = logger;
            _loginServ = new LoginService(configuration);
            new ContextAccessor(accessor);
        }

        public IActionResult Index(string returnUrl = "")
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        public IActionResult Login(string returnUrl = "")
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View("Index");
        }

        [HttpPost]
        public async Task<IActionResult> Login(UserViewModel model, string returnUrl = "")
        {
            if (ModelState.IsValid)
            {
                UserViewModel userLogin = await _loginServ.Authentication(model);
                if (userLogin.token != null && userLogin.is_locked != true)
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, userLogin.email),
                        new Claim("FullName", userLogin.fullname),
                        new Claim("Token", userLogin.token),
                        new Claim("Menu", userLogin.roleName)
                    };

                    claims.Add(new Claim(ClaimTypes.Role, userLogin.roleName));

                    var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                    await HttpContext.SignInAsync(
                        CookieAuthenticationDefaults.AuthenticationScheme,
                        new ClaimsPrincipal(claimsIdentity));

                    if (!String.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                        return Redirect(returnUrl);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewData["ErrorMessage"] = "Invalid login";

                    //userLogin.login_attempt += 1;
                    //_MiniProjDbContext.SaveChanges();

                    //if (userLogin.login_attempt > 3)
                    //{
                    //    userLogin.is_locked = true;
                    //    ViewData["ErrorMessage"] = "Error : Your Account has been locked";
                    //}
                }
            }
            return View("Index", model);
        }

        public async Task<IActionResult> LogoutAsync()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Login");
        }

        public IActionResult AccessDenied(string returnUrl = "")
        {
            return View();
        }



    }
}
