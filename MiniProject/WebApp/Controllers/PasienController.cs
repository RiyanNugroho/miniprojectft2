﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Administrator,Pasien")]
    public class PasienController : Controller
    {
        private readonly ILogger<PasienController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;

        private readonly PasienService pasienServ;
        private readonly M_blood_groupService bloodServ;
        private readonly CustomerRelationService custServ;

        public PasienController(ILogger<PasienController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            pasienServ = new PasienService(configuration);
            bloodServ = new M_blood_groupService(configuration);
            custServ = new CustomerRelationService(configuration);
        }

        private List<ListValue> colList = new List<ListValue>()
            {
                new ListValue(){ Id = 1, Value = "fullname"},
            };

        public IActionResult Index()
        {
            return View();
        }

        //public async Task<IActionResult> List()
        //{
        //    ResponseResult list = await pasienServ.GetAll();
        //    return PartialView("_List", list.Entity);
        //}

        [HttpGet]
        public async Task<IActionResult> ListPage(int page, int rows, string search, string cols, bool asc)
        {

            ResponseResult result = await pasienServ.All(page, rows, search, cols, asc);
            ViewBag.Pages = result.Pages;
            ViewData["SearchText"] = search;
            List<PasienViewModel> list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PasienViewModel>>(result.Entity.ToString());
            return PartialView("_List", list);
        }

        public async Task<IActionResult> Columns()
        {
            return PartialView("_Columns", colList);
        }

        public async Task<IActionResult> Create()
        {
            ResponseResult resultBlood = await bloodServ.GetAll();
            ViewBag.BloodList = new SelectList((List<BloodGroupViewModel>)resultBlood.Entity, "id", "code");
            ResponseResult resultRel = await custServ.GetAll();
            ViewBag.RelationList = new SelectList((List<CustRelationViewModel>)resultRel.Entity, "id", "name");
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(PasienViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await pasienServ.Create(model);
                if (result.Success)
                {
                    ViewBag.Title = "Menambah";
                    ViewBag.Body = "Ditambah";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            ResponseResult resultBlood = await bloodServ.GetAll();
            ViewBag.BloodList = new SelectList((List<BloodGroupViewModel>)resultBlood.Entity, "id", "code");
            ResponseResult resultRel = await custServ.GetAll();
            ViewBag.RelationList = new SelectList((List<CustRelationViewModel>)resultRel.Entity, "id", "name");
            return PartialView("_Create");
        }

        public async Task<IActionResult> Edit(int id)
        {
            ResponseResult result = await pasienServ.GetById(id);
            if (result != null)
            {
                return PartialView("_Edit", result.Entity);
            }
            return PartialView("_Edit", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(PasienViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await pasienServ.Update(model);
                if (result.Success)
                {
                    ViewBag.Title = "Mengubah";
                    ViewBag.Body = "Diubah";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Edit", model);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await pasienServ.GetById(id);
            if (result != null)
            {
                return PartialView("_Delete", result.Entity);
            }
            return PartialView("_Delete", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(PasienViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await pasienServ.Delete(model);
                if (result.Success)
                {
                    ViewBag.Title = "Menghapus";
                    ViewBag.Body = "Dihapus";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Delete", model);
        }
    }
}
