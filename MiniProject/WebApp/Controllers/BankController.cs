﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class BankController : Controller
    {
        private readonly ILogger<BankController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;

        private readonly BankService bankServ;

        private List<ListValue> colList = new List<ListValue>()
            {
                new ListValue(){ Id = 1, Value = "name"},
                new ListValue(){ Id = 2, Value = "va_code"}
            };

        public BankController(ILogger<BankController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            bankServ = new BankService(configuration);
        }
        public IActionResult Index()
        {
            return View();
        }

        //public async Task<IActionResult> List()
        //{
        //    ResponseResult list = await bankServ.GetAll();
        //    return PartialView("_List", list.Entity);
        //}
        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(BankViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await bankServ.Create(model);
                if (result.Success)
                {
                    ViewBag.title = "Create";
                    ViewBag.subtitle = "Created";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }

            }
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Edit(int id)
        {

            ResponseResult result = await bankServ.GetById(id);
            if (result != null)
            {
                return PartialView("_Edit", result.Entity);
            }
            return PartialView("_Edit", result.Entity);

        }


        [HttpPost]
        public async Task<IActionResult> Edit(BankViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await bankServ.Update(model);

                if (result.Success)
                {
                    ViewBag.title = "Edit";
                    ViewBag.subtitle = "Edited";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }


            }
            return PartialView("_Edit", model);
        }


        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {

            ResponseResult result = await bankServ.GetById(id);
            if (result != null)
            {
                return PartialView("_Delete", result.Entity);
            }
            return PartialView("_Delete", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(BankViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await bankServ.Delete(model);
                if (result.Success)
                {
                    ViewBag.title = "Delete";
                    ViewBag.subtitle = "Deleted";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }

            }
            return PartialView("_Delete", model);
        }

        //[HttpGet]
        //public async Task<IActionResult> Search(string name)
        //{
        //    ResponseResult result = await bankServ.Search(name);
        //    if (result != null)
        //    {
        //        return PartialView("_List", result.Entity);
        //    }
        //    return PartialView("_List", result.Entity);

        //}

        [HttpGet]
        public async Task<IActionResult> ListPage(int page, int rows, string search, string column, bool ascen)
        {

            ResponseResult result = await bankServ.Page(page, rows, search, column, ascen);
            //TupleListCount listAccount = result.<>Entity;

                ViewBag.Pages = result.Pages;
                ViewData["SearchText"] = search;
            List<BankViewModel> list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BankViewModel>>(result.Entity.ToString());
            if (list != null)
            {
                return PartialView("_List", list);
            }
            else 
            {

                ViewBag.Error = "Item tidak ditemukan";
                return PartialView("_List", list);
            }
        }

        public async Task<IActionResult> Columns()
        {
            
            return PartialView("_Columns", colList);
        }
    }
}
