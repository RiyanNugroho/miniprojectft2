﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class BiodataController : Controller
    {
        private readonly ILogger<BiodataController> _logger;
        private readonly BiodataService BioServ;

        public BiodataController(ILogger<BiodataController> logger, IConfiguration configuration)
        {
            _logger = logger;
            //_configuration = configuration;
            //XPosWebApiBaseUrl = _configuration.GetValue<string>("XPosWebApiBaseUrl");
            BioServ = new BiodataService(configuration);
            //new ContextAccessor(accessor);
        }

        public IActionResult Index()
        {
            ViewData["Search"] = "--all";
            return View();
        }

        [HttpGet]
        public IActionResult Index2(string search)
        {
            ViewData["Search"] = search;
            return View("Index");
        }


        public async Task<IActionResult> List()
        {
            ResponseResult list = await BioServ.GetAll();
            return PartialView("_List", list.Entity);
        }
        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpGet]
        public async Task<IActionResult> Search(string name)
        {
            ResponseResult result = await BioServ.Search(name);
            if (!result.Success)
            {
                return PartialView("_List", new List<BiodataViewModel>());
            }
            return PartialView("_List", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Create(BiodataViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await BioServ.Create(model);
                if (result.Success)
                {
                    //ViewBag.Title = "Tambah";
                    ViewBag.Body = "Berhasil Ditambahkan";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Create", model);
        }
        public async Task<IActionResult> Edit(int id)
        {
            ResponseResult result = await BioServ.GetById(id);
            return PartialView("_Edit", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(BiodataViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await BioServ.Update(model);
                if (result.Success)
                {
                    //ViewBag.Title = "Ubah";
                    ViewBag.Body = "Berhasil Diubah";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Edit", model);
        }

        public async Task<IActionResult> Details(int id)
        {
            //ResponseResult result = await BioServ.GetById(id);
            //if (id == 7)
            //{
            //    return View("Details");
            //}
            //if (id == 4)
            //{
            //    return View("Details2");
            //}
                ResponseResult result = await BioServ.GetById(id);
                return PartialView("_Details", result.Entity);

        }

        public async Task<IActionResult> Details2()
        {
            //ResponseResult result = await BioServ.GetById(id);
            //if (id == 7)
            //{
            //    return  View("Details");
            //}
            //if(id == 4)
            //{
            //    return View("Details2");
            //}
            return View();
        }

        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await BioServ.GetById(id);
            return PartialView("_Delete", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(BiodataViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await BioServ.Delete(model);
                if (result.Success)
                {
                    //ViewBag.Title = "Hapus";
                    ViewBag.Body = "Berhasil Dihapus!";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Delete", model);
        }
    }
}




