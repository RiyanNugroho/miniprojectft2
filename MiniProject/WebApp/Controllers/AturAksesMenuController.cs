﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;


namespace WebApp.Controllers
{
    public class AturAksesMenuController : Controller
    {
        private readonly ILogger<AturAksesMenuController> _logger;
        private readonly AturaksesMenuService Atserv;
        private readonly MenuService MenServ;

        public AturAksesMenuController(ILogger<AturAksesMenuController> logger, IConfiguration configuration)
        {
            _logger = logger;
            Atserv = new AturaksesMenuService(configuration);
            MenServ = new MenuService(configuration);
        }

        public IActionResult Index()
        {
            return View();
        }


        public async Task<IActionResult> List()
        {
            ResponseResult list = await Atserv.GetAll();
            return PartialView("_List", list.Entity);
        }

        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(MenuRoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await Atserv.Create(model);
                if (result.Success)
                {
                    ViewBag.Title = "Create";
                    ViewBag.Body = "Created!";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Create", model);
        }


        public async Task<IActionResult> Edit(int id)
        {
            ResponseResult Menulist = await MenServ.GetAll();
            return PartialView("_Edit", Menulist.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(MenuViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await MenServ.Update(model);
                if (result.Success)
                {
                    ViewBag.Title = "Edit";
                    ViewBag.Body = "Changed!";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            ResponseResult menResult = await MenServ.GetAll();
            ViewBag.Menulist = new SelectList((List<MenuViewModel>)menResult.Entity, "Id", "Name");
            return PartialView("_Edit", model);
        }

        public async Task<IActionResult> Details(int id)
        {
            ResponseResult result = await Atserv.GetById(id);
            return PartialView("_Details", result);
        }

        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await Atserv.GetById(id);
            return PartialView("_Delete", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(MenuRoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await Atserv.Delete(model);
                if (result.Success)
                {
                    ViewBag.Title = "Delete";
                    ViewBag.Body = "Deleted!";
                    return PartialView("_Success", model);
                }
                else
                    ViewBag.ErrorMessage = result.Message;
            }
            return PartialView("_Delete", model);
        }

        [HttpGet]
        public async Task<IActionResult> Search(string name)
        {
            ResponseResult result = await Atserv.Search(name);
            if (result != null)
            {
                return PartialView("_List", result.Entity);
            }
            return PartialView("_List", result.Entity);
        }


        [HttpPut]
        public async Task<IActionResult> CheckList(List<MenuCheckedViewModel> model)
        {
            ResponseResult result = await Atserv.CheckList(model);
            if (result.Success)
            {
                return PartialView("_Success", new MenuRoleViewModel());
            }
            ResponseResult menResult = await MenServ.GetAll();
            return PartialView("_Success", model);
        }
    }
}
