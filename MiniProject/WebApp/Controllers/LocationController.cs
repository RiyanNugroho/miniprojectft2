﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class LocationController : Controller
    {
        private readonly ILogger<LocationController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;

        private readonly LocationService locserv;
        private readonly LocationLevelService locLevServ;

        public LocationController(ILogger<LocationController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            locserv = new LocationService(configuration);
            locLevServ = new LocationLevelService(configuration);
        }

        public IActionResult Index()
        {
            return View();
        }

        //public async Task<IActionResult> List()
        //{
        //    ResponseResult list = await locserv.GetAll();
        //    return PartialView("_List", list.Entity);
        //}

        public async Task<IActionResult> Create()
        {
            ResponseResult locationLevels = await locLevServ.GetAll();
            //List<LocationLevelViewModel> locLevList = JsonConvert.DeserializeObject<List<LocationLevelViewModel>>(locationLevels.Entity.ToString());
            ViewBag.LocationLevels = new SelectList((List<LocationLevelViewModel>)locationLevels.Entity, "id", "name");
            ResponseResult locationRegion = await locserv.Page(1, 100000, "");
            ViewBag.LocationRegion = new SelectList((List<LocationViewModel>)JsonConvert.DeserializeObject<List<LocationViewModel>>(locationRegion.Entity.ToString()), "id", "regionlist");
            return PartialView("_Create");
        }


        [HttpPost]
        public async Task<IActionResult> Create(LocationViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await locserv.Create(model);

                if (result.Success)
                {
                    ViewBag.Title = "Create";
                    ViewBag.Sub = "Created";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            ResponseResult locationLevels = await locLevServ.GetAll();
            ViewBag.LocationLevels = new SelectList((List<LocationLevelViewModel>)locationLevels.Entity, "id", "name");
            ResponseResult locationRegion = await locserv.Page(1, 100000, "");
            ViewBag.LocationRegion = new SelectList((List<LocationViewModel>)JsonConvert.DeserializeObject<List<LocationViewModel>>(locationRegion.Entity.ToString()), "id", "regionlist");                        
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Edit(long id)
        {
            ResponseResult locationLevels = await locLevServ.GetAll();
            ViewBag.LocationLevels = new SelectList((List<LocationLevelViewModel>)locationLevels.Entity, "id", "name");
            ResponseResult locationRegion = await locserv.Page(1, 100000, "");
            ViewBag.LocationRegion = new SelectList((List<LocationViewModel>)JsonConvert.DeserializeObject<List<LocationViewModel>>(locationRegion.Entity.ToString()), "id", "regionlist");
            ResponseResult result = await locserv.GetById(id);
            return PartialView("_Edit", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(LocationViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await locserv.Update(model);
                if (result.Success)
                {
                    ViewBag.Title = "Edit";
                    ViewBag.Sub = "Edited";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            ResponseResult locationLevels = await locLevServ.GetAll();
            ViewBag.LocationLevels = new SelectList((List<LocationLevelViewModel>)locationLevels.Entity, "id", "name");
            ResponseResult locationRegion = await locserv.Page(1, 100000, "");
            ViewBag.LocationRegion = new SelectList((List<LocationViewModel>)JsonConvert.DeserializeObject<List<LocationViewModel>>(locationRegion.Entity.ToString()), "id", "regionlist");
            //ResponseResult locationRegion = await locserv.GetAll();
            //ViewBag.LocationRegion = new SelectList((List<LocationViewModel>)locationRegion.Entity, "id", "regionlist");
            return PartialView("_Edit", model);

        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            ResponseResult result = await locserv.GetById(id);
            return PartialView("_Delete", result.Entity);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(LocationViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await locserv.Delete(model);
                if (result.Success)
                {
                    ViewBag.Title = "Delete";
                    ViewBag.Sub = "Deleted";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }
            }
            return PartialView("_Delete", model);
        }

        [HttpGet]
        public async Task<IActionResult> ListPage(int page, int rows, string search)
        {

            ResponseResult result = await locserv.Page(page, rows, search);
            //TupleListCount listAccount = result.<>Entity;
            ViewBag.Pages = result.Pages;
            ViewData["SearchText"] = search;
            List<LocationViewModel> list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LocationViewModel>>(result.Entity.ToString());
            return PartialView("_List", list);
        }
    }
}
